//
//  Server.swift
//  TestSwiftApp
//
//  Created by Surbhi on 21/10/16.
//  Copyright © 2016 Neuronimbus. All rights reserved.
//

import UIKit


class Server: NSObject {
    
    static func getRequestWithURL(urlString: String, completionHandler:@escaping (_ response: Dictionary <String, AnyObject>) -> Void) {
        
        let defaultConfigObject = URLSessionConfiguration.default
        let defaultSession = URLSession(configuration: defaultConfigObject, delegate: nil, delegateQueue: nil)
        
        let urlRequest = NSMutableURLRequest(url: NSURL(string: urlString)! as URL)
//        urlRequest.timeoutInterval = 10000
        urlRequest.httpMethod = "GET"
        
        // Handling Basic HTTPS Authorization
        let task = defaultSession.dataTask(with: urlRequest as URLRequest) { ( data, response, error) -> Void in
            
            if error != nil {
                print("Error occurred: "+(error?.localizedDescription)!)
                Constants.appDelegate.stopIndicator()
                completionHandler([:])
                return;
            }
            do {
                let responseObjc = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.mutableContainers) as! [String: AnyObject]
                completionHandler(responseObjc)
            }
            catch {
                print("Error occurred parsing data: \(error)")
                completionHandler([:])
            }
        }
        task.resume()
    }
    static func postRequestWithURL(_ urlString: String, paramString: String, completionHandler:@escaping (_ response: Dictionary <String, AnyObject>) -> Void) {
        
        
        let defaultConfigObject = URLSessionConfiguration.default
        let defaultSession = URLSession(configuration: defaultConfigObject, delegate: nil, delegateQueue: nil)
        
        let urlRequest = NSMutableURLRequest(url: NSURL(string: urlString)! as URL)
        urlRequest.httpMethod = "POST"
        urlRequest.httpBody = paramString.data(using: String.Encoding.utf8)
        
        // Handling Basic HTTPS Authorization
        let authString = "wsbrandshop:Neuro@123"
        let authData = authString.data(using: String.Encoding.utf8)
        
        let authValue = "Basic \(authData!.base64EncodedString(options: NSData.Base64EncodingOptions.lineLength64Characters))"
//        urlRequest.addValue("application/json", forHTTPHeaderField: "Content-Type")
        urlRequest.setValue(authValue, forHTTPHeaderField: "Authorization")

        let task = defaultSession.dataTask(with: urlRequest as URLRequest) { ( data, response, error) -> Void in
            
            print(response)
            
            if error != nil {
                print(urlString)
                print(paramString)
                print("Error occurred: "+(error?.localizedDescription)!)
                Constants.appDelegate.stopIndicator()
                return;
            }
            do {
                
                if (response?.isKind(of: HTTPURLResponse.classForCoder()))!{
                    let statusCode = (response as! HTTPURLResponse).statusCode
                    print(statusCode)
                    
                
                }
                
//                if ([response isKindOfClass:[NSHTTPURLResponse class]]) {
//                    
//                    NSInteger statusCode = [(NSHTTPURLResponse *)response statusCode];
//                    
//                    if (statusCode != 200) {
//                        NSLog(@"dataTaskWithRequest HTTP status code: %d", statusCode);
//                        return;
//                    }
//                }

                let responseObjc = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.mutableContainers) as! [String: AnyObject]
                completionHandler(responseObjc)
            }
            catch {
                print(urlString)
                print(paramString)

                print("Error occurred parsing data: \(error)")
                completionHandler([:])
            }
        }
        task.resume()
    }
    
    
    
    static  func PostDataInDictionary(_ urlString: String, dict_data: Dictionary <String, AnyObject>, completionHandler:@escaping (_ response: Dictionary <String, AnyObject>) -> Void) {
        let defaultConfigObject = URLSessionConfiguration.default
        let defaultSession = URLSession(configuration: defaultConfigObject, delegate: nil, delegateQueue: nil)
        
        let urlRequest = NSMutableURLRequest(url: NSURL(string: urlString)! as URL)
        urlRequest.httpMethod = "POST"
        
        // Handling Basic HTTPS Authorization
        let authString = "wsbrandshop:Neuro@123"
        let authData = authString.data(using: String.Encoding.utf8)
        
        let authValue = "Basic \(authData!.base64EncodedString(options: NSData.Base64EncodingOptions.lineLength64Characters))"
        urlRequest.setValue(authValue, forHTTPHeaderField: "Authorization")

        
        
        urlRequest.addValue("application/json", forHTTPHeaderField: "Content-Type")
        urlRequest.addValue("application/json", forHTTPHeaderField: "Accept")
        
        do {
            let jsonData = try JSONSerialization.data(withJSONObject: dict_data, options: [])
            //
            //all fine with jsonData here
            urlRequest.httpBody = jsonData
            print(jsonData)
            
        } catch {
            //handle error
            print(error)
        }
        
        //urlRequest.setValue( "application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
        print(urlRequest)
        
        
        //Handling Basic HTTPS Authorization
        //let authString = "wsbrandshop:Neuro@123"
        //let authData = authString.data(using: String.Encoding.utf8)
        //let authValue = "Basic \(authData!.base64EncodedString(options: NSData.Base64EncodingOptions.lineLength64Characters))"
        //urlRequest.setValue(authValue, forHTTPHeaderField: "Authorization")
        //
        let task = defaultSession.dataTask(with: urlRequest as URLRequest, completionHandler: { data, response, error in
            
            guard error == nil else {
                return
            }
            
            guard let data = data else {
                return
            }
            
            do {
                //create json object from data
                if let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [String: Any] {
                    //  print(json)
                    completionHandler(json as Dictionary<String, AnyObject>)
                    // handle json...
                    
                }
                
            } catch let error {
                print(error.localizedDescription)
            }
        })
        
        task.resume()
        
    }
    
    
    
    static func LoginToApp(_ email : String, authType : String , authId : String , pass : String , completionHandler:@escaping (_ response: Dictionary <String, AnyObject>) -> Void)
    {
        
        let defaultConfigObject = URLSessionConfiguration.default
        let defaultSession = URLSession(configuration: defaultConfigObject, delegate: nil, delegateQueue: nil)
        var paramString = String()
        
        paramString = "email=\(email)&password=\(pass)"
        
        let loginMethod = "login"
        let urlString = Constants.BASEURL+loginMethod

        let urlRequest = NSMutableURLRequest(url: URL(string: urlString)!)
        urlRequest.timeoutInterval = 10000

        urlRequest.httpMethod = "POST"
        urlRequest.httpBody = paramString.data(using: String.Encoding.utf8)

        
        // Handling Basic HTTPS Authorization
        let authString = "wsbrandshop:Neuro@123"
        let authData = authString.data(using: String.Encoding.utf8)
        let authValue = "Basic \(authData!.base64EncodedString(options: NSData.Base64EncodingOptions.lineLength64Characters))"
        urlRequest.setValue(authValue, forHTTPHeaderField: "Authorization")
        
        let task = defaultSession.dataTask(with: urlRequest as URLRequest) { ( data, response, error) -> Void in
            
            if error != nil {
                print("Error occurred: "+(error?.localizedDescription)!)
                //                Constants.appDelegate.stopIndicator()
                return;
            }
            do {
                let responseObjc = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.mutableContainers) as! [String: AnyObject]
                completionHandler(responseObjc)
            }
            catch {
                print("Error occurred parsing data: \(error)")
                completionHandler([:])
            }
        }
        task.resume()
    }
    
    
    static func PostDataInDictionary( urlString: String, dict_data: Dictionary <String, Any>, completionHandler:@escaping ( _ response: Dictionary <String, Any>) -> Void) {
        let defaultConfigObject = URLSessionConfiguration.default
        let defaultSession = URLSession(configuration: defaultConfigObject, delegate: nil, delegateQueue: nil)
        
        let urlRequest = NSMutableURLRequest(url: NSURL(string: urlString)! as URL)
        urlRequest.httpMethod = "POST"
        
        let authString = "wsbrandshop:Neuro@123"
        let authData = authString.data(using: String.Encoding.utf8)
        let authValue = "Basic \(authData!.base64EncodedString(options: NSData.Base64EncodingOptions.lineLength64Characters))"
        urlRequest.setValue(authValue, forHTTPHeaderField: "Authorization")
        
        do {
            let jsonData = try JSONSerialization.data(withJSONObject: dict_data, options: [])
            //
            //all fine with jsonData here
            urlRequest.httpBody = jsonData
            print(jsonData)
            
        } catch {
            //handle error
            print(error)
        }
        
        print(urlRequest)
        
        let task = defaultSession.dataTask(with: urlRequest as URLRequest, completionHandler: { data, response, error in
            
            guard error == nil else {
                return
            }
            
            guard let data = data else {
                return
            }
            
            do {
                //create json object from data
                if let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [String: Any] {
//                      print(json)
                    completionHandler(json as Dictionary<String, AnyObject>)
                    // handle json...
                    
                }
                
            } catch let error {
                print(error.localizedDescription)
            }
        })
        
        task.resume()
        
    }
    
    
    static func postRequestWithURL_FormType(_ urlString: String, paramString: String, completionHandler:@escaping (_ response: Dictionary <String, AnyObject>) -> Void) {
        
        
        let defaultConfigObject = URLSessionConfiguration.default
        let defaultSession = URLSession(configuration: defaultConfigObject, delegate: nil, delegateQueue: nil)
        
        let urlRequest = NSMutableURLRequest(url: NSURL(string: urlString)! as URL)
        urlRequest.httpMethod = "POST"
        urlRequest.httpBody = paramString.data(using: String.Encoding.utf8)
        
        // Handling Basic HTTPS Authorization
        let authString = "wsbrandshop:Neuro@123"
        let authData = authString.data(using: String.Encoding.utf8)
        
        let authValue = "Basic \(authData!.base64EncodedString(options: NSData.Base64EncodingOptions.lineLength64Characters))"
        urlRequest.setValue(authValue, forHTTPHeaderField: "Authorization")
        urlRequest.setValue("application/json", forHTTPHeaderField: "Content-Type")
        
        let task = defaultSession.dataTask(with: urlRequest as URLRequest) { ( data, response, error) -> Void in
            
            if error != nil {
                print(urlString)
                print(paramString)
                print("Error occurred: "+(error?.localizedDescription)!)
                Constants.appDelegate.stopIndicator()
                return;
            }
            do {
                let responseObjc = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.mutableContainers) as! [String: AnyObject]
//                print(responseObjc)
                completionHandler(responseObjc)
            }
            catch {
                print(urlString)
                print(paramString)
                
                print("Error occurred parsing data: \(error)")
                completionHandler([:])
            }
        }
        task.resume()
    }

}



