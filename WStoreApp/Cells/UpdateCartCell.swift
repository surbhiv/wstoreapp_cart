//
//  UpdateCartCell.swift
//  WStoreApp
//
//  Created by Surbhi on 01/09/17.
//  Copyright © 2017 SurbhiV. All rights reserved.
//

import UIKit

class UpdateCartCell: UICollectionViewCell {
    @IBOutlet weak var emptyCartButton: UIButton!
    @IBOutlet weak var continueShopButton: UIButton!
    @IBOutlet weak var updateCartButton: UIButton!
    @IBOutlet weak var proceedCheckoutButton: UIButton!
}
