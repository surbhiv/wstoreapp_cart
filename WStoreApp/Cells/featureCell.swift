//
//  featureCell.swift
//  WStoreApp
//
//  Created by Surbhi on 22/06/17.
//  Copyright © 2017 SurbhiV. All rights reserved.
//

import UIKit

class featureCell: UICollectionViewCell {
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var descLabel: UILabel!
    @IBOutlet weak var featureImage: UIImageView!
    @IBOutlet weak var featureweb: UIWebView!

    override func layoutSubviews() {
        super.layoutSubviews()
        titleLabel.sizeToFit()
        descLabel.sizeToFit()
    }
}
