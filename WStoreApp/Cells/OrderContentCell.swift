//
//  OrderContentCell.swift
//  WStoreApp
//
//  Created by Surbhi on 30/08/17.
//  Copyright © 2017 SurbhiV. All rights reserved.
//

import UIKit

class OrderContentCell: UICollectionViewCell {
    
    @IBOutlet weak var orderTable: UITableView!
}
