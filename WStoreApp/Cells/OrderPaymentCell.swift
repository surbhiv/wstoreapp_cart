//
//  OrderPaymentCell.swift
//  WStoreApp
//
//  Created by Surbhi on 30/08/17.
//  Copyright © 2017 SurbhiV. All rights reserved.
//

import UIKit

class OrderPaymentCell: UICollectionViewCell {
    
    @IBOutlet weak var editCartButton: UIButton!
    @IBOutlet weak var changePaymentButton: UIButton!
    @IBOutlet weak var changeShippingAddressButton: UIButton!
    @IBOutlet weak var changeBillingAddressButton: UIButton!
    @IBOutlet weak var shippingAddressLabel: UILabel!
    @IBOutlet weak var billingAddressLabel: UILabel!
    
//editCartButtonPressed
    
}
