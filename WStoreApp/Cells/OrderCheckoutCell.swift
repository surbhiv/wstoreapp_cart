//
//  OrderCheckoutCell.swift
//  WStoreApp
//
//  Created by Surbhi on 30/08/17.
//  Copyright © 2017 SurbhiV. All rights reserved.
//

import UIKit

class OrderCheckoutCell: UICollectionViewCell {
    
    @IBOutlet weak var subTotalLabel: UILabel!
    @IBOutlet weak var shippingChargesLabel: UILabel!
    @IBOutlet weak var grandTotalLabel: UILabel!
    @IBOutlet weak var placeOrderButton: UIButton!
    

    
}
