//
//  OrderDetailcell.swift
//  WStoreApp
//
//  Created by Surbhi on 30/08/17.
//  Copyright © 2017 SurbhiV. All rights reserved.
//

import UIKit

class OrderDetailcell: UITableViewCell {

    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var quantityText: UITextField!
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var subTotalLabel: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
