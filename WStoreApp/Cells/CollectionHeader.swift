//
//  CollectionHeader.swift
//  WStoreApp
//
//  Created by Surbhi on 17/06/17.
//  Copyright © 2017 SurbhiV. All rights reserved.
//

import UIKit

class CollectionHeader: UICollectionReusableView {
 
    @IBOutlet weak var headingLabel: UILabel!
    @IBOutlet weak var listButton: UIButton!
    @IBOutlet weak var gridButton: UIButton!
    @IBOutlet weak var filterButton: UIButton!

}
