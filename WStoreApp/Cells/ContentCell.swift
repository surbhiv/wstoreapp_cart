//
//  ContentCell.swift
//  WStoreApp
//
//  Created by Surbhi on 17/08/17.
//  Copyright © 2017 SurbhiV. All rights reserved.
//

import UIKit

class ContentCell: UITableViewCell {

    @IBOutlet weak var productImage: UIImageView!
    @IBOutlet weak var productName: UILabel!
    @IBOutlet weak var productDescription: UILabel!
    @IBOutlet weak var productPrice: UILabel!
    @IBOutlet weak var productQuantity: UITextField!
    @IBOutlet weak var deleteProduct: UIButton!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
}
