//
//  DiscountCell.swift
//  WStoreApp
//
//  Created by Surbhi on 18/08/17.
//  Copyright © 2017 SurbhiV. All rights reserved.
//

import UIKit

class DiscountCell: UICollectionViewCell {
    
    @IBOutlet weak var discountTextfield: UITextField!
    @IBOutlet weak var applyDiscountButton: UIButton!
}
