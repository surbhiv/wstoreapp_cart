//
//  ProductListCell.swift
//  WStoreApp
//
//  Created by Surbhi on 15/06/17.
//  Copyright © 2017 SurbhiV. All rights reserved.
//

import UIKit

class ProductListCell: UICollectionViewCell {
    
    @IBOutlet weak var prodImage: UIImageView!
    @IBOutlet weak var prodName: UILabel!
    @IBOutlet weak var prodStack: UIStackView!
    @IBOutlet weak var prodOldPrice: UILabel!
    @IBOutlet weak var prodSellingPrice: UILabel!
    @IBOutlet weak var prodStackWidth: NSLayoutConstraint!

    
}
