//
//  ShoppingCartCell.swift
//  WStoreApp
//
//  Created by Surbhi on 17/08/17.
//  Copyright © 2017 SurbhiV. All rights reserved.
//

import UIKit

class ShoppingCartCell: UICollectionViewCell {
    
    @IBOutlet weak var shoppingTable: UITableView!
    @IBOutlet weak var grandTotalLabel: UILabel!
    @IBOutlet weak var appliedDiscountLabel: UILabel!
    @IBOutlet weak var subTotalLabel: UILabel!
    
    @IBOutlet weak var discountLabel: UILabel!
}
