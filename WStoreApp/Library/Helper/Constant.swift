//
//  Constant.swift
//  MyRecipeApp
//
//  Created by Surbhi on 24/10/16.
//  Copyright © 2016 Neuronimbus. All rights reserved.
//

import Foundation
import UIKit


struct ScreenSize {
    static let SCREEN               = UIScreen.main.bounds
    static let SCREEN_WIDTH         = UIScreen.main.bounds.size.width
    static let SCREEN_HEIGHT        = UIScreen.main.bounds.size.height
    static let SCREEN_MAX_LENGTH    = max(ScreenSize.SCREEN_WIDTH, ScreenSize.SCREEN_HEIGHT)
    static let SCREEN_MIN_LENGTH    = min(ScreenSize.SCREEN_WIDTH, ScreenSize.SCREEN_HEIGHT)
}

struct DeviceType {
    static let IS_IPHONE_4_OR_LESS  = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH < 568.0
    static let IS_IPHONE_5          = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH == 568.0
    static let IS_IPHONE_6          = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH == 667.0
    static let IS_IPHONE_6P         = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH == 736.0
    static let IS_IPAD              = UIDevice.current.userInterfaceIdiom == .pad
}

struct iOSVersion {
    static let SYS_VERSION_FLOAT = (UIDevice.current.systemVersion as NSString).floatValue
    static let iOS7 = (iOSVersion.SYS_VERSION_FLOAT < 8.0 && iOSVersion.SYS_VERSION_FLOAT >= 7.0)
    static let iOS8 = (iOSVersion.SYS_VERSION_FLOAT >= 8.0 && iOSVersion.SYS_VERSION_FLOAT < 9.0)
    static let iOS9 = (iOSVersion.SYS_VERSION_FLOAT >= 9.0 && iOSVersion.SYS_VERSION_FLOAT < 10.0)
    static let iOS10 = (iOSVersion.SYS_VERSION_FLOAT >= 10.0 && iOSVersion.SYS_VERSION_FLOAT < 11.0)

}


struct DeviceOrientation {
    static let IS_portT = UIDevice.current.orientation.isPortrait
    static let IS_Land = UIDevice.current.orientation.isLandscape
    
}

struct Constants {
    
    static let pushNotification = "com.neuro.pushNotification"
    static let pushNotificationToUpdate = "sideMenuPushNotification"
    static let mainStoryboard: UIStoryboard =  UIStoryboard(name: "Main_iPad",bundle: nil)
    //(DeviceType.IS_IPAD ? UIStoryboard(name: "Main_iPad",bundle: nil) : UIStoryboard(name: "Main",bundle: nil))
    
//    static let BASEURL = "http://52.66.173.141/brandshop/api/"
    static let BASEURL = "https://www.whirlpoolindiawstore.com/brandshop/api/"

    static let DOCUMENTSDirectoryURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
    
    static let PROFILE = "UserProfile"
    static let PASSWORD = "pwd_Blank"
    
    static let DevToken = "deviceToken"
    static let AUTHID = "auth_id"
    static let AUTHType = "auth_type"
    static let DEVId = "device_id"
    static let UserID = "id"
    static let MOB = "mobile"
    static let NAME = "name"
    static let PROFILEPIC = "profile_pic"
    static let PASS = "password"
    static let STATE = "success"
    static let EMAIL = "email"

    static let catID_array = "CTAEGORYARRAY"
    static let SCREENSAVER = "ScreenSaverArray"
    static let RANGE = "Our_Range"
    static let TOPRANGE = "Top_Category"

    static let RUPEE = "₹"

    static let RecipeOpenCount = "numberOfTimesRecipeDetailOpened"
    
    static let appDelegate = UIApplication.shared.delegate as! AppDelegate
    
    static let CART_Array = "CartArray"
    static let CART_Count = "cartTotalCount"

    
    static let COMMON_IMAGE_TOTAL = "Common_Image_Total_Count"
    static let COLOR_DETAIL_TOTAL = "Color_Detail_Total_Count"
    static let FEATURE_IMAGE_TOTAL = "Feature_Image_Total_Count"
    static let PROD_DETAIL_IMAGE_TOTAL = "Product_Image_Total_Count"
    static let CATEGORY_IMAGE_TOTAL = "CategoryImageCountTotal"
    static let COLOR_LIST_TOTAL = "Color_LIST_Total_Count"
    
    
    static let COMMON_IMAGE_CURRENT = "Common_Image_Current_Count"
    static let COLOR_DETAIL_CURRENT = "Color_Detail_Current_Count"
    static let FEATURE_IMAGE_CURRENT = "Feature_Image_Current_Count"
    static let PROD_DETAIL_IMAGE_CURRENT = "Product_Image_Current_Count"
    static let CATEGORY_IMAGE_CURRENT = "CategoryImageCount"
    static let COLOR_LIST_CURRENT = "Color_List_Current_Count"
    static let DISCOUNTApplied = "discount_amount"
    static let DISCOUNTCoupon = "discount_Coupon"

    static let SUBTotal = "SUBTotalAmount"
    static let GRANDTotal = "GRANDTotalAmount"

    
    
    static let Color_Detail_Noti = "ColorDetailImageNotification"
    static let Color_List_Noti = "ColorListImageNotification"

    static let Feature_Noti = "FeatureImageNotification"
    static let Product_Detail_Noti = "ProductDetailImageNotification"

    
    static let selectedPin = "SLECTEDPIN"
    static let QuoteID = "quote_id"
    static let isTransactionEnabled = "forTransactionMode"
    }


struct Colors {
    static let appNavigationColor =  UIColor(red: 58.0/255.0, green: 58.0/255.0, blue: 60.0/255.0, alpha: 1.0)
    static let Apptextcolor =  UIColor(red: 33.0/255.0, green: 33.0/255.0, blue: 33.0/255.0, alpha: 1.0) // also to be used for TextColor
    static let appThemeColorText = UIColor(red: 238.0/255.0, green:  161.0/255.0, blue: 22.0/255.0, alpha: 1.0)
    static let ShadowColor = UIColor(red: 85.0/255.0, green:  85.0/255.0, blue: 85.0/255.0, alpha: 1.0)
    static let Lighttextcolor = UIColor(red: 240.0/255.0, green:  240.0/255.0, blue: 245.0/255.0, alpha: 1.0)
    static let greenTextColor = UIColor(red: 0.0/255.0, green:  153.0/255.0, blue: 51.0/255.0, alpha: 1.0)
    static let detailYellowTextColor = UIColor(red: 253.0/255.0, green:  198.0/255.0, blue: 45.0/255.0, alpha: 1.0)

    
}


struct FONTS {
    static let Helvetica_Regular = "HelveticaNeue"
    static let Helvetica_Light = "HelveticaNeue-Light"
    static let Helvetica_Medium = "HelveticaNeue-Medium"
    static let Helvetica_Bold = "HelveticaNeue-Bold"
    static let Helvetica_Thin = "HelveticaNeue-Thin"
    static let Helvetica_UltraLight = "HelveticaNeue-UltraLight"
    static let Helvetica_Italic = "HelveticaNeue-Italic"
    static let Helvetica_LightItalic = "HelveticaNeue-LightItalic"
    static let Helvetica_MediumItalic = "HelveticaNeue-MediumItalic"
    static let Helvetica_ThinItalic = "HelveticaNeue-ThinItalic"
    static let Helvetica_UltraLightItalic = "HelveticaNeue-UltraLightItalic"
    static let Helvetica_BoldItalic = "HelveticaNeue-BoldItalic"
    static let Helvetica_CondensedBlack = "HelveticaNeue-CondensedBlack"
    static let Helvetica_CondensedBold = "HelveticaNeue-CondensedBold"

}
