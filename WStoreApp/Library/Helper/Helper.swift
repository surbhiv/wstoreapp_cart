//
//  Helper.swift
//  NBA
//
//  Created by HiteshDhawan on 12/04/16.
//  Copyright © 2016 Neuronimbus. All rights reserved.
//

import UIKit
import AVFoundation
import AVKit
import Photos

class Helper: NSObject,NSCoding {
    private var eventTitle: String!
    private var eventLocation: String!
    
    static func isValidEmail(_ emailString: String) -> Bool {
        
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
        let emailPredicate = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailPredicate.evaluate(with: emailString)
        
    }
    
    static func isValidPhoneNumber(_ phoneNumber: String) -> Bool {
        
        let numberOnly = CharacterSet.init(charactersIn: "0123456789")
        let charactersetFromTF = CharacterSet.init(charactersIn: phoneNumber)
        let stringIsValid = numberOnly.isSuperset(of: charactersetFromTF)

        if phoneNumber.characters.count == 10 && stringIsValid == true{
            return true
        }
        return false
    }
    
    static func isValidPincode(value: String) -> Bool {
        
        let PHONE_REGEX = "^\\d{6}$"
        let phoneTest = NSPredicate(format: "SELF MATCHES %@", PHONE_REGEX)
        let result =  phoneTest.evaluate(with: value)
        print(result)
        
        let numberOnly = CharacterSet.init(charactersIn: "0123456789")
        
        
        if value.characters.count == 6  && result == true{
            return true
        }
        else{
            return false
        }
    }
    
    static func setImageOntextField(_ textField: UITextField){
        
        let imageV = UIImageView(frame:CGRect(x: 0, y: 0, width: 15, height: 15))
        imageV.image = UIImage.init(named: "downArrow")
        textField.rightViewMode = UITextFieldViewMode.always
        textField.rightView = imageV
    }
    
    static func activatetextField(_ lineLabel:UILabel, height : NSLayoutConstraint){
        
        lineLabel.backgroundColor = Colors.appThemeColorText
        height.constant = 2.0
    }
    
    static func deActivatetextField(_ lineLabel:UILabel, height : NSLayoutConstraint){
        
        lineLabel.backgroundColor = Colors.Apptextcolor
        height.constant = 1.0
        
    }
    
    
    static func loadImageFromUrl(_ urls: String, view: UIImageView, width:CGFloat){
        
        // Create Url from string
        
        if  urls == ""{
            
            view.image = UIImage.init(named: "no_image.png")
        }
        else
        {
            let url = URL(string: urls)!

            
            let task = URLSession.shared.dataTask(with: url, completionHandler: { (responseData, responseUrl, error) -> Void in

                if let data = responseData{
                    
                    // execute in UI thread
                    DispatchQueue.main.async(execute: { () -> Void in
                        let img = UIImage(data: data)

                        if img != nil
                        {
                            let newImg = self.resizeImage(img!, newWidth: width)
                            view.image = newImg
                        }
                        else
                        {
                            view.image = UIImage.init(named: "profile_PlaceHolder.png")
                        }
                    })
                }
            }) 
            
            // Run task
            task.resume()
        }
    }
    
    
    static func loadImageFromUrlWithIndicator(_ url: String, view: UIImageView, indic : UIActivityIndicatorView){
        
        // Create Url from string
        let url = URL(string: url)!
        
        // Download task:
        // - sharedSession = global NSURLCache, NSHTTPCookieStorage and NSURLCredentialStorage objects.
        let task = URLSession.shared.dataTask(with: url, completionHandler: { (responseData, responseUrl, error) -> Void in
            // if responseData is not null...
            if let data = responseData{
                
                // execute in UI thread
                DispatchQueue.main.async(execute: { () -> Void in
                    view.image = UIImage(data: data)
                    indic.stopAnimating()
                })
            }
        }) 
        
        // Run task
        task.resume()
    }

    
    static func loadImageFromUrlWithIndicatorAndSize(_ url: String, view: UIImageView, indic : UIActivityIndicatorView, size: CGSize){
        
        // Create Url from string
        let url = URL(string: url)!
        
        // Download task:
        // - sharedSession = global NSURLCache, NSHTTPCookieStorage and NSURLCredentialStorage objects.
        let task = URLSession.shared.dataTask(with: url, completionHandler: { (responseData, responseUrl, error) -> Void in
            // if responseData is not null...
            if let data = responseData{
                
                // execute in UI thread
                DispatchQueue.main.async(execute: { () -> Void in
                    
                    let img = UIImage(data: data)
                    if img != nil
                    {
                        let newImage = self.resizeImage(img!, newWidth: size.width)
                        view.image = newImage
                    }
                    else
                    {
                        view.image = UIImage.init(named: "no_image.png")

                    }
                    indic.stopAnimating()
                })
            }
        }) 
        
        // Run task
        task.resume()
    }

    
    
    static func md5(string: String) -> String {
        var digest = [UInt8](repeating: 0, count: Int(CC_MD5_DIGEST_LENGTH))
        if let data = string.data(using: String.Encoding.utf8) {
            CC_MD5((data as NSData).bytes, CC_LONG(data.count), &digest)
        }
        
        var digestHex = ""
        for index in 0..<Int(CC_MD5_DIGEST_LENGTH) {
            digestHex += String(format: "%02x", digest[index])
        }
        
        return digestHex
    }
    
    static func resizeImage(_ image: UIImage, newWidth: CGFloat) -> UIImage {
        
        let scale = newWidth / image.size.height
        let newHeight = image.size.width * scale
        UIGraphicsBeginImageContext(CGSize(width: newHeight,height: newWidth))
        image.draw(in: CGRect(x: 0, y: 0, width: newHeight, height: newWidth))
        
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return newImage!
    }
    

    
    static func getOpenCount() -> Int {
        let launches = UserDefaults.standard.integer(forKey: Constants.RecipeOpenCount)
        return launches
    }
    
    static func incrementOpenCount(){
        var launches = UserDefaults.standard.integer(forKey: Constants.RecipeOpenCount)
        launches += 1
        UserDefaults.standard.set(launches, forKey: Constants.RecipeOpenCount)
        UserDefaults.standard.synchronize()
    }
    
    static func resetCount(){
        var launches = UserDefaults.standard.integer(forKey: Constants.RecipeOpenCount)
        launches = 0
        UserDefaults.standard.set(launches, forKey: Constants.RecipeOpenCount)
        UserDefaults.standard.synchronize()
    }
    
    
    static func giveShadowToView(_ shadowView : UIView) -> UIView {
        shadowView.layer.shadowColor = UIColor.gray.cgColor
        shadowView.layer.shadowOpacity = 1
        shadowView.layer.shadowOffset = CGSize.zero
        shadowView.layer.shadowRadius = 2
        shadowView.layer.cornerRadius = 5
        
        
        shadowView.layer.masksToBounds = true
        return shadowView
    }
    
    static func getHeightForText(_ text : String , fon : UIFont, width : CGFloat) -> CGFloat{
        
        
        let label_new:UILabel = UILabel(frame: CGRect(x: 0, y: 0, width: width, height: CGFloat.greatestFiniteMagnitude))
        label_new.numberOfLines = 0
        label_new.lineBreakMode = NSLineBreakMode.byTruncatingTail
        label_new.font = fon
        label_new.text = text
        label_new.sizeToFit()
        return label_new.frame.height
        
    }
    static func getWidthForText(_ text : String , fon : UIFont , height : CGFloat) -> CGFloat{
        
        let label_new:UILabel = UILabel(frame: CGRect(x: 0, y: 0, width: height, height: CGFloat.greatestFiniteMagnitude))
        label_new.numberOfLines = 0
        label_new.lineBreakMode = NSLineBreakMode.byTruncatingTail
        label_new.font = fon
        label_new.text = text
        label_new.sizeToFit()
        return label_new.frame.width
        
    }
    
    
    static func convertToDictionary(text: String) -> [String: Any]? {
        if let data = text.data(using: .utf8) {
            do {
                return try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any]
            } catch {
                print(error.localizedDescription)
            }
        }
        return nil
    }
    
    required convenience init?(coder aDecoder: NSCoder) {
        self.init()
        eventTitle = aDecoder.decodeObject(forKey: "eventTitle") as? String
        eventLocation = aDecoder.decodeObject(forKey: "eventLocation") as? String
    }
    
    func encode(with aCoder: NSCoder) {
        aCoder.encode(eventTitle, forKey: "eventTitle")
        aCoder.encode(eventLocation, forKey: "eventLocation")
    }
    
    static func SaveArrayToUserDefaults(arr : Array<Any> , keyName : String){
        let data = NSKeyedArchiver.archivedData(withRootObject: arr)
        // This calls the encode function of your Event class
        
        UserDefaults.standard.set(data, forKey: keyName)
        /* This saves the object to a buffer, but you will need to call synchronize,
         before it is actually saved to UserDefaults */
        
        UserDefaults.standard.synchronize()
    }
    
    static func fetchArrayFromUserDefaults(keyName : String){//->Array<Any>
        if let data = UserDefaults.standard.object(forKey: keyName) as? Data {
            if let storedData = NSKeyedUnarchiver.unarchiveObject(with: data) as? [Helper] {
                // In here you can access your array
                print(storedData)
            }
        }
    }
    
    static func downloadVideoLinkAndCreateAsset(_ videoLink: String, objectNum : NSInteger) {
        
        // use guard to make sure you have a valid url
        guard let videoURL = URL(string: videoLink) else { return }
        
        let documentsDirectoryURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
        
        // check if the file already exist at the destination folder if you don't want to download it twice
        if !FileManager.default.fileExists(atPath: documentsDirectoryURL.appendingPathComponent(videoURL.lastPathComponent).path) {
            
            // set up your download task
            URLSession.shared.downloadTask(with: videoURL) { (location, response, error) -> Void in
                
                // use guard to unwrap your optional url
                guard let location = location else { return }
                
                // create a deatination url with the server response suggested file name
                let destinationURL = documentsDirectoryURL.appendingPathComponent(response?.suggestedFilename ?? videoURL.lastPathComponent)
                
                do {
                    
                    try FileManager.default.moveItem(at: location, to: destinationURL)
                    
                    PHPhotoLibrary.requestAuthorization({ (authorizationStatus: PHAuthorizationStatus) -> Void in
                        
                        // check if user authorized access photos for your app
                        if authorizationStatus == .authorized {
                            PHPhotoLibrary.shared().performChanges({
                                PHAssetChangeRequest.creationRequestForAssetFromVideo(atFileURL: destinationURL)}) { completed, error in
                                    if completed {
                                        print("Video asset created")
                                        
                                    } else {
                                        print(error ?? "Some error Occurred. Video Asset creation failed.")
                                    }
                                    
                                    // post Notification
                                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "videoDownloadingComplete"), object: objectNum, userInfo: nil)
                            }
                        }
                    })
                    
                } catch let error as NSError {
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "videoDownloadingComplete"), object: objectNum, userInfo: nil)

                    print(error.localizedDescription)}
                
                }.resume()
            
        }
        else {
            //delete the video
            

            
            
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "videoDownloadingComplete"), object: videoLink, userInfo: nil)

            print("File already exists at destination url")
        }
    }

    static func downloadImageLinkAndCreateAsset(_ imageLink: String, notificationName : String, objectNUm : NSInteger, isLast : Bool) {//, screen: String, type : String
        
        // use guard to make sure you have a valid url
        guard let imageURL = URL(string: imageLink) else { return }
        
        let documentsDirectoryURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
        
        // check if the file already exist at the destination folder if you don't want to download it twice
        if !FileManager.default.fileExists(atPath: documentsDirectoryURL.appendingPathComponent(imageURL.lastPathComponent).path) {
            
            // set up your download task
            URLSession.shared.downloadTask(with: imageURL) { (location, response, error) -> Void in
                
                // use guard to unwrap your optional url
                guard let location = location else { return }
                
                // create a deatination url with the server response suggested file name
                let destinationURL = documentsDirectoryURL.appendingPathComponent(response?.suggestedFilename ?? imageURL.lastPathComponent)
                
                do {
                    
                    try FileManager.default.moveItem(at: location, to: destinationURL)
                    
                    PHPhotoLibrary.requestAuthorization({ (authorizationStatus: PHAuthorizationStatus) -> Void in
                        
                        // check if user authorized access photos for your app
                        if authorizationStatus == .authorized {
                            PHPhotoLibrary.shared().performChanges({
                                PHAssetChangeRequest.creationRequestForAssetFromImage(atFileURL: destinationURL) }) { completed, error in
                                    if completed {
                                        print("Image asset created")
                                        
                                    } else {
                                        print(error ?? "Some error Occurred. Image Asset creation failed.")
                                    }
                                    
                                    if notificationName == "Category_ImageDownloadComplete"{
                                        var current = UserDefaults.standard.integer(forKey:  Constants.CATEGORY_IMAGE_CURRENT)
                                        current = current + 1
                                        UserDefaults.standard.set(current, forKey:  Constants.CATEGORY_IMAGE_CURRENT)
                                    }
                                    if notificationName == "ProductList_ImageDownloadComplete"{
                                        var current = UserDefaults.standard.integer(forKey: "ProductListImageCount")
                                        current = current + 1
                                        UserDefaults.standard.set(current, forKey: "ProductListImageCount")
                                    }
                                    if notificationName == Constants.Color_List_Noti{
                                        var current = UserDefaults.standard.integer(forKey: Constants.COLOR_LIST_CURRENT)
                                        current = current + 1
                                        UserDefaults.standard.set(current, forKey: Constants.COLOR_LIST_CURRENT)
                                    }
                                    if notificationName == Constants.Color_Detail_Noti{
                                        var current = UserDefaults.standard.integer(forKey: Constants.COLOR_DETAIL_CURRENT)
                                        current = current + 1
                                        UserDefaults.standard.set(current, forKey: Constants.COLOR_DETAIL_CURRENT)
                                    }
                                    if notificationName == Constants.Feature_Noti{
                                        var current = UserDefaults.standard.integer(forKey: Constants.FEATURE_IMAGE_CURRENT)
                                        current = current + 1
                                        UserDefaults.standard.set(current, forKey: Constants.FEATURE_IMAGE_CURRENT)
                                    }
                                    if notificationName == Constants.Product_Detail_Noti{
                                        var current = UserDefaults.standard.integer(forKey: Constants.PROD_DETAIL_IMAGE_CURRENT)
                                        current = current + 1
                                        UserDefaults.standard.set(current, forKey: Constants.PROD_DETAIL_IMAGE_CURRENT)
                                    }


                                    
                                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: notificationName), object: objectNUm, userInfo: nil)

                                    
                            }
                        }
                    })
                    
                } catch let error as NSError {
                    
                    if notificationName == "Category_ImageDownloadComplete"{
                        var current = UserDefaults.standard.integer(forKey:  Constants.CATEGORY_IMAGE_CURRENT)
                        current = current + 1
                        UserDefaults.standard.set(current, forKey:  Constants.CATEGORY_IMAGE_CURRENT)
                    }
                    if notificationName == "ProductList_ImageDownloadComplete"{
                        var current = UserDefaults.standard.integer(forKey: "ProductListImageCount")
                        current = current + 1
                        UserDefaults.standard.set(current, forKey: "ProductListImageCount")
                    }
                    
                    if notificationName == Constants.Color_List_Noti{
                        var current = UserDefaults.standard.integer(forKey: Constants.COLOR_LIST_CURRENT)
                        current = current + 1
                        UserDefaults.standard.set(current, forKey: Constants.COLOR_LIST_CURRENT)
                    }
                    if notificationName == Constants.Color_Detail_Noti{
                        var current = UserDefaults.standard.integer(forKey: Constants.COLOR_DETAIL_CURRENT)
                        current = current + 1
                        UserDefaults.standard.set(current, forKey: Constants.COLOR_DETAIL_CURRENT)
                    }
                    if notificationName == Constants.Feature_Noti{
                        var current = UserDefaults.standard.integer(forKey: Constants.FEATURE_IMAGE_CURRENT)
                        current = current + 1
                        UserDefaults.standard.set(current, forKey: Constants.FEATURE_IMAGE_CURRENT)
                    }
                    if notificationName == Constants.Product_Detail_Noti{
                        var current = UserDefaults.standard.integer(forKey: Constants.PROD_DETAIL_IMAGE_CURRENT)
                        current = current + 1
                        UserDefaults.standard.set(current, forKey: Constants.PROD_DETAIL_IMAGE_CURRENT)
                    }


                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: notificationName), object: objectNUm, userInfo: nil)

                    print(error)}
                
                }.resume()
            
        }
        else {
            
            if notificationName == "Category_ImageDownloadComplete"{
                var current = UserDefaults.standard.integer(forKey:  Constants.CATEGORY_IMAGE_CURRENT)
                current = current + 1
                UserDefaults.standard.set(current, forKey:  Constants.CATEGORY_IMAGE_CURRENT)
            }
            if notificationName == "ProductList_ImageDownloadComplete"{
                var current = UserDefaults.standard.integer(forKey: "ProductListImageCount")
                current = current + 1
                UserDefaults.standard.set(current, forKey: "ProductListImageCount")
            }
            if notificationName == Constants.Color_List_Noti{
                var current = UserDefaults.standard.integer(forKey: Constants.COLOR_LIST_CURRENT)
                current = current + 1
                UserDefaults.standard.set(current, forKey: Constants.COLOR_LIST_CURRENT)
            }
            if notificationName == Constants.Color_Detail_Noti{
                var current = UserDefaults.standard.integer(forKey: Constants.COLOR_DETAIL_CURRENT)
                current = current + 1
                UserDefaults.standard.set(current, forKey: Constants.COLOR_DETAIL_CURRENT)
            }
            if notificationName == Constants.Feature_Noti{
                var current = UserDefaults.standard.integer(forKey: Constants.FEATURE_IMAGE_CURRENT)
                current = current + 1
                UserDefaults.standard.set(current, forKey: Constants.FEATURE_IMAGE_CURRENT)
            }
            if notificationName == Constants.Product_Detail_Noti{
                var current = UserDefaults.standard.integer(forKey: Constants.PROD_DETAIL_IMAGE_CURRENT)
                current = current + 1
                UserDefaults.standard.set(current, forKey: Constants.PROD_DETAIL_IMAGE_CURRENT)
            }


            NotificationCenter.default.post(name: NSNotification.Name(rawValue: notificationName), object: objectNUm, userInfo: nil)


            print("File already exists at destination url")
        }
    }

    static func downloadImageLinkAndCreateAsset(_ imageLink: String) {//, screen: String, type : String
        
        // use guard to make sure you have a valid url
        guard let imageURL = URL(string: imageLink) else { return }
        
        let documentsDirectoryURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
        
        // check if the file already exist at the destination folder if you don't want to download it twice
        if !FileManager.default.fileExists(atPath: documentsDirectoryURL.appendingPathComponent(imageURL.lastPathComponent).path) {
            
            // set up your download task
            URLSession.shared.downloadTask(with: imageURL) { (location, response, error) -> Void in
                
                // use guard to unwrap your optional url
                guard let location = location else { return }
                
                // create a deatination url with the server response suggested file name
                let destinationURL = documentsDirectoryURL.appendingPathComponent(response?.suggestedFilename ?? imageURL.lastPathComponent)
                
                do {
                    
                    try FileManager.default.moveItem(at: location, to: destinationURL)
                    
                    PHPhotoLibrary.requestAuthorization({ (authorizationStatus: PHAuthorizationStatus) -> Void in
                        
                        // check if user authorized access photos for your app
                        if authorizationStatus == .authorized {
                            PHPhotoLibrary.shared().performChanges({
                                PHAssetChangeRequest.creationRequestForAssetFromImage(atFileURL: destinationURL) }) { completed, error in
                                    if completed {
                                        print("Image asset created")
                                        
                                    } else {
                                        print(error ?? "Some error Occurred. Image Asset creation failed.")
                                    }
                            }
                        }
                    })
                    
                } catch let error as NSError {
                    
                    print(error.localizedDescription)}
                
                }.resume()
            
        }
        else {
            print("File already exists at destination url")
        }
    }
    
    static func findAlbum(albumName: String) -> PHAssetCollection? {
        let fetchOptions = PHFetchOptions()
        fetchOptions.predicate = NSPredicate(format: "title = %@", albumName)
        let fetchResult : PHFetchResult = PHAssetCollection.fetchAssetCollections(with: .album, subtype: .any, options: fetchOptions)
        guard let photoAlbum = fetchResult.firstObject else {
            return nil
        }
        return photoAlbum
    }

    func saveImage(image: UIImage, album: PHAssetCollection, completion:((PHAsset?)->())? = nil) {
        var placeholder: PHObjectPlaceholder?
        PHPhotoLibrary.shared().performChanges({
            let createAssetRequest = PHAssetChangeRequest.creationRequestForAsset(from: image)
            guard let albumChangeRequest = PHAssetCollectionChangeRequest(for: album),
                let photoPlaceholder = createAssetRequest.placeholderForCreatedAsset else { return }
            placeholder = photoPlaceholder
            let fastEnumeration = NSArray(array: [photoPlaceholder] as [PHObjectPlaceholder])
            albumChangeRequest.addAssets(fastEnumeration)
        }, completionHandler: { success, error in
            guard let placeholder = placeholder else {
                completion?(nil)
                return
            }
            if success {
                let assets:PHFetchResult<PHAsset> =  PHAsset.fetchAssets(withLocalIdentifiers: [placeholder.localIdentifier], options: nil)
                let asset:PHAsset? = assets.firstObject
                completion?(asset)
            } else {
                completion?(nil)
            }
        })
    }


    static func downloadImageAsset(_ imageLink: String,objectNUm : NSInteger, completionHandler:@escaping (_ response: NSInteger) -> Void) {
        
        // use guard to make sure you have a valid url
        guard let imageURL = URL(string: imageLink) else { return }
        
        let documentsDirectoryURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
        
        // check if the file already exist at the destination folder if you don't want to download it twice
        if !FileManager.default.fileExists(atPath: documentsDirectoryURL.appendingPathComponent(imageURL.lastPathComponent).path) {
            
            // set up your download task
            URLSession.shared.downloadTask(with: imageURL) { (location, response, error) -> Void in
                
                // use guard to unwrap your optional url
                guard let location = location else { return }
                
                // create a deatination url with the server response suggested file name
                let destinationURL = documentsDirectoryURL.appendingPathComponent(response?.suggestedFilename ?? imageURL.lastPathComponent)
                
                do {
                    
                    try FileManager.default.moveItem(at: location, to: destinationURL)
                    
                    PHPhotoLibrary.requestAuthorization({ (authorizationStatus: PHAuthorizationStatus) -> Void in
                        
                        // check if user authorized access photos for your app
                        if authorizationStatus == .authorized {
                            PHPhotoLibrary.shared().performChanges({
                                PHAssetChangeRequest.creationRequestForAssetFromImage(atFileURL: destinationURL) }) { completed, error in
                                    if completed {
                                        print("Image asset created")
                                        
                                    } else {
                                        print(error ?? "Some error Occurred. Image Asset creation failed.")
                                    }
                                    
                                    completionHandler(objectNUm)
 
                            }
                        }
                    })
                    
                } catch let error as NSError {
                    
                    print(error)}
                completionHandler(objectNUm)

                }.resume()
            
        }
        else {
            
            print("File already exists at destination url")
            completionHandler(objectNUm)
        }
    }

    static func getRandomString(length: Int) -> String {
        
        let letters : NSString = "0123456789"
        let len = UInt32(letters.length)
        
        var randomString = ""
        
        for _ in 0 ..< length {
            let rand = arc4random_uniform(len)
            var nextChar = letters.character(at: Int(rand))
            randomString += NSString(characters: &nextChar, length: 1) as String
        }
        
        return randomString
    }
    
    
//    - (NSString *) createSHA512:(NSString *)source {
//    
//    const char *s = [source cStringUsingEncoding:NSASCIIStringEncoding];
//    
//    NSData *keyData = [NSData dataWithBytes:s length:strlen(s)];
//    
//    uint8_t digest[CC_SHA512_DIGEST_LENGTH] = {0};
//    
//    CC_SHA512(keyData.bytes, (CC_LONG)keyData.length, digest);
//    
//    NSData *output = [NSData dataWithBytes:digest length:CC_SHA512_DIGEST_LENGTH];
//    NSLog(@"out --------- %@",output);
//    
//    NSString *str = [[[[output description]stringByReplacingOccurrencesOfString:@"<" withString:@""]stringByReplacingOccurrencesOfString:@">" withString:@""]stringByReplacingOccurrencesOfString:@" " withString:@""];
//    return str;
//    }

}

