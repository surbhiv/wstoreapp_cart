//
//  Loader.swift
//  CircleK
//
//  Created by Hitesh Dhawan on 18/07/18.
//  Copyright © 2018 Varun Tyagi. All rights reserved.
//

import Foundation
import UIKit

class Loader: UIView  {
    
    var activityView1  = UIView()
    var back = UIImageView()
    var bgView = UIView()
    var imageForR = UIImageView ()
    var load = UILabel()

    class var shared: Loader {
        struct Static {
            static let instance: Loader = Loader()
        }
        return Static.instance
    }
    
    override init(frame: CGRect) {
        
        super.init(frame: frame)
        
        activityView1 = UIView.init(frame: CGRect(x: 0, y: 0, width: SCREEN_MAX_LENGTH, height: SCREEN_MIN_LENGTH))
        activityView1.backgroundColor = UIColor.clear
        
        back = UIImageView.init(frame: CGRect(x: 0, y: 0, width: SCREEN_MAX_LENGTH, height: SCREEN_MIN_LENGTH))
        back.backgroundColor = UIColor.black
        back.alpha = 0.5
        activityView1.addSubview(back)
        
        bgView = UIView.init(frame: CGRect(x: (SCREEN_MAX_LENGTH - 120)/2,y: (SCREEN_MIN_LENGTH - 150)/2, width: 120,height: 150))
        bgView.backgroundColor = Colors.appNavigationColor
        bgView.layer.cornerRadius = 18.0
        bgView.layer.shadowColor = Colors.Lighttextcolor.cgColor
        bgView.clipsToBounds = true
        
        let bgImage = UIImageView.init(frame: CGRect(x: 36,y: 36, width: 48,height: 48))
        bgImage.backgroundColor = UIColor.clear
        bgImage.image = UIImage.init(named: "loader_icon.png")
        
        imageForR = UIImageView.init(frame: CGRect(x: 25,y: 25, width: 70,height: 70))
        imageForR.image = UIImage.init(named: "mouse.png")
        imageForR.backgroundColor = UIColor.clear
        
        load = UILabel.init(frame: CGRect(x: 0, y: 120, width: 120, height: 25))
        load.text = "LOADING..."
        load.textColor = UIColor.white
        load.font = UIFont.boldSystemFont(ofSize: 14.0)
        load.textAlignment = NSTextAlignment.center
        
        bgView.addSubview(imageForR)
        bgView.addSubview(bgImage)
        bgView.addSubview(load)
        
        activityView1.addSubview(bgView)
        print(self.window!)
        self.window?.addSubview(activityView1)
        self.window?.sendSubview(toBack: activityView1)
    }
    
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    

    func startIndicator() {
        DispatchQueue.main.async(execute: {
        
            Constants.appDelegate.window?.addSubview(self.activityView1)
            Constants.appDelegate.window?.bringSubview(toFront: self.activityView1)
        })
    }
    
    func stopIndicator() {
        DispatchQueue.main.async(execute: {
            Constants.appDelegate.window?.willRemoveSubview(self.activityView1)
            self.activityView1.removeFromSuperview()
        })
    }
    
    func rotate360Degrees() {
        let rotate = CABasicAnimation(keyPath: "transform.rotation.y")
        rotate.fromValue = nil
        rotate.toValue = 180
        rotate.duration = 50
        rotate.repeatCount = 100.0
        imageForR.layer.add(rotate, forKey: "myRotationAnimation")
    }
}



