//
//  MenuTableView.swift
//  TestSwiftApp
//
//  Created by Surbhi on 24/10/16.
//  Copyright © 2016 Neuronimbus. All rights reserved.
//

import UIKit
import SafariServices

class MenuTableView: UITableViewController,SFSafariViewControllerDelegate {
    
    var menuArray : NSMutableArray = [ "Home", "Refrigerator", "Washing Machines", "Air Conditioner", "Microwave Oven", "Water Purifier", "Accessories", "Built-In Appliances","Air Purifiers", "Small Domestic Appliances","Logout","Update" ]
    
    var selectedMenuItem : Int = 0
    var lastSelectedMenuItem : Int = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        NotificationCenter.default.addObserver(self, selector: #selector(ReloadMenuView), name: NSNotification.Name(rawValue: Constants.pushNotificationToUpdate), object: nil)
        
        
        self.navigationController?.isNavigationBarHidden = true

        // Customize apperance of table view
        tableView.contentInset = UIEdgeInsetsMake(0, 0, 0, 0) //
        tableView.separatorStyle = .none
        tableView.backgroundColor = UIColor.clear
        tableView.scrollsToTop = false
        
        self.view.backgroundColor = UIColor.white
        
        // Preserve selection between presentations
        self.clearsSelectionOnViewWillAppear = false
        
        tableView.selectRow(at: IndexPath(row: selectedMenuItem, section: 0), animated: false, scrollPosition: .middle)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
    }
    
   
    
    func  UpdateMenuView(){
        
        tableView.reloadData()
    }
    
    
    func  ReloadMenuView(){
        
        tableView.reloadData()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Table view data source
    
    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        
        return 0
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        // Return the number of sections.
        return 1
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if DeviceType.IS_IPHONE_6 || DeviceType.IS_IPHONE_6P
        {
            return 45;
        }
        else if DeviceType.IS_IPAD{
            return 60
        }
        return 40;
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // Return the number of rows in the section.
        return menuArray.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        var cell = tableView.dequeueReusableCell(withIdentifier: "CELL")
        
    
        if (cell == nil) {
            cell = UITableViewCell(style: UITableViewCellStyle.default, reuseIdentifier: "CELL")
            cell!.backgroundColor = UIColor.clear
            cell!.textLabel?.textColor = UIColor.black
            cell!.textLabel?.font = UIFont(name: "Helvetica", size: 13)!
            
            if DeviceType.IS_IPHONE_6 || DeviceType.IS_IPHONE_6P
            {
                cell!.textLabel?.font = UIFont(name: "Helvetica", size: 14.5)!
            }
            if DeviceType.IS_IPAD {
                cell!.textLabel?.font = UIFont(name: "Helvetica", size: 18.0)!
            }

            
            let selectedBackgroundView = UIView(frame: CGRect(x: 0, y: 0, width: cell!.frame.size.width, height: cell!.frame.size.height - 4.5))
            selectedBackgroundView.backgroundColor = UIColor.gray.withAlphaComponent(0.1)
            cell!.selectedBackgroundView = selectedBackgroundView
        }

        cell!.textLabel?.text = menuArray[indexPath.row] as? String
//        cell?.imageView?.image = UIImage.init(named: (menuImageArray[indexPath.row] as? String)!)
        
        if (cell?.contentView.viewWithTag(11)) != nil
        {
            let vw = cell?.contentView.viewWithTag(11)
            vw?.removeFromSuperview()
        }
        
        
        let line = UILabel.init(frame: CGRect(x: 0, y: (cell?.frame.size.height)! - 3.5, width: (cell?.frame.size.width)!, height: 1))
        line.tag = 11
        
        if DeviceType.IS_IPAD {
            line.frame = CGRect(x: 0, y: (cell!.textLabel!.frame.minY  + cell!.textLabel!.frame.size.height) + 1, width: (cell?.frame.size.width)!, height: 1.5)
        }
        line.backgroundColor =  Colors.Apptextcolor.withAlphaComponent(0.5)
        cell?.contentView.addSubview(line)
        
        
        if (cell?.contentView.viewWithTag(12)) != nil
        {
            let vw = cell?.contentView.viewWithTag(12)
            vw?.removeFromSuperview()
        }
        
        
        return cell!
    }
    
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if (indexPath.row == selectedMenuItem && lastSelectedMenuItem != selectedMenuItem){
            
            hideSideMenuView()
            return
        }

        //Present new view controller
//        var destViewController : UIViewController
        switch (indexPath.row) {
        case 0:
            let destViewController = Constants.mainStoryboard.instantiateViewController(withIdentifier: "DashBoard") as! DashBoardVC
            lastSelectedMenuItem = selectedMenuItem
            selectedMenuItem = indexPath.row
            destViewController.isViaSideMenu = true
            sideMenuController()?.setContentViewController(destViewController)
            break

        case 1: // REFRIGERATORS
            let destViewController = Constants.mainStoryboard.instantiateViewController(withIdentifier: "SubSubCategoryVC") as! SubSubCategoryVC
            lastSelectedMenuItem = selectedMenuItem
            selectedMenuItem = indexPath.row
            destViewController.topSubCatType = self.menuArray[indexPath.row] as! String
            destViewController.isDirectView = true
            destViewController.catID = "3"
            sideMenuController()?.setContentViewController(destViewController)
            break
            
        case 2: // WASHING MACHINES
            let destViewController = Constants.mainStoryboard.instantiateViewController(withIdentifier: "SubSubCategoryVC") as! SubSubCategoryVC
            lastSelectedMenuItem = selectedMenuItem
            selectedMenuItem = indexPath.row
            destViewController.topSubCatType = self.menuArray[indexPath.row] as! String
            destViewController.isDirectView = true
            destViewController.catID = "9"
            sideMenuController()?.setContentViewController(destViewController)
            break

        case 3: // AC
            let destViewController = Constants.mainStoryboard.instantiateViewController(withIdentifier: "SubSubCategoryVC") as! SubSubCategoryVC
            lastSelectedMenuItem = selectedMenuItem
            selectedMenuItem = indexPath.row
            destViewController.topSubCatType = self.menuArray[indexPath.row] as! String
            destViewController.isDirectView = true
            destViewController.catID = "10"
            sideMenuController()?.setContentViewController(destViewController)
            break
            
        case 4: // MICROWAVE
            let destViewController = Constants.mainStoryboard.instantiateViewController(withIdentifier: "SubSubCategoryVC") as! SubSubCategoryVC
            lastSelectedMenuItem = selectedMenuItem
            selectedMenuItem = indexPath.row
            destViewController.topSubCatType = self.menuArray[indexPath.row] as! String
            destViewController.isDirectView = true
            destViewController.catID = "11"
            sideMenuController()?.setContentViewController(destViewController)
            break
            
        case 5: // WATER PURIFIER
            let destViewController = Constants.mainStoryboard.instantiateViewController(withIdentifier: "SubSubCategoryVC") as! SubSubCategoryVC
            lastSelectedMenuItem = selectedMenuItem
            selectedMenuItem = indexPath.row
            destViewController.topSubCatType = self.menuArray[indexPath.row] as! String
            destViewController.isDirectView = true
            destViewController.catID = "12"
            sideMenuController()?.setContentViewController(destViewController)
            break
            
        case 6: // ACCESSORY
            let destViewController = Constants.mainStoryboard.instantiateViewController(withIdentifier: "SubSubCategoryVC") as! SubSubCategoryVC
            lastSelectedMenuItem = selectedMenuItem
            selectedMenuItem = indexPath.row
            destViewController.topSubCatType = self.menuArray[indexPath.row] as! String
            destViewController.isDirectView = true
            destViewController.catID = "17"
            sideMenuController()?.setContentViewController(destViewController)
            break


        case 7: // BUILT IN
            let destViewController = Constants.mainStoryboard.instantiateViewController(withIdentifier: "SubSubCategoryVC") as! SubSubCategoryVC
            lastSelectedMenuItem = selectedMenuItem
            selectedMenuItem = indexPath.row
            destViewController.topSubCatType = self.menuArray[indexPath.row] as! String
            destViewController.isDirectView = true
            destViewController.catID = "22"
            sideMenuController()?.setContentViewController(destViewController)
            break
            
        case 8: // AIR PURIFIERS
            
            let dict = UserDefaults.standard.value(forKey: "72") as! Dictionary<String,Any>
            let isNextCategory = dict["type"] as! NSString
            if isNextCategory == "product"{
                lastSelectedMenuItem = selectedMenuItem
                selectedMenuItem = indexPath.row

                let destViewController = Constants.mainStoryboard.instantiateViewController(withIdentifier: "ProductListVC") as! ProductListVC
                destViewController.ProductType = dict["category"] as! String
                destViewController.catID = "72"
                destViewController.isDirectView = true
                sideMenuController()?.setContentViewController(destViewController)

            }
            else{
                let destViewController = Constants.mainStoryboard.instantiateViewController(withIdentifier: "SubSubCategoryVC") as! SubSubCategoryVC
                lastSelectedMenuItem = selectedMenuItem
                selectedMenuItem = indexPath.row
                destViewController.topSubCatType = self.menuArray[indexPath.row] as! String
                destViewController.isDirectView = true
                destViewController.catID = "72"
                sideMenuController()?.setContentViewController(destViewController)
            }
            break
            
        case 9: // SMALL DOMESTI APPLIANCE
            
            let dict = UserDefaults.standard.value(forKey: "75") as! Dictionary<String,Any>
            let isNextCategory = dict["type"] as! NSString

            if isNextCategory == "product"{
                lastSelectedMenuItem = selectedMenuItem
                selectedMenuItem = indexPath.row
                
                let destViewController = Constants.mainStoryboard.instantiateViewController(withIdentifier: "ProductListVC") as! ProductListVC
                destViewController.ProductType = dict["category"] as! String
                destViewController.catID = "75"
                destViewController.isDirectView = true
                sideMenuController()?.setContentViewController(destViewController)
                
            }
            else{
                let destViewController = Constants.mainStoryboard.instantiateViewController(withIdentifier: "SubSubCategoryVC") as! SubSubCategoryVC
                lastSelectedMenuItem = selectedMenuItem
                selectedMenuItem = indexPath.row
                destViewController.topSubCatType = self.menuArray[indexPath.row] as! String
                destViewController.isDirectView = true
                destViewController.catID = "75"
                sideMenuController()?.setContentViewController(destViewController)
            }
            break


        case 10: // LOGOUT
            let alertController = UIAlertController.init(title: "Virtual Store", message: "Are you sure, you want to logout?", preferredStyle: UIAlertControllerStyle.alert)
            let  yesButton = UIAlertAction.init(title: "OK", style: UIAlertActionStyle.default, handler: { (action:UIAlertAction) in
                UserDefaults.standard.removeObject(forKey: Constants.UserID)
                UserDefaults.standard.removeObject(forKey: Constants.PROFILE)
                UserDefaults.standard.removeObject(forKey: Constants.PASSWORD)
                UserDefaults.standard.removeObject(forKey: Constants.isTransactionEnabled)
                UserDefaults.standard.removeObject(forKey: Constants.DISCOUNTCoupon)
                UserDefaults.standard.removeObject(forKey: Constants.QuoteID)
                UserDefaults.standard.removeObject(forKey: Constants.DISCOUNTApplied)
                UserDefaults.standard.removeObject(forKey: Constants.CART_Array)

                
                let destinationController = Constants.mainStoryboard.instantiateViewController(withIdentifier: "LoginView")
                self.present(destinationController, animated: true, completion: nil);
                
            })
            let noButton = UIAlertAction.init(title: "CANCEL", style: UIAlertActionStyle.cancel, handler: { (action:UIAlertAction) in

                self.hideSideMenuView()
                
            })
            alertController.addAction(noButton)
            alertController.addAction(yesButton)
            
            self.present(alertController, animated: true, completion: {
            })

            break

            
        case 11: //Update
            
            let destViewController = Constants.mainStoryboard.instantiateViewController(withIdentifier: "CatalogueUpdateVC") as! CatalogueUpdateVC
            lastSelectedMenuItem = selectedMenuItem
            selectedMenuItem = indexPath.row
            sideMenuController()?.setContentViewController(destViewController)
            
            break
            
            
            
        default:
            break
            
        }
    }
    
    
}
