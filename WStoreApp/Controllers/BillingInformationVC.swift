//
//  BillingInformationVC.swift
//  WStoreApp
//
//  Created by Surbhi on 23/08/17.
//  Copyright © 2017 SurbhiV. All rights reserved.
//

import UIKit

class BillingInformationVC: UIViewController, UITextFieldDelegate, UIPickerViewDelegate, UIPickerViewDataSource {
   


    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var contentViewHeightConstant: NSLayoutConstraint!
    
    @IBOutlet weak var mainShipmentView: UIView!
    @IBOutlet weak var diffShipmentView: UIView!
    @IBOutlet weak var diffShipmentViewHeightConstant: NSLayoutConstraint!

    @IBOutlet weak var fNameLabel1: UILabel!
    @IBOutlet weak var fNameText1: UITextField!
    @IBOutlet weak var fNameLine1: UILabel!
    @IBOutlet weak var fNameLineHeight1: NSLayoutConstraint!
    
    @IBOutlet weak var lNameLabel1: UILabel!
    @IBOutlet weak var lNameText1: UITextField!
    @IBOutlet weak var lNameLine1: UILabel!
    @IBOutlet weak var lNameLineHeight1: NSLayoutConstraint!

    @IBOutlet weak var phoneLabel1: UILabel!
    @IBOutlet weak var phoneText1: UITextField!
    @IBOutlet weak var phoneLine1: UILabel!
    @IBOutlet weak var phoneLineHeight1: NSLayoutConstraint!

    @IBOutlet weak var emailLabel1: UILabel!
    @IBOutlet weak var emailText1: UITextField!
    @IBOutlet weak var emailLine1: UILabel!
    @IBOutlet weak var emailLineHeight1: NSLayoutConstraint!

    @IBOutlet weak var addLabel1: UILabel!
    @IBOutlet weak var addText1: UITextField!
    @IBOutlet weak var addLine1: UILabel!
    @IBOutlet weak var addLineHeight1: NSLayoutConstraint!

    @IBOutlet weak var countryLabel1: UILabel!
    @IBOutlet weak var countryText1: UITextField!
    @IBOutlet weak var countryLine1: UILabel!
    @IBOutlet weak var countryLineHeight1: NSLayoutConstraint!

    @IBOutlet weak var stateLabel1: UILabel!
    @IBOutlet weak var stateText1: UITextField!
    @IBOutlet weak var stateLine1: UILabel!
    @IBOutlet weak var stateLineHeight1: NSLayoutConstraint!

    @IBOutlet weak var cityLabel1: UILabel!
    @IBOutlet weak var cityText1: UITextField!
    @IBOutlet weak var cityLine1: UILabel!
    @IBOutlet weak var cityLineHeight1: NSLayoutConstraint!

    @IBOutlet weak var pinLabel1: UILabel!
    @IBOutlet weak var pinText1: UITextField!
    @IBOutlet weak var pinLine1: UILabel!
    @IBOutlet weak var pinLineHeight1: NSLayoutConstraint!

    @IBOutlet weak var fNameLabel2: UILabel!
    @IBOutlet weak var fNameText2: UITextField!
    @IBOutlet weak var fNameLine2: UILabel!
    @IBOutlet weak var fNameLineHeight2: NSLayoutConstraint!
    
    @IBOutlet weak var lNameLabel2: UILabel!
    @IBOutlet weak var lNameText2: UITextField!
    @IBOutlet weak var lNameLine2: UILabel!
    @IBOutlet weak var lNameLineHeight2: NSLayoutConstraint!
    
    @IBOutlet weak var phoneLabel2: UILabel!
    @IBOutlet weak var phoneText2: UITextField!
    @IBOutlet weak var phoneLine2: UILabel!
    @IBOutlet weak var phoneLineHeight2: NSLayoutConstraint!
    
    @IBOutlet weak var emailLabel2: UILabel!
    @IBOutlet weak var emailText2: UITextField!
    @IBOutlet weak var emailLine2: UILabel!
    @IBOutlet weak var emailLineHeight2: NSLayoutConstraint!
    
    @IBOutlet weak var addLabel2: UILabel!
    @IBOutlet weak var addText2: UITextField!
    @IBOutlet weak var addLine2: UILabel!
    @IBOutlet weak var addLineHeight2: NSLayoutConstraint!
    
    @IBOutlet weak var countryLabel2: UILabel!
    @IBOutlet weak var countryText2: UITextField!
    @IBOutlet weak var countryLine2: UILabel!
    @IBOutlet weak var countryLineHeight2: NSLayoutConstraint!
    
    @IBOutlet weak var stateLabel2: UILabel!
    @IBOutlet weak var stateText2: UITextField!
    @IBOutlet weak var stateLine2: UILabel!
    @IBOutlet weak var stateLineHeight2: NSLayoutConstraint!
    
    @IBOutlet weak var cityLabel2: UILabel!
    @IBOutlet weak var cityText2: UITextField!
    @IBOutlet weak var cityLine2: UILabel!
    @IBOutlet weak var cityLineHeight2: NSLayoutConstraint!
    
    @IBOutlet weak var pinLabel2: UILabel!
    @IBOutlet weak var pinText2: UITextField!
    @IBOutlet weak var pinLine2: UILabel!
    @IBOutlet weak var pinLineHeight2: NSLayoutConstraint!

    @IBOutlet weak var currentAddButton: UIButton!
    @IBOutlet weak var diffAddButton: UIButton!
    @IBOutlet weak var continueButton: UIButton!
    
    var picker = UIPickerView()
    let countryArray = Array<Any>()
    var stateArray = Array<Any>()
    var cityArray = Array<Any>()
    var selectedCity_bill = String()
    var selectedState_bill = String()
    var selectedPin_bill = String()
    
    var selectedCity_ship = String()
    var selectedState_ship = String()
    var selectedPin_ship = String()
    
    var cityArray_ship = Array<Any>()
    var selectedBillingCityID = String()
    var selectedBillingStateID = String()
    var selectedShippingCityID = String()
    var selectedShipingStateID = String()
    
    //MARK:- Start
    override func viewDidLoad() {
        super.viewDidLoad()
        self.diffShipmentView.isHidden = true
        self.diffAddButton.isSelected = false
        self.currentAddButton.isSelected = true
        self.diffShipmentViewHeightConstant.constant = 0
        self.contentViewHeightConstant.constant = 665
        self.picker = UIPickerView.init()
        self.picker.delegate = self
        self.picker.dataSource = self

        fNameLabel1.isHidden = true
        lNameLabel1.isHidden = true
        phoneLabel1.isHidden = true
        emailLabel1.isHidden = true
        addLabel1.isHidden = true
        countryLabel1.isHidden = false
        stateLabel1.isHidden = false
        cityLabel1.isHidden = false
        pinLabel1.isHidden = false
        fNameLabel2.isHidden = true
        lNameLabel2.isHidden = true
        phoneLabel2.isHidden = true
        emailLabel2.isHidden = true
        addLabel2.isHidden = true
        countryLabel2.isHidden = true
        stateLabel2.isHidden = true
        cityLabel2.isHidden = true
        pinLabel2.isHidden = true
        
        countryText1.text = "India"
        countryText2.text = "India"
        countryText1.isUserInteractionEnabled = false
        countryText2.isUserInteractionEnabled = false

        
        stateText1.inputView = self.picker
        stateText2.inputView = self.picker
        cityText1.inputView = self.picker
        cityText2.inputView = self.picker

        // Do any additional setup after loading the view.
        self.selectedPin_bill = UserDefaults.standard.string(forKey: Constants.selectedPin)!
        self.fetchStateandCity()
        
    }
    
    //MARK:- States and City API
    func fetchStateandCity(){
        if Reachability.isConnectedToNetwork() {
            var quoteID = ""
            if UserDefaults.standard.object(forKey: Constants.QuoteID) == nil{
                quoteID = ""
            }
            else{
                quoteID = "\(UserDefaults.standard.object(forKey: Constants.QuoteID)!)"
            }

            Constants.appDelegate.startIndicator()
            
            if self.selectedPin_bill == ""{
                Constants.appDelegate.stopIndicator()

                CustomAlertController.showAlert(on: self, with: Simple, andAttributes:["title": "Enter valid pincode.", "message":""])
            }
            else{
                let urlStr = Constants.BASEURL + "getCityStates"
                let param = "pincode=\(self.selectedPin_bill)&quote_id=\(quoteID)"
                
                Server.postRequestWithURL(urlStr, paramString: param, completionHandler: { (response) in
                    
                    Constants.appDelegate.stopIndicator()
                    if response[Constants.STATE]?.int32Value  == 1 {
                        
                        self.selectedState_bill = response["current_state"] as! String
                        self.selectedCity_bill = response["current_city"] as! String
                        
                        DispatchQueue.main.async {
                            self.stateText1.text = self.selectedState_bill
                            self.cityText1.text = self.selectedCity_bill
                            self.pinText1.text = self.selectedPin_bill
                            
                            self.stateArray = response["states"] as! Array<Any>
                            self.setCityArrayforState(state: self.selectedState_bill, forBilling: true)
                        }
                        
                    }
                    else {
                        let mess = response["message"] as! String
                        CustomAlertController.showAlert(on: self, with: Simple, andAttributes: ["title":"", "message":mess])
                    }
                })
            }
        }
        else {
            Constants.appDelegate.stopIndicator()
            
            CustomAlertController.showAlert(on: self, with: Simple, andAttributes:["title": "", "message":"Please check your internet connection."])
        }
        
    }
    
    //MARK:- SET CItyArray
    func setCityArrayforState(state : String , forBilling : Bool){
        
        if forBilling == true{
            for item in self.stateArray{
                let dict = item as! Dictionary<String,Any>
                if dict["name"] as! String == state{
                    if self.cityArray.count > 0{
                        self.cityArray.removeAll()
                    }
                    self.cityArray = dict["cities"] as! Array<Any>
                }
            }
        }
        else {
            for item in self.stateArray{
                let dict = item as! Dictionary<String,Any>
                if dict["name"] as! String == state{
                    if self.cityArray_ship.count > 0{
                        self.cityArray_ship.removeAll()
                    }
                    self.cityArray_ship = dict["cities"] as! Array<Any>
                }
            }
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK:- Button Actions
    @IBAction func backButtonPressed(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }

    @IBAction func currentAddressButtonPressed(_ sender: Any) {
        self.diffAddButton.isSelected = false
        self.currentAddButton.isSelected = true
        
        self.diffShipmentView.isHidden = true
        self.diffShipmentViewHeightConstant.constant = 0
        self.contentViewHeightConstant.constant = 665
    }

    @IBAction func differentAddressButtonPressed(_ sender: Any) {
        self.currentAddButton.isSelected = false
        self.diffAddButton.isSelected = true
        
        self.diffShipmentView.isHidden = false
        self.diffShipmentViewHeightConstant.constant = 325
        self.contentViewHeightConstant.constant = 986

    }
    
    @IBAction func ContinueButtonPressed(_ sender: Any) {
        
        if self.diffAddButton.isSelected == true{
            if fNameText1.text!.characters.count == 0
            {
                self.showAlertWithAction(title:  "", message: "First Name Field cannot be Blank", textfield: fNameText1)
            }
            else if lNameText1.text!.characters.count == 0 {
                self.showAlertWithAction(title:  "", message: "Last Name Field cannot be Blank", textfield: lNameText1)
            }
            else if fNameText2.text!.characters.count == 0{
                self.showAlertWithAction(title:  "", message: "First Name Field cannot be Blank", textfield: fNameText2)
            }
            else if lNameText2.text!.characters.count == 0 {
                self.showAlertWithAction(title:  "", message: "Last Name Field cannot be Blank", textfield: lNameText2)
            }
            else if phoneText1.text!.characters.count == 0 {
                self.showAlertWithAction(title:  "", message: "Mobile Number cannot be Blank", textfield: phoneText1)
            }
            else if phoneText1.text!.characters.count < 10 {
                self.showAlertWithAction(title:  "", message: "Mobile Number must be 10 digit", textfield: phoneText1)
            }
            else if phoneText2.text!.characters.count == 0 {
                self.showAlertWithAction(title:  "", message: "Mobile Number cannot be Blank", textfield: phoneText2)
            }
            else if phoneText2.text!.characters.count < 10 {
                self.showAlertWithAction(title:  "", message: "Mobile Number must be 10 digit", textfield: phoneText1)
            }
            else if emailText1.text!.characters.count == 0 {
                self.showAlertWithAction(title:  "", message: "Email Field cannot be Blank", textfield: emailText1)
            }
            else if addText1.text!.characters.count == 0 {
                self.showAlertWithAction(title:  "", message: "Address Field cannot be Blank", textfield: addText1)
            }
            else if addText2.text!.characters.count == 0{
                self.showAlertWithAction(title:  "", message: "Address Field cannot be Blank", textfield: addText2)
            }
            else if !Helper.isValidEmail(emailText1.text!) {
                self.showAlertWithAction(title:  "", message: "Invalid Email Address", textfield: emailText1)
            }
            else if stateText1.text!.characters.count == 0 || stateText1.text! == "SELECT STATE" {
                self.showAlertWithAction(title:  "", message: "Please Select a State", textfield: stateText1)
            }
            else if stateText2.text!.characters.count == 0 || stateText2.text! == "SELECT STATE" {
                self.showAlertWithAction(title:  "", message: "Please Select a State", textfield: stateText2)
            }
            else if cityText1.text!.characters.count == 0 || cityText1.text! == "SELECT CITY" {
                self.showAlertWithAction(title:  "", message: "Please Select a City", textfield: cityText1)
            }
            else if cityText2.text!.characters.count == 0 || cityText2.text! == "SELECT CITY" {
                self.showAlertWithAction(title:  "", message: "Please Select a City", textfield: cityText2)
            }
            else if pinText1.text!.characters.count == 0 || pinText1.text! == "ZIP?POSTAL CODE" {
                self.showAlertWithAction(title:  "", message: "Invalid Postal Code", textfield: pinText1)
            }
            else if pinText2.text!.characters.count == 0 || pinText2.text! == "ZIP?POSTAL CODE"{
                self.showAlertWithAction(title:  "", message: "Invalid Postal Code", textfield: pinText2)
            }
            else{
                
                let personalDIctionary : Dictionary<String, Any>  = ["billing_firstname":fNameText1.text!,
                                                           "billing_lastname": lNameText1.text!,
                                                           "billing_telephone": phoneText1.text! ,
                                                           "billing_email": emailText1.text!,
                                                           "billing_address" : addText1.text!,
                                                           "billing_country": countryText1.text!,
                                                           "billing_region": stateText1.text!,
                                                           "billing_city" : cityText1.text!,
                                                           "billing_postcode" : pinText1.text!,
                                                           "use_for_shipping" : "0",
                                                           "shipping_firstname":fNameText2.text!,
                                                           "shipping_lastname": lNameText2.text!,
                                                           "shipping_telephone": phoneText2.text! ,
                                                           "shipping_address" : addText2.text!,
                                                           "shipping_country": countryText2.text!,
                                                           "shipping_region": stateText2.text!,
                                                           "shipping_city" : cityText2.text!,
                                                           "shipping_postcode" : pinText2.text!,
                                                           "payment_method" : "payucheckout_shared"
                                                           ] as Dictionary<String, Any>

                
                let persSTR = "\(fNameText1.text!) \(lNameText1.text!), \n\(addText1.text!), \n\(cityText1.text!) - \(pinText1.text!), \n\(stateText1.text!), \(countryText1.text!). \n\(emailText1.text!) \n\(phoneText1.text!)"
                let shipSTR = "\(fNameText2.text!) \(lNameText2.text!), \n\(addText2.text!), \n\(cityText2.text!) - \(pinText2.text!), \n\(stateText2.text!), \(countryText2.text!). \n\(emailText1.text!) \n\(phoneText2.text!)"
                
                self.GoToPaymentVC(personalDict: personalDIctionary, billStr: persSTR, shipStr:  shipSTR)
            }
        }
        else{
//            fNameText1.text = "surbhi"
//            lNameText1.text = "Varma"
//            phoneText1.text = "9977889988"
//            emailText1.text = "surbhi@gmail.com"
//            addText1.text = "KB"
            
            if fNameText1.text!.characters.count == 0
            {
                self.showAlertWithAction(title:  "", message: "First Name Field cannot be Blank", textfield: fNameText1)
            }
            else if lNameText1.text!.characters.count == 0 {
                self.showAlertWithAction(title:  "", message: "Last Name Field cannot be Blank", textfield: lNameText1)
            }
            else if phoneText1.text!.characters.count == 0 {
                self.showAlertWithAction(title:  "", message: "Mobile Number cannot be Blank", textfield: phoneText1)
            }
            else if phoneText1.text!.characters.count < 10 {
                self.showAlertWithAction(title:  "", message: "Mobile Number must be 10 digit", textfield: phoneText1)
            }
            else if emailText1.text!.characters.count == 0 {
                self.showAlertWithAction(title:  "", message: "Email Field cannot be Blank", textfield: emailText1)
            }
            else if addText1.text!.characters.count == 0 {
                self.showAlertWithAction(title:  "", message: "Address Field cannot be Blank", textfield: addText1)
            }
            else if !Helper.isValidEmail(emailText1.text!) {
                self.showAlertWithAction(title:  "", message: "Invalid Email Address", textfield: emailText1)
            }
            else if stateText1.text!.characters.count == 0 || stateText1.text! == "SELECT STATE" {
                self.showAlertWithAction(title:  "", message: "Please Select a State", textfield: stateText1)
            }
            else if cityText1.text!.characters.count == 0 || cityText1.text! == "SELECT CITY" {
                self.showAlertWithAction(title:  "", message: "Please Select a City", textfield: cityText1)
            }
            else if pinText1.text!.characters.count == 0 || pinText1.text! == "ZIP?POSTAL CODE" {
                self.showAlertWithAction(title:  "", message: "Invalid Postal Code", textfield: pinText1)
            }
            else{
                
                let persSTR = "\(fNameText1.text!) \(lNameText1.text!), \n\(addText1.text!), \n\(cityText1.text!) - \(pinText1.text!), \n\(stateText1.text!), \(countryText1.text!). \n\(emailText1.text!) \n\(phoneText1.text!)"
                
                let personalDIctionary : Dictionary<String, Any> = ["billing_firstname":fNameText1.text!,
                                                           "billing_lastname": lNameText1.text!,
                                                           "billing_telephone": phoneText1.text! ,
                                                           "billing_email": emailText1.text!,
                                                           "billing_address" : addText1.text!,
                                                           "billing_country": countryText1.text!,
                                                           "billing_region": stateText1.text!,
                                                           "billing_city" : cityText1.text!,
                                                           "billing_postcode" : pinText1.text!,
                                                           "use_for_shipping" : "1",
                                                           "shipping_firstname":fNameText1.text!,
                                                           "shipping_lastname": lNameText1.text!,
                                                           "shipping_telephone": phoneText1.text! ,
                                                           "shipping_address" : addText1.text!,
                                                           "shipping_country": countryText1.text!,
                                                           "shipping_region": stateText1.text!,
                                                           "shipping_city" : cityText1.text!,
                                                           "shipping_postcode" : pinText1.text!,
                                                           "payment_method" : "payucheckout_shared"
                                                            ] as Dictionary<String, Any>

                
                let shipSTR = "\(fNameText1.text!) \(lNameText1.text!), \n\(addText1.text!), \n\(cityText1.text!) - \(pinText1.text!), \n\(stateText1.text!), \(countryText1.text!). \n\(emailText1.text!) \n\(phoneText1.text!)"

                self.GoToPaymentVC(personalDict: personalDIctionary, billStr: persSTR, shipStr:  shipSTR)

            }
        }
    }
    
    //MARK:- GoToPayment VC
    
    func GoToPaymentVC(personalDict : Dictionary<String,Any>, billStr : String, shipStr : String){
        let nextStepVC = self.parent as! CheckoutMainVC
        
        nextStepVC.orderView.address = personalDict
        nextStepVC.orderView.billAddressSTR = billStr
        nextStepVC.orderView.shipAddressSTR = shipStr
        
        nextStepVC.orderView.orderCollection.reloadData()
        
        nextStepVC.HeaderlabelSelected(senderTag : 1 )
    }
    
    
    func createDictWIthValues(key : String, val : Any)->Dictionary<String, Any>{
        
        let dict = [key : val] as Dictionary<String, Any>
        return dict
    }
    
    func resignTextfields(){
        fNameText1.resignFirstResponder()
        fNameText2.resignFirstResponder()
        lNameText1.resignFirstResponder()
        lNameText2.resignFirstResponder()
        phoneText1.resignFirstResponder()
        phoneText2.resignFirstResponder()
        emailText1.resignFirstResponder()
        addText1.resignFirstResponder()
        addText2.resignFirstResponder()
        stateText1.resignFirstResponder()
        stateText2.resignFirstResponder()
        cityText1.resignFirstResponder()
        cityText2.resignFirstResponder()
        pinText1.resignFirstResponder()
        pinText2.resignFirstResponder()
    }
    
    //MARK:- Textfield Delegates
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
//        self.resignTextfields()
        
        if textField == fNameText1 {
            ACTIVATETextField(textf: fNameText1, textLabel: fNameLabel1, line: fNameLine1, height: fNameLineHeight1)
        }
        else  if textField == lNameText1 {
            ACTIVATETextField(textf: lNameText1, textLabel: lNameLabel1, line: lNameLine1, height: lNameLineHeight1)
        }
        else if textField == phoneText1 {
            ACTIVATETextField(textf: phoneText1, textLabel: phoneLabel1, line: phoneLine1, height: phoneLineHeight1)
        }
        else if textField == emailText1 {
            ACTIVATETextField(textf: emailText1, textLabel: emailLabel1, line: emailLine1, height: emailLineHeight1)
        }
        else  if textField == addText1 {
            ACTIVATETextField(textf: addText1, textLabel: addLabel1, line: addLine1, height: addLineHeight1)
        }
        else if textField == countryText1 {
            ACTIVATETextField(textf: countryText1, textLabel: countryLabel1, line: countryLine1, height: countryLineHeight1)
        }
        else if textField == stateText1 {
            picker.reloadAllComponents()

            ACTIVATETextField(textf: stateText1, textLabel: stateLabel1, line: stateLine1, height: stateLineHeight1)
        }
        else  if textField == cityText1 {
            picker.reloadAllComponents()

            ACTIVATETextField(textf: cityText1, textLabel: cityLabel1, line: cityLine1, height: cityLineHeight1)
        }
        else if textField == pinText1 {
            ACTIVATETextField(textf: pinText1, textLabel: pinLabel1, line: pinLine1, height: pinLineHeight1)
        }
        else if textField == fNameText2 {
            ACTIVATETextField(textf: fNameText2, textLabel: fNameLabel2, line: fNameLine2, height: fNameLineHeight2)
        }
        else  if textField == lNameText2 {
            ACTIVATETextField(textf: lNameText2, textLabel: lNameLabel2, line: lNameLine2, height: lNameLineHeight2)
        }
        else if textField == phoneText2 {
            ACTIVATETextField(textf: phoneText2, textLabel: phoneLabel2, line: phoneLine2, height: phoneLineHeight2)
        }
//        else if textField == emailText2 {
//            ACTIVATETextField(textf: emailText2, textLabel: emailLabel2, line: emailLine2, height: emailLineHeight2)
//        }
        else  if textField == addText2 {
            ACTIVATETextField(textf: addText2, textLabel: addLabel2, line: addLine2, height: addLineHeight2)
        }
        else if textField == countryText2 {
            ACTIVATETextField(textf: countryText2, textLabel: countryLabel2, line: countryLine2, height: countryLineHeight2)
        }
        else if textField == stateText2 {
            picker.reloadAllComponents()

            ACTIVATETextField(textf: stateText2, textLabel: stateLabel2, line: stateLine2, height: stateLineHeight2)
        }
        else  if textField == cityText2 {
            picker.reloadAllComponents()

            ACTIVATETextField(textf: cityText2, textLabel: cityLabel2, line: cityLine2, height: cityLineHeight2)
        }
        else if textField == pinText2 {
            ACTIVATETextField(textf: pinText2, textLabel: pinLabel2, line: pinLine2, height: pinLineHeight2)
        }
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        
        if textField == fNameText1 {
            DEACTIVATETextField(textf: fNameText1, textLabel: fNameLabel1, line: fNameLine1, height: fNameLineHeight1)
        }
        else  if textField == lNameText1 {
            DEACTIVATETextField(textf: lNameText1, textLabel: lNameLabel1, line: lNameLine1, height: lNameLineHeight1)
        }
        else if textField == phoneText1 {
            DEACTIVATETextField(textf: phoneText1, textLabel: phoneLabel1, line: phoneLine1, height: phoneLineHeight1)
        }
        else if textField == emailText1 {
            DEACTIVATETextField(textf: emailText1, textLabel: emailLabel1, line: emailLine1, height: emailLineHeight1)
        }
        else  if textField == addText1 {
            DEACTIVATETextField(textf: addText1, textLabel: addLabel1, line: addLine1, height: addLineHeight1)
        }
        else if textField == countryText1 {
            DEACTIVATETextField(textf: countryText1, textLabel: countryLabel1, line: countryLine1, height: countryLineHeight1)
        }
        else if textField == stateText1 {
            self.setCityArrayforState(state: self.stateText1.text!, forBilling: true)

            DEACTIVATETextField(textf: stateText1, textLabel: stateLabel1, line: stateLine1, height: stateLineHeight1)
        }
        else  if textField == cityText1 {

            DEACTIVATETextField(textf: cityText1, textLabel: cityLabel1, line: cityLine1, height: cityLineHeight1)
        }
        else if textField == pinText1 {
            DEACTIVATETextField(textf: pinText1, textLabel: pinLabel1, line: pinLine1, height: pinLineHeight1)
        }
        else if textField == fNameText2 {
            DEACTIVATETextField(textf: fNameText2, textLabel: fNameLabel2, line: fNameLine2, height: fNameLineHeight2)
        }
        else  if textField == lNameText2 {
            DEACTIVATETextField(textf: lNameText2, textLabel: lNameLabel2, line: lNameLine2, height: lNameLineHeight2)
        }
        else if textField == phoneText2 {
            DEACTIVATETextField(textf: phoneText2, textLabel: phoneLabel2, line: phoneLine2, height: phoneLineHeight2)
        }
//        else if textField == emailText2 {
//            DEACTIVATETextField(textf: emailText2, textLabel: emailLabel2, line: emailLine2, height: emailLineHeight2)
//        }
        else  if textField == addText2 {
            DEACTIVATETextField(textf: addText2, textLabel: addLabel2, line: addLine2, height: addLineHeight2)
        }
        else if textField == countryText2 {
            DEACTIVATETextField(textf: countryText2, textLabel: countryLabel2, line: countryLine2, height: countryLineHeight2)
        }
        else if textField == stateText2 {
            self.setCityArrayforState(state: self.stateText2.text!, forBilling: false)
            DEACTIVATETextField(textf: stateText2, textLabel: stateLabel2, line: stateLine2, height: stateLineHeight2)
        }
        else  if textField == cityText2 {
            DEACTIVATETextField(textf: cityText2, textLabel: cityLabel2, line: cityLine2, height: cityLineHeight2)
        }
        else if textField == pinText2 {
            DEACTIVATETextField(textf: pinText2, textLabel: pinLabel2, line: pinLine2, height: pinLineHeight2)
        }
    }

    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if range.location == 0 && string == " "{
            return false
        }
        if textField == fNameText1 || textField == fNameText2 || textField == lNameText1 || textField == lNameText2{
            
            let charSet : CharacterSet = CharacterSet.init(charactersIn: "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLKMNOPQRSTUVWXYZ ")
            let newRa = string.rangeOfCharacter(from: charSet)
            if newRa == nil && string != ""{
                return false
            }
        }
        if textField == phoneText1 || textField == phoneText2{
            let charSet : CharacterSet = CharacterSet.init(charactersIn: "0987654321")
            let newRa = string.rangeOfCharacter(from: charSet)
            if (newRa == nil || range.location > 9) && string != "" {
                return false
            }
        }
        if textField == pinText1 || textField == pinText2{
            let charSet : CharacterSet = CharacterSet.init(charactersIn: "0987654321")
            let newRa = string.rangeOfCharacter(from: charSet)
            if (newRa == nil || range.location > 5) && string != "" {
                return false
            }
        }
        
        return true
    }
    
    
    
    //MARK:- Activate Deactivate TextFields
    func ACTIVATETextField(textf : UITextField,textLabel : UILabel , line: UILabel, height : NSLayoutConstraint)
    {
        DispatchQueue.main.async() {
            UIView.animate(withDuration: 1.0, delay: 0, options: UIViewAnimationOptions.transitionFlipFromBottom, animations: {
                
                textLabel.isHidden = false
                Helper.activatetextField(line,height: height)
                
            }, completion: { (true) in
                textLabel.isHidden = false
            })
        }
    }
    
    
    func DEACTIVATETextField(textf : UITextField,textLabel : UILabel , line: UILabel, height : NSLayoutConstraint) {
        if textf.text == "" {
            DispatchQueue.main.async() {
                UIView.animate(withDuration: 1.0, animations: {
                    textLabel.isHidden = true
                    
                    Helper.deActivatetextField(line,height: height)
                }, completion: nil)
            }
        }
    }
    
    
//    MARK:- Picker View Methods
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
         return 1
    }
    
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        
        if self.stateText1.isFirstResponder {
            return stateArray.count
        }
        if self.stateText2.isFirstResponder {
            return stateArray.count
        }
        if self.cityText1.isFirstResponder {
            return cityArray.count
        }
        if self.cityText2.isFirstResponder {
            return cityArray_ship.count
        }
        return 0
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        
        if self.stateText1.isFirstResponder {
            let dict = stateArray [row] as! Dictionary<String,Any>
            return String.init(format: dict["name"] as! String, locale: nil)
        }
        if self.stateText2.isFirstResponder {
            let dict = stateArray [row] as! Dictionary<String,Any>
            return String.init(format: dict["name"] as! String, locale: nil)
        }
        if self.cityText1.isFirstResponder {
            let dict = cityArray [row] as! Dictionary<String,Any>
            return String.init(format: dict["name"] as! String, locale: nil)
        }
        if self.cityText2.isFirstResponder {
            let dict = cityArray_ship [row] as! Dictionary<String,Any>
            return String.init(format: dict["name"] as! String, locale: nil)
        }
        
        return ""
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        
        if self.self.stateText1.isFirstResponder
        {
            let dict = stateArray [row] as! Dictionary<String,Any>
            self.stateText1.text = String.init(format: dict["name"] as! String, locale: nil)
            self.selectedBillingStateID = String.init(format: dict["id"] as! String, locale: nil)
            self.cityText1.text = "Select City"
            self.selectedBillingCityID = "0"
            self.setCityArrayforState(state: self.stateText1.text!, forBilling: true)
        }
        if self.cityText1.isFirstResponder
        {
            let dict = cityArray [row] as! Dictionary<String,Any>
            self.selectedBillingCityID = String.init(format: dict["id"] as! String, locale: nil)
            self.cityText1.text = String.init(format: dict["name"] as! String, locale: nil)
        }
       
        if self.self.stateText2.isFirstResponder
        {
            let dict = stateArray [row] as! Dictionary<String,Any>
            self.stateText2.text = String.init(format: dict["name"] as! String, locale: nil)
            self.selectedShipingStateID = String.init(format: dict["id"] as! String, locale: nil)
            self.cityText2.text = "Select City"
            self.selectedShippingCityID = "0"
            self.setCityArrayforState(state: self.stateText2.text!, forBilling: false)
        }
        if self.cityText2.isFirstResponder
        {
            let dict = cityArray_ship [row] as! Dictionary<String,Any>
            self.selectedShippingCityID = String.init(format: dict["id"] as! String, locale: nil)
            self.cityText2.text = String.init(format: dict["name"] as! String, locale: nil)
        }
    }


    
    
    
//    MARK:-
    func showAlertWithAction(title : String, message : String, textfield : UITextField)
    {
        let Alert = UIAlertController.init(title: title, message: message, preferredStyle: UIAlertControllerStyle.alert)
        Alert.addAction(UIAlertAction.init(title: "OK", style: UIAlertActionStyle.default, handler: { (action:UIAlertAction) in
            textfield.becomeFirstResponder()
        }))
        self.present(Alert, animated: true, completion: nil)
    }
    
    
    
//    MARK:-
//
//
//    
//    
//    MARK:-
//
//    
//    
//    
//    MARK:-
//
//    
//    
//    
//    MARK:-

    
}
