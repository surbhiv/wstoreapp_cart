//
//  SpecialisationVC.swift
//  WStoreApp
//
//  Created by Surbhi on 22/06/17.
//  Copyright © 2017 SurbhiV. All rights reserved.
//

import UIKit

class SpecialisationVC: UIViewController, UITableViewDataSource, UITableViewDelegate {

    @IBOutlet weak var specialTable: UITableView!
    @IBOutlet weak var noLabel: UILabel!

    @IBOutlet weak var specialtableheight: NSLayoutConstraint!
    var specArray = Array<Any>()
    var expandedSections = NSMutableIndexSet()

    override func viewDidLoad() {
        super.viewDidLoad()
        expandedSections = NSMutableIndexSet.init()

        if specArray.count>0 {
            self.specialTable.isHidden = false
            self.noLabel.isHidden = true
            self.specialTable.reloadData()
        }
        else{
            self.specialTable.isHidden = true
            self.noLabel.isHidden = false
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK:- TABLEVIEW
    func numberOfSections(in tableView: UITableView) -> Int {
        return self.specArray.count
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let dict = specArray[section] as! Dictionary<String,Any>
        let descAr = dict["description"] as! Array<Any>
        
        if expandedSections.contains(section) {
            return descAr.count+1
        }
        return 1
        
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "headingTableCell", for: indexPath)
            let dict = specArray[indexPath.section] as! Dictionary<String,Any>
//            let img = cell.contentView.viewWithTag(1101) as! UIImageView
            let bckView = cell.contentView.viewWithTag(1104)!
            let myColor : UIColor = UIColor.lightGray
            bckView.layer.borderColor = myColor.cgColor
            bckView.layer.borderWidth = 0.5

            let lab = cell.contentView.viewWithTag(1102) as! UILabel
            lab.text = dict["title"] as? String
            
            tableView.estimatedRowHeight = 70
            tableView.rowHeight = UITableViewAutomaticDimension
            
            return cell
        }
        else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "specialisationCell", for: indexPath) as! specialisationCell
            let dict = specArray[indexPath.section] as! Dictionary<String,Any>
            let descAr = dict["description"] as! Array<Any>
            let subdict = descAr[indexPath.row - 1] as! Dictionary<String,Any>
            var keyStr = subdict["title"] as? String
            var valStr = subdict["value"] as? String
            
            keyStr = keyStr?.replacingOccurrences(of: "ÃÂÃÂ", with: "x")
            valStr = valStr?.replacingOccurrences(of: "ÃÂÃÂ", with: "x")
            keyStr = keyStr?.replacingOccurrences(of: "ÃÂÃÂ", with: "")
            valStr = valStr?.replacingOccurrences(of: "ÃÂÃÂ", with: "")
            keyStr = keyStr?.replacingOccurrences(of: "ÃÂ¢ÃÂÃÂ¢", with: "™")
            valStr = valStr?.replacingOccurrences(of: "ÃÂ¢ÃÂÃÂ¢", with: "™")
            keyStr = keyStr?.replacingOccurrences(of: "ÃÂ¢ÃÂÃÂ", with: "")
            valStr = valStr?.replacingOccurrences(of: "ÃÂ¢ÃÂÃÂ", with: "")

            cell.keyLabel.text = keyStr!
            cell.valLabel.text = valStr!
            
            let myColor : UIColor = UIColor.lightGray
            cell.backView.layer.borderColor = myColor.cgColor
            cell.backView.layer.borderWidth = 0.5
            
            tableView.estimatedRowHeight = 50
            tableView.rowHeight = UITableViewAutomaticDimension

            return cell

        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        let dict_current = specArray[indexPath.section] as! Dictionary<String,Any>
        let descAr_current = dict_current["description"] as! Array<Any>

        if indexPath.row == 0 {
            let cell = tableView.cellForRow(at: NSIndexPath.init(row: 0, section: indexPath.section) as IndexPath)
            let img = cell?.contentView.viewWithTag(1101) as! UIImageView
            
            var isExpanded = Bool()
            if expandedSections.contains(indexPath.section) {
                isExpanded = true
            }
            else{
                isExpanded = false
            }
            
            
            if isExpanded == true{
                expandedSections.remove(NSIndexSet.init(index: indexPath.section) as IndexSet)
                var indexArray = Array<Any>()
                for ind in 1 ..< descAr_current.count + 1{
                    let index = IndexPath.init(row: ind, section: indexPath.section)
                    indexArray.append(index)
                }

                tableView.beginUpdates()
                    tableView.deleteRows(at: indexArray as! [IndexPath], with: .automatic)
                    img.transform = CGAffineTransform.init(rotationAngle: 0.0)
                tableView.endUpdates()
            }
            else{
                let count = expandedSections.count
                
                if count > 0{
                    let selectedIndex = expandedSections.firstIndex
                    var cell_old = tableView.cellForRow(at: NSIndexPath.init(row: 0, section: selectedIndex) as IndexPath)
                    
                    if cell_old == nil{
                        cell_old = tableView.dequeueReusableCell(withIdentifier: "headingTableCell", for: NSIndexPath.init(row: 0, section: selectedIndex) as IndexPath)
                    }
                    
                    let img_old = cell_old?.contentView.viewWithTag(1101) as! UIImageView
                    
                    let dict_old = specArray[selectedIndex] as! Dictionary<String,Any>
                    let descAr_old = dict_old["description"] as! Array<Any>

                    expandedSections.remove(NSIndexSet.init(index: selectedIndex) as IndexSet)
                    
                    var indexArray = Array<Any>()
                    for ind in 1 ..< descAr_old.count + 1 {
                        let index = IndexPath.init(row: ind, section: selectedIndex)
                        indexArray.append(index)
                    }
                    
                    
                    tableView.beginUpdates()
                    tableView.deleteRows(at: indexArray as! [IndexPath], with: .automatic)
                    img_old.transform = CGAffineTransform.init(rotationAngle: 0.0)
                    tableView.endUpdates()
                }
                
//                let addindex = IndexPath.init(row: 1, section: indexPath.section)
                var addindex = Array<Any>()
                for ind in 1 ..< descAr_current.count + 1{
                    let index = IndexPath.init(row: ind, section: indexPath.section)
                    addindex.append(index)
                }
                
                
                expandedSections.add(NSIndexSet.init(index: indexPath.section) as IndexSet)
                
                tableView.beginUpdates()
                tableView.insertRows(at: addindex as! [IndexPath], with: .automatic)
                img.transform = CGAffineTransform.init(rotationAngle: CGFloat(Double.pi/2))
                tableView.endUpdates()
                
            }
        }
    }

    
}
