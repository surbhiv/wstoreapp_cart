//
//  featureVC.swift
//  WStoreApp
//
//  Created by Surbhi on 22/06/17.
//  Copyright © 2017 SurbhiV. All rights reserved.
//

import UIKit

class featureVC: UIViewController,UICollectionViewDelegate, UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {

    @IBOutlet weak var featureCollection: UICollectionView!
    @IBOutlet weak var noLabel: UILabel!

    @IBOutlet weak var featureCollectionheight: NSLayoutConstraint!
    var featureArray = Array<Any>()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        if featureArray.count>0 {
            self.featureCollection.isHidden = false
            self.noLabel.isHidden = true
        }
        else{
            self.featureCollection.isHidden = true
            self.noLabel.isHidden = false
        }
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK:- CollectionView
    
    
    // MARK:- COLLECTION VIEW FLOWLAYOUT
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 10
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 10
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        return CGSize.init(width: 0, height: 0)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForFooterInSection section: Int) -> CGSize {
        return CGSize.init(width: 1014, height: 130)
    }
    
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        
        if kind == UICollectionElementKindSectionHeader
        {
            let hview = collectionView.dequeueReusableSupplementaryView(ofKind: UICollectionElementKindSectionHeader, withReuseIdentifier: "CollectionHeader", for: indexPath) as! CollectionHeader
            return hview
        }
        else{
            let hview = collectionView.dequeueReusableSupplementaryView(ofKind: UICollectionElementKindSectionFooter, withReuseIdentifier: "FooterCollectionCell", for: indexPath) as! FooterCollectionCell
            return hview
            
        }
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let dict = featureArray[indexPath.item] as! Dictionary<String,Any>
//        let titleStr = dict["title"] as? String
        let descriptionStr = dict["description"] as? String
        
//        let titleHeight = Helper.getHeightForText(titleStr!, fon: UIFont.init(name: FONTS.Helvetica_Medium, size: 20)!, width: 480)
        let descHeight = Helper.getHeightForText(descriptionStr!, fon: UIFont.init(name: FONTS.Helvetica_Regular, size: 16)!, width: 480)

        let height = 380 + descHeight//350 + titleHeight + descHeight
        
        if indexPath.item + 1 < featureArray.count && indexPath.item % 2 == 0 {
            let dict2 = featureArray[indexPath.item + 1] as! Dictionary<String,Any>
//            let titleStr2 = dict2["title"] as? String
            let descriptionStr2 = dict2["description"] as? String
            
//            let titleHeight2 = Helper.getHeightForText(titleStr2!, fon: UIFont.init(name: FONTS.Helvetica_Medium, size: 20)!, width: 480)
            let descHeight2 = Helper.getHeightForText(descriptionStr2!, fon: UIFont.init(name: FONTS.Helvetica_Regular, size: 16)!, width: 480)
            
            let height2 = 380 + descHeight2//350 + titleHeight2 + descHeight2
            
            if height2 > height {
                return CGSize.init(width: 500, height: height2)
            }
            else{
                return CGSize.init(width: 500, height: height)
            }
        }
        else if indexPath.item - 1 >= 0 && indexPath.item % 2 == 1{
            let dict2 = featureArray[indexPath.item - 1] as! Dictionary<String,Any>
//            let titleStr2 = dict2["title"] as? String
            let descriptionStr2 = dict2["description"] as? String
            
//            let titleHeight2 = Helper.getHeightForText(titleStr2!, fon: UIFont.init(name: FONTS.Helvetica_Medium, size: 20)!, width: 480)
            let descHeight2 = Helper.getHeightForText(descriptionStr2!, fon: UIFont.init(name: FONTS.Helvetica_Regular, size: 16)!, width: 480)
            
            let height2 = 380 + descHeight2//350 + titleHeight2 + descHeight2
            
            if height2 > height {
                return CGSize.init(width: 500, height: height2)
            }
            else{
                return CGSize.init(width: 500, height: height)
            }
        }
        
        return CGSize.init(width: 500, height: height)

    }
    
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return featureArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "featureCell", for: indexPath) as! featureCell
        cell.titleLabel.text = ""
        cell.descLabel.text = ""
        cell.featureImage.image = nil
        
        
        let dict = featureArray[indexPath.item] as! Dictionary<String,Any>
        let imageStr = dict["img"] as? String
        var titleStr = dict["title"] as? String
        var descriptionStr = dict["description"] as? String
        let typeStr = dict["type"] as? String


        titleStr = titleStr?.replacingOccurrences(of: "ÃÂ®", with: "®")
        titleStr = titleStr?.replacingOccurrences(of: "Ã¢ÂÂ¢", with: "™")
        titleStr = titleStr?.replacingOccurrences(of: "â¢", with: "™")
        titleStr = titleStr?.replacingOccurrences(of: "Ã¢ÂÂ", with: "'")
        titleStr = titleStr?.replacingOccurrences(of: "ÃÂ", with: "")
        titleStr = titleStr?.replacingOccurrences(of: "Ã¢ÂÂ", with: "")

        descriptionStr = descriptionStr?.replacingOccurrences(of: "ÃÂ®", with: "®")
        descriptionStr = descriptionStr?.replacingOccurrences(of: "Ã¢ÂÂ¢", with: "™")
        descriptionStr = descriptionStr?.replacingOccurrences(of: "â¢", with: "™")
        descriptionStr = descriptionStr?.replacingOccurrences(of: "ÃÂ", with: "")
        descriptionStr = descriptionStr?.replacingOccurrences(of: "Ã¢ÂÂ", with: "")

        descriptionStr = descriptionStr?.replacingOccurrences(of: "Ã¢ÂÂ", with: "'")
        
        cell.titleLabel.text = titleStr
        cell.descLabel.text = descriptionStr
        
        
        if imageStr != nil && typeStr == "image" {
            
            cell.featureweb.isHidden = true
            cell.featureImage.isHidden = false
            
            let imageUrl = URL.init(string: imageStr!)
            
            let documentsDirectoryURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
            if !FileManager.default.fileExists(atPath: documentsDirectoryURL.appendingPathComponent((imageUrl!.lastPathComponent)).path) {
                if Reachability.isConnectedToNetwork() == true {
                    Helper.downloadImageLinkAndCreateAsset(imageStr!)
                    cell.featureImage.sd_setImage(with: URL.init(string: imageStr!))
                }
                else{
                    cell.featureImage.image = UIImage.init(named: "noImage.png")
                }
            }
            else{
                let abc = documentsDirectoryURL.appendingPathComponent(imageUrl!.lastPathComponent).path
                let imageURL = URL(fileURLWithPath: abc)
                cell.featureImage.image = UIImage(contentsOfFile: imageURL.path)
            }
        }
        else if imageStr != nil && typeStr == "video"{

            if Reachability.isConnectedToNetwork() == true {
                cell.featureweb.isHidden = false
                cell.featureImage.isHidden = true

                cell.featureweb.loadRequest(URLRequest.init(url: URL.init(string: imageStr!)!))
            }
            else{
                cell.featureweb.isHidden = true
                cell.featureImage.isHidden = false

                cell.featureImage.image = UIImage.init(named: "novideo.png")
            }
        }
        
        return cell
    }
    
    
}
