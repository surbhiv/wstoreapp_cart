//
//  SubSubCategoryVC.swift
//  WStoreApp
//
//  Created by Surbhi on 14/06/17.
//  Copyright © 2017 SurbhiV. All rights reserved.
//

import UIKit
import Firebase

class SubSubCategoryVC: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout  {

    @IBOutlet weak var catCollection: UICollectionView!
    @IBOutlet weak var noLabel: UILabel!
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var backButtonImage: UIImageView!
    @IBOutlet weak var cartButton: UIButton!
    @IBOutlet weak var cartCountLable: UILabel!

    var topCatType = String()
    var topSubCatType = String()
    var ProductTypeSelected = String()

    var isDirectView = Bool()
    var catID = String()
    var subCatArray = Array<Any>()
    
    var oldCatId = String()
    var oldSubCatArray = Array<Any>()
    var hasOldSubCat = false
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let dict = UserDefaults.standard.value(forKey: self.catID) as! Dictionary<String,Any>
        let arr = dict["data"] as! Array<Any>
        if arr.count > 0 {
            self.noLabel.isHidden = true
            self.subCatArray = dict["data"] as! Array<Any>
            DispatchQueue.main.async {
                self.catCollection.reloadData()
            }
        }
        else{
            self.noLabel.isHidden = false
            self.getSubCategoriesforType()
        }


        if self.isDirectView == true {
            backButtonImage.image = UIImage.init(named: "sideMenu")
        }
        else{
            backButtonImage.image = UIImage.init(named: "backARROW")
        }
        
    }
    override var prefersStatusBarHidden : Bool {
        return true
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        sideMenuController()?.sideMenu?.allowPanGesture = false
        sideMenuController()?.sideMenu?.allowLeftSwipe = false
        sideMenuController()?.sideMenu?.allowRightSwipe = false

        if subCatArray.count > 0 {
            DispatchQueue.main.async {
                self.catCollection.reloadData()
            }
        }
        
        
        cartCountLable.layer.cornerRadius = 7.5
        cartCountLable.clipsToBounds = true
        
        if UserDefaults.standard.bool(forKey: Constants.isTransactionEnabled) == false{
            self.cartButton.isHidden = true
            if UserDefaults.standard.object(forKey: Constants.CART_Array) == nil{
                cartCountLable.isHidden = true
            }
            else{
                let arr = UserDefaults.standard.object(forKey: Constants.CART_Array) as! Array<Any>
                let count = arr.count
                cartCountLable.isHidden = false
                cartCountLable.text = "\(count)"
            }
        }
        else{
            self.cartButton.isHidden = false
            if UserDefaults.standard.object(forKey: Constants.CART_Array) == nil{
                cartCountLable.isHidden = true
            }
            else{
                let arr = UserDefaults.standard.object(forKey: Constants.CART_Array) as! Array<Any>
                let count = arr.count
                cartCountLable.isHidden = false
                cartCountLable.text = "\(count)"
            }
        }

    }
    // MARK:- FETCH API
    func getSubCategoriesforType(){
        
        if Reachability.isConnectedToNetwork() {
            Constants.appDelegate.startIndicator()
            
            let urlStr = Constants.BASEURL + "productlisting"
            let param = "brandshop_id=\(String(describing: UserDefaults.standard.object(forKey: Constants.UserID)!))&category_id=\(self.catID)"
            
            Server.postRequestWithURL(urlStr, paramString: param, completionHandler: { (response) in
                
                Constants.appDelegate.stopIndicator()
                if response[Constants.STATE]?.int32Value  == 1 {
                    
                    UserDefaults.standard.setValue(response, forKeyPath: self.catID)

                    self.subCatArray = response["data"] as! Array<Any>
//                    UserDefaults.standard.set(self.subCatArray, forKey: "product_\(self.catID)")
                    self.topSubCatType = response["category"] as! String
                    
                    if self.subCatArray.count > 0{
                        self.noLabel.isHidden = true
                    }
                    else{
                        self.noLabel.isHidden = false
                    }
                    DispatchQueue.main.async {
                        self.catCollection.reloadData()
                    }
                    
                }
                else {
                    let dict = UserDefaults.standard.value(forKey: self.catID) as! Array<AnyObject>
                    if dict.count > 0 {
                        self.noLabel.isHidden = true

                        self.subCatArray = UserDefaults.standard.value(forKey: self.catID) as! Array<AnyObject>
                        Constants.appDelegate.stopIndicator()
                        DispatchQueue.main.async {
                            self.catCollection.reloadData()
                        }
                    }
                    else{
                        self.noLabel.isHidden = false

                        Constants.appDelegate.stopIndicator()
                        var mess = String()
                        if response.count == 0{
                            mess = "Some Error Occurred"
                        }
                        else{
                            mess = response["message"] as! String
                        }
                        CustomAlertController.showAlert(on: self, with: Simple, andAttributes: ["title":"", "message":mess])
                    }
                }
            })
            
        }
        else {
            let dict = UserDefaults.standard.value(forKey: self.catID) as! Dictionary<String,Any>
            let arr = dict["data"] as! Array<Any>
            if arr.count > 0{
                self.subCatArray = dict["data"] as! Array<Any>
            }
            
            if self.subCatArray.count > 0
            {
                self.noLabel.isHidden = false

                DispatchQueue.main.async {
                    self.catCollection.reloadData()
                }
            }
            else{
                self.noLabel.isHidden = true

                CustomAlertController.showAlert(on: self, with: Simple, andAttributes:["title": "", "message":"Please check your internet connection."])
            }
        }
    }
    
    
    //MARK:- BUTTON ACTIONS
    
    @IBAction func BackButtonPressed(_ sender: Any) {
        if self.isDirectView == true {
            if hasOldSubCat == true {
                self.catID = self.oldCatId
                self.subCatArray = self.oldSubCatArray
                hasOldSubCat = false
                backButtonImage.image = UIImage.init(named: "sideMenu")

                Constants.appDelegate.startIndicator()
                let dict = UserDefaults.standard.value(forKey: self.catID) as! Dictionary<String,Any>
                let arr = dict["data"] as! Array<Any>
                self.topSubCatType = dict["category"] as! String

                if arr.count > 0 {
                    self.subCatArray = dict["data"] as! Array<Any>
                    DispatchQueue.main.async {
                        Constants.appDelegate.stopIndicator()
                        self.catCollection.reloadData()
                    }
                }
                else{
                    self.getSubCategoriesforType()
                }

            }
            else{
                toggleSideMenuView()
            }
        }
        else{
            
            if hasOldSubCat == true {
                self.catID = self.oldCatId
                self.subCatArray = self.oldSubCatArray
                hasOldSubCat = false
                
                
                Constants.appDelegate.startIndicator()
                let dict = UserDefaults.standard.value(forKey: self.catID) as! Dictionary<String,Any>
                let arr = dict["data"] as! Array<Any>
                if arr.count > 0 {
                    self.subCatArray = dict["data"] as! Array<Any>
                    DispatchQueue.main.async {
                        Constants.appDelegate.stopIndicator()
                        self.catCollection.reloadData()
                    }
                }
                else{
                    self.getSubCategoriesforType()
                }

            }
            else{
                self.navigationController?.popViewController(animated: true)
            }
        }
    }
    
    @IBAction func SearchButtonPressed(_ sender: Any) {
        
    }
    
    // MARK:- COLLECTION VIEW FLOWLAYOUT
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
            return CGSize.init(width: 1014, height: 130)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForFooterInSection section: Int) -> CGSize {
            return CGSize.init(width: 1014, height: 120)
    }
    
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        
        if kind == UICollectionElementKindSectionHeader
        {
            let hview = collectionView.dequeueReusableSupplementaryView(ofKind: UICollectionElementKindSectionHeader, withReuseIdentifier: "CollectionHeader", for: indexPath) as! CollectionHeader
            
            hview.headingLabel.textAlignment = .center
            hview.headingLabel.text = self.topSubCatType.uppercased()
            return hview
        }
        else{ 
            let hview = collectionView.dequeueReusableSupplementaryView(ofKind: UICollectionElementKindSectionFooter, withReuseIdentifier: "FooterCollectionCell", for: indexPath) as! FooterCollectionCell
            return hview
        }
    }

    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize
    {
        if subCatArray.count > 0 {
            return CGSize(width: (ScreenSize.SCREEN_MAX_LENGTH - 40)/2, height: (ScreenSize.SCREEN_MAX_LENGTH - 40)/2 + 125)//45 + htHEad + htDesc
        }
        return CGSize(width: (ScreenSize.SCREEN_MAX_LENGTH - 40)/2, height: (ScreenSize.SCREEN_MAX_LENGTH - 40)/2)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 20
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 20
    }
    
    // MARK:- COLLECTION VIEW
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return subCatArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "SubSUBCategoryCell", for: indexPath)
        let dict = self.subCatArray[indexPath.item] as! Dictionary<String,Any>
                
        let img = cell.viewWithTag(301) as! UIImageView
        let labHEAD = cell.viewWithTag(302) as! UILabel
        let labSUBHead = cell.viewWithTag(303) as! UILabel

        let imgSTR = dict["image"] as! String
//        img.sd_setImage(with: URL.init(string: imgSTR))
        let imageUrl = URL.init(string: imgSTR)
        
        let documentsDirectoryURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
        if !FileManager.default.fileExists(atPath: documentsDirectoryURL.appendingPathComponent((imageUrl!.lastPathComponent)).path) {
//            Helper.downloadImageLinkAndCreateAsset(imgSTR)
            if Reachability.isConnectedToNetwork() == true {
                Helper.downloadImageLinkAndCreateAsset(imgSTR)
                img.sd_setImage(with: URL.init(string: imgSTR))
            }
            else{
                img.image = UIImage.init(named: "noImage.png")
            }
        }
        else{
            let abc = documentsDirectoryURL.appendingPathComponent(imageUrl!.lastPathComponent).path
            let imageURL = URL(fileURLWithPath: abc)
            img.image = UIImage(contentsOfFile: imageURL.path)
        }


        var name = dict["name"] as? String
        
        if topSubCatType.contains("Built In"){
            
        }
        else{
            name = name?.replacingOccurrences(of: " Refrigerators", with: "")
        }
        

        var nameArray = name?.components(separatedBy: " ")
        
        let att1 = [NSForegroundColorAttributeName: UIColor.black, NSFontAttributeName: UIFont(name: FONTS.Helvetica_Bold, size: 18)! ]
        let att2 = [NSForegroundColorAttributeName: Colors.appThemeColorText, NSFontAttributeName: UIFont(name: FONTS.Helvetica_Medium, size: 17)!]

        let fName = NSMutableAttributedString(string: "\((nameArray?.first)!) ", attributes: att1)
        nameArray?.removeFirst()
        let lnameStr = nameArray?.joined(separator: " ")
        let lname = NSMutableAttributedString(string: lnameStr!, attributes: att2)
        let combination = NSMutableAttributedString()
        combination.append(fName)
        combination.append(lname)
        
        labHEAD.attributedText = combination
        labSUBHead.text = dict["description"] as? String
                
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let dict = self.subCatArray[indexPath.item] as! Dictionary<String,Any>
        
        let pricekey = dict["category"] as? String
        if pricekey == "0" {
            let destViewController = Constants.mainStoryboard.instantiateViewController(withIdentifier: "ProductListVC") as! ProductListVC
            destViewController.ProductType = dict["name"] as! String
            destViewController.catID = "\(dict["id"]!)"
            destViewController.isDirectView = false
            self.navigationController?.pushViewController(destViewController, animated: true)
        }
        else{
            self.oldCatId = self.catID
            self.oldSubCatArray = self.subCatArray
            self.catID = "\(dict["id"]!)"
            hasOldSubCat = true

            backButtonImage.image = UIImage.init(named: "backARROW")

            Constants.appDelegate.startIndicator()
            let dict = UserDefaults.standard.value(forKey: self.catID) as! Dictionary<String,Any>
            let arr = dict["data"] as! Array<Any>
            self.topSubCatType = dict["category"] as! String

            if arr.count > 0 {
                self.subCatArray = dict["data"] as! Array<Any>
                DispatchQueue.main.async {
                    Constants.appDelegate.stopIndicator()
                    self.catCollection.reloadData()
                }
            }
            else{
                self.getSubCategoriesforType()
            }
        }
    }
    
    
    //MARK:- Orientation
    
    override var supportedInterfaceOrientations : UIInterfaceOrientationMask {
        return UIInterfaceOrientationMask.all
    }
    
    override func didRotate(from fromInterfaceOrientation: UIInterfaceOrientation) {
        self.catCollection.reloadData()
    }

}
