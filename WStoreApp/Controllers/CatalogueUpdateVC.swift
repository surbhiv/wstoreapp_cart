//
//  CatalogueUpdateVC.swift
//  WStoreApp
//
//  Created by Surbhi on 01/09/17.
//  Copyright © 2017 SurbhiV. All rights reserved.
//

import UIKit

class CatalogueUpdateVC: UIViewController {

    
    var superCatArray = ["kitchen","laundry","living-space"]
    var categoryIDArray = Array<NSInteger>()
    var productIDArray = Array<NSInteger>()
    var imagArray = Array<Any>()
    var videoArray = Array<Any>()

    var catIDArray = Array<NSInteger>()
    var prodDArray = Array<NSInteger>()
    var categImgArray = Array<String>()
    var colorListArray = Array<String>()
    var prodImageArray = Array<String>()
    var colorDetailArray = Array<String>()
    var featureArray = Array<String>()
    var prodCommonImageArray = Array<String>()
    var prodDetailImageArray = Array<String>()

    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        NotificationCenter.default.addObserver(self, selector: #selector(VideoDownloadComplete), name: NSNotification.Name(rawValue: "videoDownloadingComplete"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(ImageDownloadComplete), name: NSNotification.Name(rawValue: "ImageDownloadingComplete"), object: nil)

    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        
        let Alert = UIAlertController.init(title: "Would you like to update the App Now?", message: "It will take upto 5 hours to update the app.", preferredStyle: UIAlertControllerStyle.alert)
        Alert.addAction(UIAlertAction.init(title: "YES", style: UIAlertActionStyle.default, handler: { (action:UIAlertAction) in
            self.getImagesToDisplay()
        }))
        Alert.addAction(UIAlertAction.init(title: "LATER", style: UIAlertActionStyle.default, handler: { (action:UIAlertAction) in
            let mo = self.sideMenuController()?.sideMenu?.menuViewController as! MenuTableView
            mo.tableView(mo.tableView, didSelectRowAt: IndexPath.init(row: 0, section: 0))
            self.toggleSideMenuView()
            mo.tableView.selectRow(at: IndexPath.init(row: 0, section: 0), animated: true, scrollPosition: .middle)
        }))
        self.present(Alert, animated: true, completion: nil)
    }
    
    
    @IBAction func toggleSideMenu(_ sender: AnyObject) {
        toggleSideMenuView()
    }


    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func getImagesToDisplay(){
        
        if Reachability.isConnectedToNetwork() {
            Constants.appDelegate.startIndicator()
            
            let urlStr = "http://neuronimbusinteractive.com/screensaver/get_image.php"
            Server.getRequestWithURL(urlString: urlStr, completionHandler: { (response) in
                Constants.appDelegate.stopIndicator()
                
                if response[Constants.STATE]?.int32Value  == 1 {
                    
                    var imgArray = response["data_ios"] as! Array<AnyObject>
                    
                    for img in imgArray{
                        let dc = img as! Dictionary<String,Any>
                        let imgStr = dc["image"] as! String
                        self.imagArray.append(imgStr)
                    }
                    
                    DispatchQueue.main.async {
                        
                        if imgArray.count > 1{
                            
                            let newImg = imgArray[imgArray.count - 1] as! Dictionary<String,Any>
                            let newmm = newImg["image"] as! String
                            let videoStr = "http://neuronimbusinteractive.com/screensaver/videos/Whirlpool3videoloop4.mp4"
                            
                            let newvid = ["video" : videoStr,
                                          "image" : newmm]
                            
                            let count = imgArray.count
                            imgArray.insert(newvid as AnyObject, at: count)
                            self.videoArray.insert(videoStr, at: 0)
                            
                            UserDefaults.standard.set(imgArray, forKey: Constants.SCREENSAVER)
                            
                            self.getSubCategoriesforType("kitchen")
                        }
                    }
                }
                else {
//                    CustomAlertController.showAlert(on: self, with: Simple, andAttributes:["title": "", "message":"Please check your internet connection."])
                    let Alert = UIAlertController.init(title: "", message: "Network connection Lost.", preferredStyle: UIAlertControllerStyle.alert)
                    Alert.addAction(UIAlertAction.init(title: "OK", style: UIAlertActionStyle.default, handler: { (action:UIAlertAction) in
                        let mo = self.sideMenuController()?.sideMenu?.menuViewController as! MenuTableView
                        mo.tableView(mo.tableView, didSelectRowAt: IndexPath.init(row: 0, section: 0))
                        self.toggleSideMenuView()
                        mo.tableView.selectRow(at: IndexPath.init(row: 0, section: 0), animated: true, scrollPosition: .middle)
                    }))
                    self.present(Alert, animated: true, completion: nil)

                }
            })
        }
        else {
//            CustomAlertController.showAlert(on: self, with: Simple, andAttributes:["title": "", "message":"Please check your internet connection."])
            let Alert = UIAlertController.init(title: "", message: "Please check your internet connection.", preferredStyle: UIAlertControllerStyle.alert)
            Alert.addAction(UIAlertAction.init(title: "OK", style: UIAlertActionStyle.default, handler: { (action:UIAlertAction) in
                let mo = self.sideMenuController()?.sideMenu?.menuViewController as! MenuTableView
                mo.tableView(mo.tableView, didSelectRowAt: IndexPath.init(row: 0, section: 0))
                self.toggleSideMenuView()
                mo.tableView.selectRow(at: IndexPath.init(row: 0, section: 0), animated: true, scrollPosition: .middle)
            }))
            self.present(Alert, animated: true, completion: nil)

        }
    }
    
    
    //MARK:- GET SUB CATEGORIES
    func getSubCategoriesforType(_ typeStr : String){
        // Kitchen // Laundry // Living Spaces
        
        if Reachability.isConnectedToNetwork() {
            Constants.appDelegate.startIndicator()
            
            let urlStr = Constants.BASEURL + "categories"
            let param = "type=\(typeStr)"
            
            Server.postRequestWithURL(urlStr, paramString: param, completionHandler: { (response) in
                
                if response[Constants.STATE]?.int32Value  == 1 {
                    
                    let subCatArray = response["data"] as! Array<AnyObject>
                    
                    UserDefaults.standard.set(subCatArray, forKey: typeStr.uppercased())
                    
                    for cat in subCatArray{
                        let dict = cat as! Dictionary<String,Any>
                        let catdID = dict["id"] as! NSInteger
                        let imageStr = dict["image"] as! String
                        self.imagArray.append(imageStr)
                        
                        if self.categoryIDArray.contains(catdID) == false{
                            self.categoryIDArray.append(catdID)
                        }
                    }
                    
                    if typeStr == "kitchen" {
                        Constants.appDelegate.stopIndicator()
                        self.getSubCategoriesforType("laundry")
                    }
                    else if typeStr == "laundry" {
                        Constants.appDelegate.stopIndicator()
                        self.getSubCategoriesforType("living-spaces")
                    }
                    else{
                        self.categoryIDArray.append(17) // for accessories addinf cat id manually
                        
                        UserDefaults.standard.set(self.categoryIDArray, forKey: Constants.catID_array)

                        Constants.appDelegate.stopIndicator()
                        
                        if self.videoArray.count>0{
                            Constants.appDelegate.startIndicator()
                            self.downloadVideos()
                        }
                        else{
                            if self.imagArray.count > 0 {
                                self.downloadImages()
                            }
                        }
                    }
                }
                else {
                    self.getSubCategoriesforType(typeStr)
                }
            })
        }
        else {
            CustomAlertController.showAlert(on: self, with: Simple, andAttributes:["title": "", "message":"Please check your internet connection."])
        }
    }
    
    //MARK:- VIDEO DOWNLOAD
    func downloadVideos(){
        let documentsDirectoryURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
        
        for obj in 0 ..< self.videoArray.count{
            
            let imgname = self.videoArray[obj] as! String
            
            let imageUrl = URL.init(string: imgname)
            if !FileManager.default.fileExists(atPath: documentsDirectoryURL.appendingPathComponent((imageUrl!.lastPathComponent)).path) {
                Helper.downloadVideoLinkAndCreateAsset(imgname, objectNum: obj)
            }
            else{
                if obj == self.videoArray.count-1 {
                    self.downloadImages()
                }
            }
        }
    }
    
    //MARK:- DOWNLOAD SCREENSAVER VIDEO

    func VideoDownloadComplete(noti : Notification){
        let obj =  noti.object as! NSInteger
        
        if obj == self.videoArray.count-1 {
            self.downloadImages()
        }
    }
    
    
    //MARK:- DOWNLOAD SCREENSAVER IMAGES
    func downloadImages(){
        let documentsDirectoryURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
        
        for obj in 0 ..< self.imagArray.count{
            
            let imgname = self.imagArray[obj] as! String
            
            let imageUrl = URL.init(string: imgname)
            if !FileManager.default.fileExists(atPath: documentsDirectoryURL.appendingPathComponent((imageUrl!.lastPathComponent)).path) {
                Helper.downloadImageLinkAndCreateAsset(imgname, notificationName: "ImageDownloadingComplete", objectNUm: obj, isLast : true)
            }
            else{
                if obj == self.imagArray.count-1 {
                    let when = DispatchTime.now() + 15
                    DispatchQueue.main.asyncAfter(deadline: when) {
                        Constants.appDelegate.stopIndicator()
                        self.startCategoryAPIDownload()
                    }
                }
            }
        }
    }
    
    func ImageDownloadComplete(noti : Notification){
        
        let obj =  noti.object as! NSInteger
        
        if obj == self.imagArray.count-1 {
            
            let when = DispatchTime.now() + 15
            DispatchQueue.main.asyncAfter(deadline: when) {
                Constants.appDelegate.stopIndicator()
                self.startCategoryAPIDownload()
            }
        }
    }
    
    //MARK:- Category DOwnload API

    func startCategoryAPIDownload(){
        catIDArray = UserDefaults.standard.value(forKey: Constants.catID_array) as! Array<NSInteger>
        if categoryIDArray.count > 0{
            self.getSub_SubCategoriesforType(catArray: self.categoryIDArray, objectLocation: 0)
        }

    }
    
    
    func getSub_SubCategoriesforType(catArray : Array<NSInteger>, objectLocation : Int){
        
        if Reachability.isConnectedToNetwork() {
            
            Constants.appDelegate.startIndicator()
            
            let catdID = "\(String(describing: catArray[objectLocation]))"
            
            let urlStr = Constants.BASEURL + "productlisting"
            let param = "brandshop_id=\(String(describing: UserDefaults.standard.object(forKey: Constants.UserID)!))&category_id=\(catdID)"
            
            Server.postRequestWithURL(urlStr, paramString: param, completionHandler: { (response) in
                
                
                if response[Constants.STATE]?.int32Value  == 1 {
                    
                    
                    let subCatArray = response["data"] as! Array<AnyObject>
                    if response["type"] as! String == "category"{
                        UserDefaults.standard.setValue(response, forKeyPath: catdID)
                        
                        for dc in subCatArray{
                            let dict = dc as! Dictionary<String,Any>
                            
                            let imageString = dict["image"] as! String
                            if self.categImgArray.contains(imageString) == false {
                                self.categImgArray.append(imageString)
                            }
                            
                            let catdID = dict["id"] as! String
                            let catInt = NSInteger.init(catdID)
                            
                            if self.catIDArray.contains(catInt!) == false{
                                self.catIDArray.append(catInt!)
                            }
                        }
                        // save sub categories
                    }
                    else{
                        // save product list table
                        UserDefaults.standard.setValue(response, forKeyPath: catdID)
                        
                        for dc in subCatArray{
                            let dict = dc as! Dictionary<String,Any>
                            
                            let imageString = dict["image"] as! String
                            if self.prodImageArray.contains(imageString) == false {
                                self.prodImageArray.append(imageString)
                            }
                            
                            var colorArr = Array<Any>()
                            if dict["color_options"] == nil{
                            }
                            else{
                                
                                let colordict = (dict["color_options"] as? Dictionary<String, Any>)!
                                
                                if colordict.count > 0
                                {
                                    for dc in colordict{
                                        let key = dc.key
                                        let val = dc.value as! Dictionary<String,Any>
                                        
                                        let valImg = val["thumb"] as! String
                                        let valName = val["label"] as! String
                                        
                                        let newDc = ["key" : key,
                                                     "label" : valName,
                                                     "thumb" : valImg]
                                        
                                        if self.colorListArray.contains(valImg) == false {
                                            self.colorListArray.append(valImg)
                                        }
                                        
                                        colorArr.append(newDc)
                                    }
                                }
                            }
                            
                            let catdID = dict["id"] as! String
                            let catInt = NSInteger.init(catdID)
                            
                            if self.prodDArray.contains(catInt!) == false{
                                self.prodDArray.append(catInt!)
                            }
                        }
                    }
                    
                    if objectLocation < self.catIDArray.count - 1 {
                        self.getSub_SubCategoriesforType(catArray: self.catIDArray, objectLocation: objectLocation + 1)
                    }
                    else{
                        // api for product detail
                        self.downloadCategoryImage(object : 0)
//                        self.callDetailAPI()
                    }
                }
                else {
                    let mess = response["message"] as! String
//                    CustomAlertController.showAlert(on: self, with: Simple, andAttributes: ["title":"", "message":mess])
                    let Alert = UIAlertController.init(title: "", message: mess, preferredStyle: UIAlertControllerStyle.alert)
                    Alert.addAction(UIAlertAction.init(title: "OK", style: UIAlertActionStyle.default, handler: { (action:UIAlertAction) in
                        let mo = self.sideMenuController()?.sideMenu?.menuViewController as! MenuTableView
                        mo.tableView(mo.tableView, didSelectRowAt: IndexPath.init(row: 0, section: 0))
                        self.toggleSideMenuView()
                        mo.tableView.selectRow(at: IndexPath.init(row: 0, section: 0), animated: true, scrollPosition: .middle)
                    }))
                    self.present(Alert, animated: true, completion: nil)

                }
            })
            
        }
        else {
            Constants.appDelegate.stopIndicator()
            
            let Alert = UIAlertController.init(title: "", message: "Please check your internet connection.", preferredStyle: UIAlertControllerStyle.alert)
            Alert.addAction(UIAlertAction.init(title: "OK", style: UIAlertActionStyle.default, handler: { (action:UIAlertAction) in
                let mo = self.sideMenuController()?.sideMenu?.menuViewController as! MenuTableView
                mo.tableView(mo.tableView, didSelectRowAt: IndexPath.init(row: 0, section: 0))
                self.toggleSideMenuView()
                mo.tableView.selectRow(at: IndexPath.init(row: 0, section: 0), animated: true, scrollPosition: .middle)
            }))
            self.present(Alert, animated: true, completion: nil)
        }
    }
    
    //MARK:- CATEGORY DOWNLOAD IMAGES
    func downloadCategoryImage(object : NSInteger){
        
        let count = self.categImgArray.count


        UserDefaults.standard.set(count, forKey: Constants.CATEGORY_IMAGE_TOTAL)
        UserDefaults.standard.set(0, forKey: Constants.CATEGORY_IMAGE_CURRENT)
        
        let documentsDirectoryURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
        
        let imgname = self.categImgArray[object]
        
        let imageUrl = URL.init(string: imgname)
        if !FileManager.default.fileExists(atPath: documentsDirectoryURL.appendingPathComponent((imageUrl!.lastPathComponent)).path) {
            
            Helper.downloadImageAsset(imgname, objectNUm: object, completionHandler: { (objectNum) in
                let receivedNum = objectNum
                print("Downloaded Image - \(receivedNum)")
                if receivedNum < count - 1{
                    self.downloadCategoryImage(object: receivedNum + 1)
                }
                else{
                    print("Going to Download Color Images")
                    let when = DispatchTime.now() + 10 // change 90 to desired number of seconds
                    DispatchQueue.main.asyncAfter(deadline: when) {
                        // Your code with delay
                        self.downloadColorListImage(object : 0)
                    }
                }
            })
        }
        else
        {
            print("Incrementing cz Downloaded")
            if object == count - 1{
                let when = DispatchTime.now() + 10 // change 90 to desired number of seconds
                DispatchQueue.main.asyncAfter(deadline: when) {
                    // Your code with delay
                    self.downloadColorListImage(object : 0)
                }
            }
            else{
                self.downloadCategoryImage(object : object + 1)
            }
        }
    }
    
    //MARK:- COLOR LIST DOWNLOAD IMAGES
    func downloadColorListImage(object : NSInteger){
        
        let count = self.colorListArray.count
        print("Color List Count = \(count)")
        
        let documentsDirectoryURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
        
        let imgname = self.colorListArray[object]
        
        let imageUrl = URL.init(string: imgname)
        if !FileManager.default.fileExists(atPath: documentsDirectoryURL.appendingPathComponent((imageUrl!.lastPathComponent)).path) {
            
            Helper.downloadImageAsset(imgname, objectNUm: object, completionHandler: { (objectNum) in
                let receivedNum = objectNum
                print("Downloaded Image - \(receivedNum)")
                if receivedNum < count - 1{
                    self.downloadColorListImage(object: receivedNum + 1)
                }
                else{
                    print("Going to Download Color Images")
                    let when = DispatchTime.now() + 10 // change 90 to desired number of seconds
                    DispatchQueue.main.asyncAfter(deadline: when) {
                        // Your code with delay
                        print("Product List Image Count - \(self.prodImageArray.count)")
                        
                        self.downloadProductListImage(object : 0)
                    }
                }
            })
        }
        else
        {
            print("Incrementing cz Downloaded")
            if object == count - 1{
                let when = DispatchTime.now() + 10 // change 90 to desired number of seconds
                DispatchQueue.main.asyncAfter(deadline: when) {
                    // Your code with delay
                    print("Product List Image Count - \(self.prodImageArray.count)")
                    
                    self.downloadProductListImage(object : 0)
                }
            }
            else{
                self.downloadColorListImage(object : object + 1)
            }
        }
    }
    
    
    //MARK:- PRODUCT LIST DOWNLOAD IMAGES
    func downloadProductListImage(object : NSInteger){
        
        
        let count = self.prodImageArray.count
        print("Product Image Count - \(count)")
        
        let documentsDirectoryURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
        
        let imgname = self.prodImageArray[object]
        let imageUrl = URL.init(string: imgname)
        if !FileManager.default.fileExists(atPath: documentsDirectoryURL.appendingPathComponent((imageUrl!.lastPathComponent)).path) {
            
            Helper.downloadImageAsset(imgname, objectNUm: object, completionHandler: { (objectNum) in
                let receivedNum = objectNum
                print("Downloaded Image - \(receivedNum)")
                if receivedNum < count - 1{
                    self.downloadProductListImage(object: receivedNum + 1)
                }
                else if object == count - 1{
                    print("Going to Download Detail API")
                    let when = DispatchTime.now() + 10 // change 90 to desired number of seconds
                    DispatchQueue.main.asyncAfter(deadline: when) {
                        // Your code with delay
                        Constants.appDelegate.stopIndicator()
                        self.callDetailAPI()
                    }
                }
            })
        }
        else{
            if object == count - 1{
                print("Going to Download Detail API")
                let when = DispatchTime.now() + 10 // change 90 to desired number of seconds
                DispatchQueue.main.asyncAfter(deadline: when) {
                    // Your code with delay
                    Constants.appDelegate.stopIndicator()
                    self.callDetailAPI()
                }
            }
            else{
                self.downloadProductListImage(object : object + 1)
            }
        }
        
    }
    
    func callDetailAPI(){
        if self.prodDArray.count > 0{
            let when = DispatchTime.now() + 1
            DispatchQueue.main.asyncAfter(deadline: when) {
                self.getProductDetailsforType(productArray: self.prodDArray, objectLocation: 0)
            }
        }
    }
    
    // MARK:- FETCH API PRODUCT DETAILS
    func getProductDetailsforType(productArray : Array<NSInteger>, objectLocation : Int){
        
        if Reachability.isConnectedToNetwork() {
            Constants.appDelegate.startIndicator()
            
            let productID = "\(String(describing: productArray[objectLocation]))"
            
            let urlStr = Constants.BASEURL + "productview"
            let param = "product_id=\(productID)"
            
            Server.postRequestWithURL(urlStr, paramString: param, completionHandler: { (response) in
                
                if response[Constants.STATE]?.int32Value  == 1 {
                    
                    let responseData = response["product"] as! Dictionary<String,Any>
                    
                    let prodID = "PROD\(productID)"
                    
                    //Colors images
                    var colorArr = Array<Any>()
                    if responseData["color_options"] == nil{
                    }
                    else{
                        
                        let colordict = (responseData["color_options"] as? Dictionary<String, Any>)!
                        
                        if colordict.count > 0
                        {
                            for dc in colordict{
                                let key = dc.key
                                let val = dc.value as! Dictionary<String,Any>
                                
                                let valImg = val["thumb"] as! String
                                let valName = val["label"] as! String
                                
                                let newDc = ["key" : key,
                                             "label" : valName,
                                             "thumb" : valImg]
                                
                                if self.colorDetailArray.contains(valImg) == false {
                                    self.colorDetailArray.append(valImg)
                                }
                                
                                colorArr.append(newDc)
                            }
                        }
                    }
                    
                    //FEATURE
                    let featureArray = responseData["features"] as! Array<Any>
                    if featureArray.count > 0{
                        for  dc in featureArray{
                            let dict = dc as! Dictionary<String,Any>
                            let imageStr = dict["img"] as? String
                            
                            if dict["type"] as! String == "image"{
                                if self.featureArray.contains(imageStr!) == false {
                                    self.featureArray.append(imageStr!)
                                }
                            }
                        }
                    }
                    
                    //Associated images
                    if responseData["associated_images"] == nil{
                    }
                    else{
                        
                        let associateddict = (responseData["associated_images"] as? Dictionary<String, Any>)!
                        
                        if associateddict.count > 0
                        {
                            for dc in associateddict{
                                let val = dc.value as! Array<Any>
                                
                                for obj in val {
                                    let objc = obj as! Dictionary<String,Any>
                                    let valBIGImg = objc["image_big"] as! String
                                    
                                    if self.prodDetailImageArray.contains(valBIGImg) == false {
                                        self.prodDetailImageArray.append(valBIGImg)
                                    }
                                }
                                
                            }
                        }
                    }
                    
                    
                    UserDefaults.standard.set(responseData, forKey: prodID)
                    
                    if objectLocation < self.prodDArray.count - 1{
                        self.getProductDetailsforType(productArray: self.prodDArray, objectLocation: objectLocation + 1)
                    }
                    else{
                        self.downloadProductColorImage(object: 0)
//                        self.storeTime()
                    }
                }
                else {
                    if objectLocation < self.prodDArray.count - 1{
                        self.getProductDetailsforType(productArray: self.prodDArray, objectLocation: objectLocation + 1)
                    }
                    else{
                        Constants.appDelegate.stopIndicator()
                        var mess = String()
                        if response.count == 0{
                            mess = "Some Error Occurred"
                        }
                        else{
                            mess = response["message"] as! String
                        }
                        let Alert = UIAlertController.init(title: "", message: mess, preferredStyle: UIAlertControllerStyle.alert)
                        Alert.addAction(UIAlertAction.init(title: "OK", style: UIAlertActionStyle.default, handler: { (action:UIAlertAction) in
                            let mo = self.sideMenuController()?.sideMenu?.menuViewController as! MenuTableView
                            mo.tableView(mo.tableView, didSelectRowAt: IndexPath.init(row: 0, section: 0))
                            self.toggleSideMenuView()
                            mo.tableView.selectRow(at: IndexPath.init(row: 0, section: 0), animated: true, scrollPosition: .middle)
                        }))
                        self.present(Alert, animated: true, completion: nil)

//                        CustomAlertController.showAlert(on: self, with: Simple, andAttributes: ["title":"", "message":mess])
                    }
                }
            })
        }
        else {
            Constants.appDelegate.stopIndicator()
            
//            CustomAlertController.showAlert(on: self, with: Simple, andAttributes:["title": "", "message":"Please check your internet connection."])
            let Alert = UIAlertController.init(title: "", message: "Please check your internet connection.", preferredStyle: UIAlertControllerStyle.alert)
            Alert.addAction(UIAlertAction.init(title: "OK", style: UIAlertActionStyle.default, handler: { (action:UIAlertAction) in
                let mo = self.sideMenuController()?.sideMenu?.menuViewController as! MenuTableView
                mo.tableView(mo.tableView, didSelectRowAt: IndexPath.init(row: 0, section: 0))
                self.toggleSideMenuView()
                mo.tableView.selectRow(at: IndexPath.init(row: 0, section: 0), animated: true, scrollPosition: .middle)
            }))
            self.present(Alert, animated: true, completion: nil)

        }
    }
    
    
    //MARK:- DOWNLOAD COLOR DETAIL IMAGES
    func downloadProductColorImage(object : NSInteger){
        
        print("Product Color Detail Image Count - \(self.colorDetailArray.count)")
        let tot = self.colorDetailArray.count
        UserDefaults.standard.set(tot, forKey: Constants.COLOR_DETAIL_TOTAL)
        
        let documentsDirectoryURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
        
        let imgname = self.colorDetailArray[object]
        
        let imageUrl = URL.init(string: imgname)
        if !FileManager.default.fileExists(atPath: documentsDirectoryURL.appendingPathComponent((imageUrl!.lastPathComponent)).path) {
            
            Helper.downloadImageAsset(imgname, objectNUm: object, completionHandler: { (objectNum) in
                let receivedNum = objectNum
                print("Downloaded Image - \(receivedNum)")
                if receivedNum < tot - 1{
                    self.downloadProductColorImage(object: receivedNum + 1)
                }
                else if object == tot - 1{
                    print("Going to Download Feature Images")
                    let when = DispatchTime.now() + 10 // change 90 to desired number of seconds
                    DispatchQueue.main.asyncAfter(deadline: when) {
                        // Your code with delay
                        self.downloadFeatureImage(object: 0)
                    }
                }
            })
        }
        else{
            if object == tot - 1{
                print("Going to Download Feature Images")
                let when = DispatchTime.now() + 10 // change 90 to desired number of seconds
                DispatchQueue.main.asyncAfter(deadline: when) {
                    // Your code with delay
                    self.downloadFeatureImage(object: 0)
                }
            }
            else{
                print("Incrementing cz Downloaded")
                
                self.downloadProductColorImage(object: object + 1)
            }
        }
    }
    
    //MARK:- DOWNLOAD FEATURE IMAGES
    func downloadFeatureImage(object : NSInteger){
        let tot = self.featureArray.count
        print("FEATURE IMAGE DOWNLOAD \(tot)")
        
        let documentsDirectoryURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
        
        let imgname = self.featureArray[object]
        let imageUrl = URL.init(string: imgname)
        if !FileManager.default.fileExists(atPath: documentsDirectoryURL.appendingPathComponent((imageUrl!.lastPathComponent)).path) {
            
            Helper.downloadImageAsset(imgname, objectNUm: object, completionHandler: { (objectNum) in
                let receivedNum = objectNum
                print("Downloaded Image - \(receivedNum)")
                if receivedNum < tot - 1{
                    self.downloadFeatureImage(object: receivedNum + 1)
                }
                else if object == tot - 1{
                    print("Going to Download Detail Images")
                    let when = DispatchTime.now() + 10 // change 90 to desired number of seconds
                    DispatchQueue.main.asyncAfter(deadline: when) {
                        // Your code with delay
                        self.downloadProductDetailImage(object: 0)
                    }
                }
            })
        }
        else{
            if object == tot - 1{
                print("Going to Download Detail Images")
                let when = DispatchTime.now() + 10 // change 90 to desired number of seconds
                DispatchQueue.main.asyncAfter(deadline: when) {
                    // Your code with delay
                    self.downloadProductDetailImage(object: 0)
                }
            }
            else{
                self.downloadFeatureImage(object: object + 1)
            }
        }
    }
    
    //MARK:- DOWNLOAD Product Detail Images
    func downloadProductDetailImage(object : NSInteger){
        print("PRODUCT DETAIL IMAGE DOWNLOAD \(self.prodDetailImageArray.count)")
        let tot = self.prodDetailImageArray.count
        
        let documentsDirectoryURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
        
        let imgname = self.prodDetailImageArray[object]
        let imageUrl = URL.init(string: imgname)
        if !FileManager.default.fileExists(atPath: documentsDirectoryURL.appendingPathComponent((imageUrl!.lastPathComponent)).path) {
            Helper.downloadImageAsset(imgname, objectNUm: object, completionHandler: { (objectNum) in
                let receivedNum = objectNum
                print("Downloaded Image - \(receivedNum)")
                if receivedNum < tot - 1{
                    self.downloadProductDetailImage(object: receivedNum + 1)
                }
                else if object == tot - 1{
                    print("Going to Download Detail Images")
                    self.storeTime()
                }
            })
        }
        else{
            print("Incrementing cz Downloaded - \(object)")
            if object == tot - 1{
                print("Going to Save timeStamp")
                self.storeTime()
            }
            else{
                self.downloadProductDetailImage(object: object + 1)
            }
        }
    }
    
    //MARK:- STOre TIMESTAMP
    func storeTime(){
        
        
        let datetime = Date()
        print(datetime)
        UserDefaults.standard.set(datetime, forKey: "TIMESTAMP")
        let when = DispatchTime.now() + 5 // change 2 to desired number of seconds
        DispatchQueue.main.asyncAfter(deadline: when) {
            // Your code with delay
            Constants.appDelegate.stopIndicator()
            let Alert = UIAlertController.init(title: "App Data Updated Successfully.", message: "", preferredStyle: UIAlertControllerStyle.alert)
            Alert.addAction(UIAlertAction.init(title: "OK", style: UIAlertActionStyle.default, handler: { (action:UIAlertAction) in
                let mo = self.sideMenuController()?.sideMenu?.menuViewController as! MenuTableView
                mo.tableView(mo.tableView, didSelectRowAt: IndexPath.init(row: 0, section: 0))
                self.toggleSideMenuView()
                mo.tableView.selectRow(at: IndexPath.init(row: 0, section: 0), animated: true, scrollPosition: .middle)
            }))
            self.present(Alert, animated: true, completion: nil)

        }
        //        }
    }

    
}
