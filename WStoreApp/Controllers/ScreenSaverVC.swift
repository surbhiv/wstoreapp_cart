//
//  ScreenSaverVC.swift
//  WStoreApp
//
//  Created by Surbhi on 16/06/17.
//  Copyright © 2017 SurbhiV. All rights reserved.
//

import UIKit
import AVFoundation
import AVKit
import Photos
import Firebase

class ScreenSaverVC: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout, UIScrollViewDelegate {
    
    @IBOutlet weak var myCollect: UICollectionView!
    var imgArray = Array<AnyObject>()
    var timer = Timer()
    var isTimerOn = false
    
    var controller = AVPlayerViewController()
    var player = AVPlayer()
    
    //MARK:- ViewDID Load
    override func viewDidLoad() {
        super.viewDidLoad()
        let screenImg = UserDefaults.standard.array(forKey: Constants.SCREENSAVER)
        if (screenImg?.count)! > 0{
            self.imgArray = screenImg! as Array<AnyObject>

            DispatchQueue.main.async {
                Constants.appDelegate.isScreenSaverOn = true
                self.myCollect.reloadData()
                if  self.isTimerOn == false {
                    self.addTimer()
                }
            }
        }
        else{
            self.getImagesToDisplay()
        }
        
        // Do any additional setup after loading the view.
        NotificationCenter.default.addObserver(self, selector: #selector(videoPlayingEnnded), name: NSNotification.Name.AVPlayerItemDidPlayToEndTime, object: nil)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        
            self.removeTimer()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        if imgArray.count > 0 {
            
            if  isTimerOn == false {
                addTimer()
            }
            Constants.appDelegate.isScreenSaverOn = false
        }
    }
    
    
    override var prefersStatusBarHidden : Bool {
        return true
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func dismissView(_ sender: AnyObject) {
        self.player.pause()
        self.dismiss(animated: true, completion: nil)
    }
    
    //MARK:- GET API DATA
    func getImagesToDisplay(){
        
        if Reachability.isConnectedToNetwork() {
            Constants.appDelegate.startIndicator()
            
            let urlStr = "http://neuronimbusinteractive.com/screensaver/get_image.php"
            Server.getRequestWithURL(urlString: urlStr, completionHandler: { (response) in
                Constants.appDelegate.stopIndicator()
                
                if response[Constants.STATE]?.int32Value  == 1 {
                    
                    self.imgArray = response["data_ios"] as! Array<AnyObject>
                    UserDefaults.standard.set(self.imgArray, forKey: Constants.SCREENSAVER)

                    DispatchQueue.main.async {
                        Constants.appDelegate.isScreenSaverOn = true
                        self.myCollect.reloadData()

                        if self.imgArray.count > 1{
                            
                            let newImg = self.imgArray[self.imgArray.count - 1] as! Dictionary<String,Any>
                            let newmm = newImg["image"] as! String
                            let newvid = ["video" : "http://neuronimbusinteractive.com/screensaver/videos/Whirlpool3videoloop4.mp4",
                                         "image" : newmm]
                            let count = self.imgArray.count
                            self.imgArray.insert(newvid as AnyObject, at: count)

                            if  self.isTimerOn == false {
                                self.addTimer()
                            }
                        }
                    }
                }
                else {
                        let mess = response["message"] as! String
                        CustomAlertController.showAlert(on: self, with: Simple, andAttributes: ["title":"", "message":mess])
                }

            })
        }
        else {
            let screenImg = UserDefaults.standard.array(forKey: Constants.SCREENSAVER)
            if (screenImg?.count)! > 0{
                self.imgArray = screenImg! as Array<AnyObject>
            }
            else{
                CustomAlertController.showAlert(on: self, with: Simple, andAttributes:["title": "", "message":"Please check your internet connection."])
            }

        }
    }
    
    //MARK:- COLLECTION FLOW LAYOUT
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize
    {
        return CGSize(width: ScreenSize.SCREEN_MAX_LENGTH, height: ScreenSize.SCREEN_MAX_LENGTH)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        if DeviceType.IS_IPAD{
            return 0
        }
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        if DeviceType.IS_IPAD{
            return 0
        }
        return 0
        
    }

    //MARK:- COLLECTIONVIEW
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return imgArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "imageViewCell", for: indexPath)
        let dict = self.imgArray[indexPath.item] as! Dictionary<String,AnyObject>
        let img = cell.viewWithTag(501) as! UIImageView
        img.backgroundColor = UIColor.black
        let documentsDirectoryURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!


        let imgName = dict["image"] as? String
        if imgName == nil {
        }
        else{
            let imageUrl = URL.init(string: imgName!)

            if !FileManager.default.fileExists(atPath: documentsDirectoryURL.appendingPathComponent((imageUrl?.lastPathComponent)!).path) {
//                Helper.downloadImageLinkAndCreateAsset(imgName!)
                if Reachability.isConnectedToNetwork() == true {
                    Helper.downloadImageLinkAndCreateAsset(imgName!)
                    img.sd_setImage(with: URL.init(string: imgName!))
                }
                else{
                    img.image = UIImage.init(named: "noImage.png")
                }

            }
            else {
                
                let abc = documentsDirectoryURL.appendingPathComponent(imageUrl!.lastPathComponent).path
                let imageURL = URL(fileURLWithPath: abc)
                img.image = UIImage(contentsOfFile: imageURL.path)
            }
        }
//        img.sd_setImage(with: URL.init(string: imgName!))
        
        
        let videostr = dict["video"] as? String
        if videostr == nil {
        }
        else{
//            self.perform(#selector(videoDownLoadOrPlay), with: videostr!, afterDelay: 4.0)
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        
        let dict = self.imgArray[indexPath.item] as! Dictionary<String,AnyObject>
        
        if indexPath.row == self.imgArray.count - 1 {
            let videostr = dict["video"] as? String
            if videostr == nil {
            }
            else{
                self.videoDownLoadOrPlay(videostr: videostr!)
            }
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        DispatchQueue.main.async(execute: { () -> Void in
            NSObject.cancelPreviousPerformRequests(withTarget: self, selector: #selector(self.videoDownLoadOrPlay), object: nil)
            self.controller.player!.pause()
            self.controller.view.removeFromSuperview()
            self.player.pause()
            self.dismissView(self.controller)
            self.dismiss(animated: true, completion: nil)
            Constants.appDelegate.isScreenSaverOn = false
        })
    }

    
    //MARK:- Video Player
    func videoPlayingEnnded()
    {
        DispatchQueue.main.async(execute: { () -> Void in
            self.dismissView(self.controller)
        })
    }
    
    
    func videoDownLoadOrPlay(videostr : String){

        self.removeTimer()
        let videoUrl = URL.init(string: videostr)
        let documentsDirectoryURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!

        if !FileManager.default.fileExists(atPath: documentsDirectoryURL.appendingPathComponent((videoUrl?.lastPathComponent)!).path) {
            Helper.downloadVideoLinkAndCreateAsset(videostr, objectNum: 0)
        }
        else {
            NSObject.cancelPreviousPerformRequests(withTarget: self, selector: #selector(self.videoDownLoadOrPlay), object: nil)

            DispatchQueue.main.async(execute: { () -> Void in
                let abc = documentsDirectoryURL.appendingPathComponent(videoUrl!.lastPathComponent).path
                self.player = AVPlayer.init(url: URL.init(fileURLWithPath:abc ))
                self.controller.player = self.player
                self.controller.view.frame = self.view.frame
                self.player.play()
                self.showDetailViewController(self.controller, sender: self)
            })
        }
    }
    
    
    //MARK: - AUTO SCROLLING
    func addTimer()
    {
        isTimerOn = true
        timer = Timer.scheduledTimer(timeInterval: 3.5, target: self, selector: #selector(nextPage), userInfo: nil, repeats: true)
        RunLoop.current.add(timer, forMode: RunLoopMode.commonModes)
    }
    
    func nextPage()
    {
        let ar = myCollect.indexPathsForVisibleItems
        if imgArray.count > 0
        {
            let MaxSections = imgArray.count
            var currentIndexPathReset = ar.first
            if(currentIndexPathReset?.item == MaxSections - 1)
            {
                currentIndexPathReset = IndexPath(item:0, section:0)
                myCollect.scrollToItem(at: currentIndexPathReset!, at: UICollectionViewScrollPosition.left, animated: false)
            }
            else
            {
                if currentIndexPathReset != nil
                {
                    let newIndex = currentIndexPathReset!.item + 1
                    myCollect.scrollToItem(at: IndexPath.init(item: newIndex, section: 0), at: UICollectionViewScrollPosition.centeredHorizontally, animated: true)
                }
            }
        }
    }
    
    
    func removeTimer()
    {
        // stop NSTimer
        isTimerOn = false
        timer.invalidate()
    }

    //MARK:- SCROLLVIEW
    // UIScrollView' delegate method
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView)
    {
        removeTimer()
    }
    
    
    func scrollViewDidEndDragging(_ scrollView: UIScrollView,
                                  willDecelerate decelerate: Bool)
    {
        if  isTimerOn == false {
            addTimer()
        }
    }
    

}
