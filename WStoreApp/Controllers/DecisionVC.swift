//
//  DecisionVC.swift
//  WStoreApp
//
//  Created by Surbhi on 01/09/17.
//  Copyright © 2017 SurbhiV. All rights reserved.
//

import UIKit

// fo deciding whether the app will run in transaction mode or not transaction Mode.
class DecisionVC: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func TransactionModePressed(_ sender: AnyObject) {
        UserDefaults.standard.set(true, forKey: Constants.isTransactionEnabled)
        let viewController = Constants.mainStoryboard.instantiateViewController(withIdentifier: "SideMenuNavigation")
        UIApplication.shared.keyWindow?.rootViewController = viewController;

    }

    @IBAction func NonTransactionModePressed(_ sender: AnyObject) {
        
        UserDefaults.standard.set(false, forKey: Constants.isTransactionEnabled)
        let viewController = Constants.mainStoryboard.instantiateViewController(withIdentifier: "SideMenuNavigation")
        UIApplication.shared.keyWindow?.rootViewController = viewController;

    }


}
