//
//  ProductListVC.swift
//  WStoreApp
//
//  Created by Surbhi on 14/06/17.
//  Copyright © 2017 SurbhiV. All rights reserved.
//

import UIKit
import Firebase

class ProductListVC: UIViewController , UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout{

    @IBOutlet weak var prodListCollection: UICollectionView!
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var backButtonImage: UIImageView!
    @IBOutlet weak var cartButton: UIButton!
    @IBOutlet weak var cartCountLable: UILabel!

    var listButton = UIButton()
    var gridButton = UIButton()
    

    var isDirectView = Bool()
    var ProductType = String()
    var catID = String()
    var prodArray = Array<Any>()
    var isListView = false
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if self.isDirectView == true {
            backButtonImage.image = UIImage.init(named: "sideMenu")
        }
        else{
            backButtonImage.image = UIImage.init(named: "backARROW")
        }

        
        let dict = UserDefaults.standard.value(forKey: self.catID) as! Dictionary<String,Any>
        let arr = dict["data"] as! Array<Any>
        if arr.count > 0 {
            self.prodArray = dict["data"] as! Array<Any>
            DispatchQueue.main.async {
                self.prodListCollection.isHidden = false
                self.view.bringSubview(toFront: self.prodListCollection)
                self.prodListCollection.reloadData()
            }
        }
        else{
            self.getProductDetailsforType()
        }
//        self.getProductDetailsforType()
    }

    override func viewWillAppear(_ animated: Bool) {
        sideMenuController()?.sideMenu?.allowPanGesture = false
        sideMenuController()?.sideMenu?.allowLeftSwipe = false
        sideMenuController()?.sideMenu?.allowRightSwipe = false
        
        cartCountLable.layer.cornerRadius = 7.5
        cartCountLable.clipsToBounds = true
        
        if UserDefaults.standard.bool(forKey: Constants.isTransactionEnabled) == false{
            self.cartButton.isHidden = true
            if UserDefaults.standard.object(forKey: Constants.CART_Array) == nil{
                cartCountLable.isHidden = true
            }
            else{
                let arr = UserDefaults.standard.object(forKey: Constants.CART_Array) as! Array<Any>
                let count = arr.count
                cartCountLable.isHidden = false
                cartCountLable.text = "\(count)"
            }
        }
        else{
            self.cartButton.isHidden = false
            if UserDefaults.standard.object(forKey: Constants.CART_Array) == nil{
                cartCountLable.isHidden = true
            }
            else{
                let arr = UserDefaults.standard.object(forKey: Constants.CART_Array) as! Array<Any>
                let count = arr.count
                cartCountLable.isHidden = false
                cartCountLable.text = "\(count)"
            }
        }


    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    override var prefersStatusBarHidden : Bool {
        return true
    }
    
    
    // MARK:- FETCH API
    func getProductDetailsforType(){
        
        if Reachability.isConnectedToNetwork() {
            Constants.appDelegate.startIndicator()
            
            let urlStr = Constants.BASEURL + "productlisting"
            let param = "brandshop_id=\(String(describing: UserDefaults.standard.object(forKey: Constants.UserID)!))&category_id=\(self.catID)"
            
            Server.postRequestWithURL(urlStr, paramString: param, completionHandler: { (response) in
                
                
                if response[Constants.STATE]?.int32Value  == 1 {
                    
                    
                    self.prodArray = response["data"] as! Array<Any>
                    
                    UserDefaults.standard.setValue(response, forKeyPath: self.catID)

                    DispatchQueue.main.async {
                        self.prodListCollection.isHidden = false
                        self.view.bringSubview(toFront: self.prodListCollection)
                        self.prodListCollection.reloadData()
                    }
                    Constants.appDelegate.stopIndicator()
                }
                else {
                    let dict = UserDefaults.standard.value(forKey: self.catID) as! Array<AnyObject>
                    if dict.count > 0 {
                        self.prodArray = UserDefaults.standard.value(forKey: self.catID) as! Array<AnyObject>
                        Constants.appDelegate.stopIndicator()

                        DispatchQueue.main.async {
                            self.prodListCollection.isHidden = false
                            self.view.bringSubview(toFront: self.prodListCollection)
                            self.prodListCollection.reloadData()
                        }
                    }
                    else{
                        Constants.appDelegate.stopIndicator()
                        var mess = String()
                        if response.count == 0{
                            mess = "Some Error Occurred"
                        }
                        else{
                            mess = response["message"] as! String
                        }
                        CustomAlertController.showAlert(on: self, with: Simple, andAttributes: ["title":"", "message":mess])
                    }
                }
            })
        }
        else {
            Constants.appDelegate.stopIndicator()
            self.prodArray = UserDefaults.standard.array(forKey: self.catID)! as Array<Any>
            if self.prodArray.count > 0
            {
                DispatchQueue.main.async {
                    self.prodListCollection.isHidden = false
                    self.view.bringSubview(toFront: self.prodListCollection)
                    self.prodListCollection.reloadData()
                }
            }
            else{
                CustomAlertController.showAlert(on: self, with: Simple, andAttributes:["title": "", "message":"Please check your internet connection."])
            }
        }
    }
    
    
    //MARK:- BUTTON ACTIONS
    
    @IBAction func BackButtonPressed(_ sender: Any) {
        
        if self.isDirectView == true {
            self.toggleSideMenuView()
        }
        else{
            self.navigationController?.popViewController(animated: true)
        }
    }
    
    func ListButtonPressed() {
        
        isListView = true
        self.gridButton.isSelected = false
        self.listButton.isSelected = true
        
        Constants.appDelegate.startIndicator()
        UIView.animate(withDuration: 0.5, delay: 1.0, options: UIViewAnimationOptions(), animations: { () -> Void in
            
            self.prodListCollection.reloadData()
            }, completion: { (finished: Bool) -> Void in
                
                self.prodListCollection.isHidden = false
                Constants.appDelegate.stopIndicator()
        })
    }
    
    func GridButtonPressed() {

        isListView = false
        self.gridButton.isSelected = true
        self.listButton.isSelected = false
        
        Constants.appDelegate.startIndicator()
        UIView.animate(withDuration: 0.5, delay: 1.0, options: UIViewAnimationOptions(), animations: { () -> Void in
            
            self.prodListCollection.reloadData()
            
            }, completion: { (finished: Bool) -> Void in
                
                self.prodListCollection.isHidden = false
                Constants.appDelegate.stopIndicator()
        })
    }

    // MARK:- COLLECTION VIEW FLOWLAYOUT
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize
    {
        if isListView == false {
//            let height = 460 + 
            let dict = self.prodArray[indexPath.item] as! Dictionary<String,Any>
            let nameHt = Helper.getHeightForText((dict["name"] as? String)!, fon: UIFont.init(name: FONTS.Helvetica_Regular, size: 22)!, width: ((ScreenSize.SCREEN_MAX_LENGTH - 30)/2 - 20))
            return CGSize(width: (ScreenSize.SCREEN_MAX_LENGTH - 30)/2, height: nameHt + 460)//(ScreenSize.SCREEN_MAX_LENGTH - 30)/2
        }
        else{
            return CGSize(width: ScreenSize.SCREEN_MAX_LENGTH - 10, height: 350)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 10
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 10
    }
    
    // MARK:- COLLECTION VIEW
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
            return CGSize.init(width: 1014, height: 165)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForFooterInSection section: Int) -> CGSize {
            return CGSize.init(width: 1014, height: 120)
    }
    
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        
        if kind == UICollectionElementKindSectionHeader
        {
            let hview = collectionView.dequeueReusableSupplementaryView(ofKind: UICollectionElementKindSectionHeader, withReuseIdentifier: "CollectionHeader", for: indexPath) as! CollectionHeader
            
            var strArr = self.ProductType.uppercased().components(separatedBy: " ")
            if strArr.count == 1 {
                let part2 = NSAttributedString(string: "\(self.ProductType.uppercased())",  attributes: [NSForegroundColorAttributeName : UIColor.black, NSFontAttributeName : UIFont(name: FONTS.Helvetica_Medium, size: 50)!])

                hview.headingLabel.attributedText = part2

            }
            else{
                let part2 = NSAttributedString(string: "\n\(strArr.last!)",  attributes: [NSForegroundColorAttributeName : UIColor.black, NSFontAttributeName : UIFont(name: FONTS.Helvetica_Medium, size: 50)!])
                strArr.removeLast()
                let part1Str = strArr.joined(separator: " ")
                let part1 = NSAttributedString(string: part1Str,  attributes: [NSForegroundColorAttributeName : Colors.appThemeColorText, NSFontAttributeName : UIFont(name: FONTS.Helvetica_Regular, size: 25)!])
                
                let attriStr = NSMutableAttributedString.init()
                attriStr.append(part1)
                attriStr.append(part2)
                
                hview.headingLabel.attributedText = attriStr
 
            }
            
            
            listButton = hview.listButton
            listButton.addTarget(self, action: #selector(ListButtonPressed), for: .touchUpInside)
            
            gridButton = hview.gridButton
            gridButton.addTarget(self, action: #selector(GridButtonPressed), for: .touchUpInside)

            
            return hview
        }
        else{
            let hview = collectionView.dequeueReusableSupplementaryView(ofKind: UICollectionElementKindSectionFooter, withReuseIdentifier: "FooterCollectionCell", for: indexPath) as! FooterCollectionCell
            return hview
            
        }
    }

    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return prodArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if isListView == false {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ProductListCell", for: indexPath) as! ProductListCell
            let dict = self.prodArray[indexPath.item] as! Dictionary<String,Any>
            
            cell.prodName.text = dict["name"] as? String
//            cell.prodImage.sd_setImage(with: URL.init(string: (dict["image"] as? String)!))
            let imgSTR = dict["image"] as! String
            let imageUrl = URL.init(string: imgSTR)
            
            let documentsDirectoryURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
            if !FileManager.default.fileExists(atPath: documentsDirectoryURL.appendingPathComponent((imageUrl!.lastPathComponent)).path) {
//                Helper.downloadImageLinkAndCreateAsset(imgSTR)
                if Reachability.isConnectedToNetwork() == true {
                    Helper.downloadImageLinkAndCreateAsset(imgSTR)
                    cell.prodImage.sd_setImage(with: URL.init(string: imgSTR))
                }
                else{
                    cell.prodImage.image = UIImage.init(named: "noImage.png")
                }

            }
            else{
                let abc = documentsDirectoryURL.appendingPathComponent(imageUrl!.lastPathComponent).path
                let imageURL = URL(fileURLWithPath: abc)
                cell.prodImage.image = UIImage(contentsOfFile: imageURL.path)

            }

            
            
            if dict["final_price"] == nil  {
                cell.prodSellingPrice.isHidden = true
            }
            else{
                cell.prodSellingPrice.isHidden = false
                cell.prodSellingPrice.text = "SELLING PRICE: ₹\(dict["final_price"]!)"
            }
            
            
            let attr = [NSForegroundColorAttributeName: UIColor.black, NSFontAttributeName: UIFont(name: FONTS.Helvetica_Medium, size: 18)!] as [String : Any]//,NSStrikethroughStyleAttributeName : 2
            
            if dict["price"] == nil  {
                cell.prodOldPrice.isHidden = true
            }
            else{
                cell.prodOldPrice.isHidden = false
                let oldP = NSMutableAttributedString.init(string: "MRP: ₹\(dict["price"]!)", attributes: attr)
                cell.prodOldPrice.attributedText = oldP
                
            }
            
            var colorArr = Array<Any>()
            if dict["color_options"] == nil{
            }
            else{
                
               let colordict = (dict["color_options"] as? Dictionary<String, Any>)!
                
                if colordict.count > 0
                {
                    for dc in colordict{
                        let key = dc.key
                        let val = dc.value as! Dictionary<String,Any>
                        
                        let valImg = val["thumb"] as! String
                        let valName = val["label"] as! String
                        
                        let newDc = ["key" : key,
                                     "label" : valName,
                                     "thumb" : valImg]
                        
                        colorArr.append(newDc)
                    }
                }
            }
            
            let stacksubview = cell.prodStack.subviews
            if stacksubview.count > 0{
                for sub in stacksubview{
                    sub.removeFromSuperview()
                }
            }

            if colorArr.count > 0{
                for arrayIndex in 0 ..< colorArr.count {
                    let dc = colorArr[arrayIndex] as! Dictionary<String,Any>
                    let url = dc["thumb"] as! String
                    
                    let xposition = (arrayIndex == 0 ? 0 : arrayIndex * 40)
                    let but = UIImageView.init(frame: CGRect.init(x: xposition , y: 0, width: 30, height: 30))
                    
                    
//                    but.sd_setImage(with: NSURL.init(string: url)! as URL)
                    let imageUrl = URL.init(string: url)
                    
                    let documentsDirectoryURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
                    if !FileManager.default.fileExists(atPath: documentsDirectoryURL.appendingPathComponent((imageUrl!.lastPathComponent)).path) {
//                        Helper.downloadImageLinkAndCreateAsset(url)
                        if Reachability.isConnectedToNetwork() == true {
                            Helper.downloadImageLinkAndCreateAsset(url)
                            but.sd_setImage(with: URL.init(string: url))
                        }
                        else{
                            but.image = UIImage.init(named: "noImage.png")
                        }

                    }
                    else{
                        let abc = documentsDirectoryURL.appendingPathComponent(imageUrl!.lastPathComponent).path
                        let imageURL = URL(fileURLWithPath: abc)
                        but.image = UIImage(contentsOfFile: imageURL.path)
                    }

                    cell.prodStack.addSubview(but)
                }
            }
            cell.prodStackWidth.constant = CGFloat(40 * colorArr.count)

            
            return cell
        }
        else{
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "prodListSingleCell", for: indexPath) as! prodListSingleCell
            let dict = self.prodArray[indexPath.item] as! Dictionary<String,Any>
            
            cell.prodName.text = dict["name"] as? String
            
            let imgSTR = dict["image"] as! String
            let imageUrl = URL.init(string: imgSTR)
            
            let documentsDirectoryURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
            if !FileManager.default.fileExists(atPath: documentsDirectoryURL.appendingPathComponent((imageUrl!.lastPathComponent)).path) {
//                Helper.downloadImageLinkAndCreateAsset(imgSTR)
                if Reachability.isConnectedToNetwork() == true {
                    Helper.downloadImageLinkAndCreateAsset(imgSTR)
                    cell.prodImage.sd_setImage(with: URL.init(string: imgSTR))
                }
                else{
                    cell.prodImage.image = UIImage.init(named: "noImage.png")
                }

            }
            else{
                let abc = documentsDirectoryURL.appendingPathComponent(imageUrl!.lastPathComponent).path
                let imageURL = URL(fileURLWithPath: abc)
                cell.prodImage.image = UIImage(contentsOfFile: imageURL.path)
            }
            
//            cell.prodImage.sd_setImage(with: URL.init(string: (dict["image"] as? String)!))
            
            if dict["final_price"] == nil  {
                cell.prodSellingPrice.isHidden = true
            }
            else{
                cell.prodSellingPrice.isHidden = false
                cell.prodSellingPrice.text = "SELLING PRICE: ₹\(dict["final_price"]!)"
            }
            
            
            let attr = [NSForegroundColorAttributeName: UIColor.black, NSFontAttributeName: UIFont(name: FONTS.Helvetica_Medium, size: 18)!] as [String : Any]//,NSStrikethroughStyleAttributeName : 3
            
            if dict["price"] == nil  {
                cell.prodOldPrice.isHidden = true
            }
            else{
                cell.prodOldPrice.isHidden = false
                let oldP = NSMutableAttributedString.init(string: "MRP: ₹\(dict["price"]!)", attributes: attr)
                cell.prodOldPrice.attributedText = oldP
            }
            
            
            let stacksubview = cell.prodStack.subviews
            if stacksubview.count > 0{
                for sub in stacksubview{
                    sub.removeFromSuperview()
                }
            }
            
            var colorArr = Array<Any>()
            if dict["color_options"] == nil{
            }
            else{
                
                let colordict = (dict["color_options"] as? Dictionary<String, Any>)!
                
                if colordict.count > 0
                {
                    for dc in colordict{
                        let key = dc.key
                        let val = dc.value as! Dictionary<String,Any>
                        
                        let valImg = val["thumb"] as! String
                        let valName = val["label"] as! String
                        
                        let newDc = ["key" : key,
                                     "label" : valName,
                                     "thumb" : valImg]
                        
                        colorArr.append(newDc)
                    }
                }
            }
            
            
            if colorArr.count > 0{
                for arrayIndex in 0 ..< colorArr.count {
                    let dc = colorArr[arrayIndex] as! Dictionary<String,Any>
                    let url = dc["thumb"] as! String
                    let xposition = (arrayIndex == 0 ? 0 : arrayIndex * 40)
                    let but = UIImageView.init(frame: CGRect.init(x: xposition , y: 0, width: 30, height: 30))
//                    but.sd_setImage(with: NSURL.init(string: url)! as URL)
                    
                    let imageUrl = URL.init(string: url)
                    
                    let documentsDirectoryURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
                    if !FileManager.default.fileExists(atPath: documentsDirectoryURL.appendingPathComponent((imageUrl!.lastPathComponent)).path) {
//                        Helper.downloadImageLinkAndCreateAsset(url)
                        if Reachability.isConnectedToNetwork() == true {
                            Helper.downloadImageLinkAndCreateAsset(url)
                            but.sd_setImage(with: URL.init(string: url))
                        }
                        else{
                            but.image = UIImage.init(named: "noImage.png")
                        }

                    }
                    else{
                        let abc = documentsDirectoryURL.appendingPathComponent(imageUrl!.lastPathComponent).path
                        let imageURL = URL(fileURLWithPath: abc)
                        but.image = UIImage(contentsOfFile: imageURL.path)

                    }

                    cell.prodStack.addSubview(but)
                }
            }
            cell.prodStackWidth.constant = CGFloat(40 * colorArr.count)
            return cell
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let dict = self.prodArray[indexPath.item] as! Dictionary<String,Any>
        let destViewController = Constants.mainStoryboard.instantiateViewController(withIdentifier: "ProductDetailViewVC") as! ProductDetailViewVC
        destViewController.productID = "\(dict["id"]!)"
        self.navigationController?.pushViewController(destViewController, animated: true)
        
    }
    
}
