//
//  CartVC.swift
//  WStoreApp
//
//  Created by Surbhi on 17/08/17.
//  Copyright © 2017 SurbhiV. All rights reserved.
//

import UIKit

class CartVC: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource,UICollectionViewDelegateFlowLayout, UITableViewDelegate, UITableViewDataSource, UITextFieldDelegate {

    @IBOutlet weak var cartCollection: UICollectionView!
    @IBOutlet weak var cartButton: UIButton!
    var cartArray = Array<Any>()
    var shoppingTable = UITableView()
    var discountText = UITextField()
    var discountAmount = 0
    
    var subtotalAmount = 0
    var grandTotal = 0
    var vwLoaded : Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if UserDefaults.standard.array(forKey: Constants.CART_Array) == nil{
            
        }
        else{
            cartArray = UserDefaults.standard.array(forKey: Constants.CART_Array)!
        }
        
        if UserDefaults.standard.object(forKey: Constants.DISCOUNTApplied) == nil || UserDefaults.standard.integer(forKey: Constants.DISCOUNTApplied) == 0{
            self.discountAmount = 0
        }
        else{
            self.discountAmount = UserDefaults.standard.integer(forKey: Constants.DISCOUNTApplied)
        }
        
        self.fetchCartApi(isEmpty: false)
    }

    override func viewWillAppear(_ animated: Bool) {
       
        if vwLoaded == true{
            self.fetchCartApi(isEmpty: false)
        }
        vwLoaded = true
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override var prefersStatusBarHidden : Bool {
        return true
    }
    
    @IBAction func BackButtonPressed(_ sender: AnyObject) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    //MARK:- GET CART
    func fetchCartApi(isEmpty : Bool){
        
        var arrayPar = Array<Any>()
        var pincode = String()
        if cartArray.count > 0{
            for index in 0 ... cartArray.count - 1 {
                let dict = cartArray[index] as! Dictionary<String,Any>
                
                let quant = dict["p_Quantity"] as! String
                let sku = dict["p_SKU"] as! String
                let productID = dict["p_ID"] as! String
                
                let newDict = ["sku":sku,
                               "qty":quant,
                               "product_id":productID]
                
                arrayPar.append(newDict)
                
                if index == cartArray.count - 1{
                    pincode = (dict["pincode"] as! String)
                }
            }
        }
        else{
            arrayPar = []
        }
        
        var quoteID = ""
        if UserDefaults.standard.object(forKey: Constants.QuoteID) == nil{
            quoteID = ""
        }
        else{
            quoteID = "\(UserDefaults.standard.object(forKey: Constants.QuoteID)!)"
        }
        

        if Reachability.isConnectedToNetwork() {
            Constants.appDelegate.startIndicator()
            
            self.discountText.resignFirstResponder()
            
            let urlStr = Constants.BASEURL + "getCart"
            
            let jsonData_Cart = try! JSONSerialization.data(withJSONObject: arrayPar, options: JSONSerialization.WritingOptions.prettyPrinted)
            var jsonString_Cart = NSString(data: jsonData_Cart, encoding: String.Encoding.utf8.rawValue)! as String
            jsonString_Cart = jsonString_Cart.replacingOccurrences(of: "\\n", with: "")
            
            
            var paramStr = "cart_items=\(jsonString_Cart)&quote_id=\(quoteID)&empty_cart="
            paramStr = "cart_items="+jsonString_Cart+"&quote_id="+quoteID+"&empty_cart=&pincode="+pincode

            if isEmpty == true {
                paramStr = "cart_items=\(jsonString_Cart)&quote_id=\(quoteID)&empty_cart=1"
                paramStr = "cart_items="+jsonString_Cart+"&quote_id="+quoteID+"&empty_cart=1&pincode="+pincode

            }
            print("FETCH CART")
            print("parameter str - \(paramStr)")
            
            Server.postRequestWithURL(urlStr, paramString: paramStr, completionHandler: { (response) in

                print("RESPONSE Fetch Cart -")
                print(response)

                if (response[Constants.STATE] as AnyObject).int32Value  == 1 {
                    
                    if (response["message"] as! String) == "cart is empty"  && (response["quote_id"] == nil || response["quote_id"] as! String == "") {

                        UserDefaults.standard.removeObject(forKey: Constants.QuoteID)
                        UserDefaults.standard.removeObject(forKey: Constants.CART_Array)
                        UserDefaults.standard.removeObject(forKey: Constants.DISCOUNTApplied)

                        let when = DispatchTime.now() + 1 // change 2 to desired number of seconds
                        DispatchQueue.main.asyncAfter(deadline: when) {
                            Constants.appDelegate.stopIndicator()
                            self.cartCollection.reloadData()
                        }
                    }
                    else{
                        
                        if (response["totals"] as? [String:Any]) != nil {

                            let totaldict = response["totals"] as? [String:Any]
                            let itemsArr = totaldict!["items"] as! Array<Any>
                            let quoteID = response["quote_id"] as! String
                            UserDefaults.standard.set(quoteID, forKey: Constants.QuoteID)

                            if itemsArr.count > 0{
                                
                                if self.cartArray.count > 0{
                                    self.cartArray.removeAll()
                                }
                                
                                for index in 0 ... itemsArr.count - 1 {
                                    let dict = itemsArr[index] as! Dictionary<String,Any>
                                    
                                    let quant = String(describing: dict["qty"] as! NSNumber)
                                    let sku = String(describing: dict["sku"] as! String)
                                    let productID = String(describing: dict["product_id"] as! String)
                                    let subtotal = String(describing: dict["subtotal"] as! NSNumber)
                                    let price = String(describing: dict["price"] as! NSNumber)
                                    let name = String(describing: dict["name"] as! String)
                                    let image = String(describing: dict["image"] as! String)
                                    let pincode = String(describing: response["pincode"] as! String)

                                    let newDict = ["p_SKU":sku,
                                                   "p_Quantity":quant,
                                                   "p_ID":productID,
                                                   "P_Price": price,
                                                   "P_Total": subtotal,
                                                   "p_Name" : name,
                                                   "p_Image" : image,
                                                   "pincode" : pincode] as [String : Any]
                                    
                                    self.cartArray.append(newDict)
                                }
                            }
                            else{
                                arrayPar = []
                            }
                            
                            self.subtotalAmount = Int.init( totaldict?["subtotal"] as! NSNumber)
                            self.grandTotal = Int.init( totaldict?["grandtotal"] as! NSNumber)
                            self.discountAmount = Int.init( totaldict?["discount"] as! NSNumber)
                            UserDefaults.standard.set(self.discountAmount, forKey: Constants.DISCOUNTApplied)
                            
                            UserDefaults.standard.set(self.cartArray, forKey: Constants.CART_Array)
                            
                            let quoteID2 = response["quote_id"] as! String
                            UserDefaults.standard.set(quoteID2, forKey: Constants.QuoteID)
                            
//                            DispatchQueue.main.async {
                                
                                let when = DispatchTime.now() + 1 // change 2 to desired number of seconds
                                DispatchQueue.main.asyncAfter(deadline: when) {
                                    // Your code with delay
                                    Constants.appDelegate.stopIndicator()
                                    self.cartCollection.reloadData()
                                }

                                
                            
//                            }
                            
                        }
                        else {
                            Constants.appDelegate.stopIndicator()
                            // obj is not a string array
                            //empty
                        }
                        
                        
                        
                    }
                }
                else {
                    Constants.appDelegate.stopIndicator()
                    
                    if response["message"] as! String == "Please provide valid quote id."
                    {
                        UserDefaults.standard.removeObject(forKey: Constants.QuoteID)
                        UserDefaults.standard.removeObject(forKey: Constants.CART_Array)
                        UserDefaults.standard.removeObject(forKey: Constants.DISCOUNTApplied)
                        self.cartCollection.reloadData()
                    }
                    else{
                        let quoteID = response["quote_id"] as! String
                        UserDefaults.standard.set(quoteID, forKey: Constants.QuoteID)
                        let mess = response["message"] as! String
                        CustomAlertController.showAlert(on: self, with: Simple, andAttributes: ["title":"", "message":mess])

                    }
                    
                }
                
                
            })
            
        }
        else {
            Constants.appDelegate.stopIndicator()
            
            CustomAlertController.showAlert(on: self, with: Simple, andAttributes:["title": "", "message":"Please check your internet connection."])
        }

    }



    // MARK:- COLLECTION VIEW FLOWLAYOUT
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize
    {
        if cartArray.count > 0{
            if indexPath.item == 0{
                return CGSize.init(width: ScreenSize.SCREEN_MAX_LENGTH, height: 100.0)
            }
            else if indexPath.item == 1{
                return CGSize.init(width: ScreenSize.SCREEN_MAX_LENGTH, height: (CGFloat(cartArray.count * 225)))
            }
            else{
                return CGSize.init(width: ScreenSize.SCREEN_MAX_LENGTH, height: 70)
            }
        }
        return CGSize.init(width: ScreenSize.SCREEN_MAX_LENGTH, height: 120.0)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 5
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 5
    }

    //MARK:- Collection View
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        
        if kind == UICollectionElementKindSectionHeader
        {
            let hview = collectionView.dequeueReusableSupplementaryView(ofKind: UICollectionElementKindSectionHeader, withReuseIdentifier: "ShoppingCollectionHeader", for: indexPath)
            let lab = hview.viewWithTag(9911) as! UILabel
            if cartArray.count > 0{
                lab.text = "Shopping Cart"
            }
            else{
                lab.text = "Shopping Cart is Empty"
            }
            
            
            return hview
        }
        else{
            let hview = collectionView.dequeueReusableSupplementaryView(ofKind: UICollectionElementKindSectionFooter, withReuseIdentifier: "FooterCollectionCell", for: indexPath) as! FooterCollectionCell
            return hview
            
        }
    }

    func numberOfSections(in collectionView: UICollectionView) -> Int {
       
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if cartArray.count > 0{
            return 3
        }
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if cartArray.count > 0{
            if indexPath.item == 0{
                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "DiscountCell", for: indexPath) as! DiscountCell
                self.discountText = cell.discountTextfield
                if self.discountAmount < 0  {
                    
                    let couponCode = UserDefaults.standard.string(forKey:  Constants.DISCOUNTCoupon)!
                    
                    cell.applyDiscountButton.setTitle("REMOVE", for: .normal)
//                    if cell.discountTextfield.text!.contains("\" Coupon Code Applied to Cart."){
//                        
//                        var str = cell.discountTextfield.text!
//                        str = str.replacingOccurrences(of: "\"" , with: "")
//                        str = str.replacingOccurrences(of: "\" Coupon Code Applied to Cart." , with: "")
//
//                        cell.discountTextfield.text = str
//                    }
//                    else{
                        cell.discountTextfield.text = "\"\(couponCode)\" Coupon Code Applied to Cart."
//                    }
                    
                    cell.discountTextfield.isUserInteractionEnabled = false
                    cell.discountTextfield.textColor = Colors.greenTextColor
                }
                else{
                    cell.applyDiscountButton.setTitle("APPLY", for: .normal)
                    cell.discountTextfield.text = ""
                    cell.discountTextfield.isUserInteractionEnabled = true
                    cell.discountTextfield.textColor = UIColor.black
                }
                
                cell.applyDiscountButton.addTarget(self, action: #selector(ApplyDiscountPressed), for: .touchUpInside)
                
                return cell
            }
            else if indexPath.item == 1 {
                
                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ShoppingCartCell", for: indexPath) as! ShoppingCartCell
                cell.shoppingTable.delegate = self
                cell.shoppingTable.dataSource = self
                self.shoppingTable = cell.shoppingTable
                self.shoppingTable.reloadData()
                
                var totalAmount = self.CalculateTotals()

                
                if self.subtotalAmount == 0 {
                    cell.subTotalLabel.text = "Rs. \(totalAmount)"
                    cell.appliedDiscountLabel.text = "Rs. \(self.discountAmount)"
                    
                    self.subtotalAmount = totalAmount
                    self.grandTotal = totalAmount

                    if self.discountAmount < 0 {
                        totalAmount = totalAmount + self.discountAmount
                        cell.appliedDiscountLabel.text = "Rs. \(String(describing: self.discountAmount))"
                        self.grandTotal = totalAmount
                    }
                    cell.grandTotalLabel.text = "Rs. \(totalAmount)"
                }
                else{
                    cell.subTotalLabel.text = "Rs. \(self.subtotalAmount)"
                    cell.appliedDiscountLabel.text = "Rs. \(self.discountAmount)"
                    cell.grandTotalLabel.text = "Rs. \(self.grandTotal)"
                }
                

                
                return cell
            }
            else{
                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "UpdateCartCell", for: indexPath) as! UpdateCartCell
                
                cell.proceedCheckoutButton.addTarget(self, action: #selector(proceedToPayment), for: .touchUpInside)
                cell.emptyCartButton.addTarget(self, action: #selector(emptyCartPressed), for: .touchUpInside)
                cell.continueShopButton.addTarget(self, action: #selector(GoToShoppingPage), for: .touchUpInside)

                return cell

            }
        }
        else{
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "EmptyCartCell", for: indexPath)
            let but = cell.viewWithTag(9912) as! UIButton
            but.addTarget(self, action: #selector(GoToShoppingPage), for: UIControlEvents.touchUpInside)
            return cell
        }

    }
    
    
    func collectionView(_ collectionView: UICollectionView, shouldSelectItemAt indexPath: IndexPath) -> Bool {
        return false
    }
    
    //MARK:- TABLEVIEW
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return cartArray.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 210
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "ContentCell", for: indexPath) as! ContentCell
        let dict = cartArray[indexPath.row] as! Dictionary<String,Any>
        
        cell.productName.text = (dict["p_Name"] as! String)
        cell.productDescription.text = "SKU- \(String(describing: dict["p_SKU"] as! String))"
        cell.productPrice.text = "Rs. \(String(describing: dict["P_Price"] as! String))"
        cell.productQuantity.text = (dict["p_Quantity"] as! String)
        cell.deleteProduct.addTarget(self, action: #selector(deleteObjectPressed(sender:)), for: .touchUpInside)
        cell.productQuantity.delegate = self
        

        
        let imgSTR = dict["p_Image"] as! String
        let imageUrl = URL.init(string: imgSTR)
        
        let documentsDirectoryURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
        if !FileManager.default.fileExists(atPath: documentsDirectoryURL.appendingPathComponent((imageUrl!.lastPathComponent)).path) {
            if Reachability.isConnectedToNetwork() == true {
                cell.productImage.sd_setImage(with: URL.init(string: imgSTR))
                Helper.downloadImageLinkAndCreateAsset(imgSTR)
            }
            else{
                cell.productImage.image = UIImage.init(named: "noImage.png")
            }
        }
        else{
            let abc = documentsDirectoryURL.appendingPathComponent(imageUrl!.lastPathComponent).path
            let imageURL = URL(fileURLWithPath: abc)
            cell.productImage.image = UIImage(contentsOfFile: imageURL.path)
        }
        
        return cell
    }
    
    
    //MARK:- Calculate Totals
    func CalculateTotals()-> Int {
        var fnalTotal : Int = 0
        
        for index in 0 ... self.cartArray.count - 1 {
            var dict = cartArray[index] as! Dictionary<String,Any>
            let quantStr = dict["p_Quantity"] as! String
            let quant = Int.init(quantStr)
            let priceStr = dict["P_Price"] as! String
            let price = Int.init(priceStr)
            let total = price! * quant!
            fnalTotal = fnalTotal + total
        }
        
        return fnalTotal
    }

    //MARK:- Continue Shopping

    func GoToShoppingPage() {
        self.navigationController?.popToRootViewController(animated: false)
    }
    
    //MARK:- Empty Cart
    func emptyCartPressed(){

//        self.forceRemoveDiscountAPI()
        UserDefaults.standard.removeObject(forKey: Constants.CART_Array)
        UserDefaults.standard.removeObject(forKey: Constants.DISCOUNTApplied)
        UserDefaults.standard.set("", forKey: Constants.DISCOUNTCoupon)

        if UserDefaults.standard.array(forKey: Constants.CART_Array) == nil{
            if cartArray.count > 0{
                cartArray.removeAll()
            }
        }
        else{
            cartArray = UserDefaults.standard.array(forKey: Constants.CART_Array)!
        }
        
        self.fetchCartApi(isEmpty: true)
        
    }
    
    //MARK:- Delete SPecific Object
    func deleteObjectPressed(sender : AnyObject){
        
        let but = sender as! UIButton
        let cellcontent = but.superview!
        let cell = cellcontent.superview as! ContentCell
        let indexpath = self.shoppingTable.indexPath(for: cell)
        let row = indexpath?.row

        cartArray.remove(at: row!)
        
        if cartArray.count > 0{
            UserDefaults.standard.set(cartArray, forKey: Constants.CART_Array)
            self.fetchCartApi(isEmpty: false)
        }
        else{
            UserDefaults.standard.removeObject(forKey: Constants.CART_Array)
            UserDefaults.standard.removeObject(forKey: Constants.DISCOUNTApplied)
//            self.forceRemoveDiscountAPI()
            self.fetchCartApi(isEmpty: true)
        }
        
    }
    
    
    //MARK:- Update Cart
    func updateCartPressed(){
        
        for index in 0 ... cartArray.count - 1 {
            let cell = self.shoppingTable.cellForRow(at: IndexPath.init(row: index, section: 0)) as! ContentCell
            let quant = cell.productQuantity.text!
            
            var dict = cartArray[index] as! Dictionary<String,Any>
            dict.updateValue(quant, forKey: "p_Quantity")
            
            if cartArray.count > 0{
                cartArray.remove(at: index)
                cartArray.insert(dict, at: index)
                UserDefaults.standard.set(cartArray, forKey: Constants.CART_Array)
            }
            else{
                UserDefaults.standard.removeObject(forKey: Constants.CART_Array)
                UserDefaults.standard.removeObject(forKey: Constants.DISCOUNTApplied)
            }
        }
        
        if cartArray.count > 0{
            self.fetchCartApi(isEmpty: false)
        }
        else{
            self.fetchCartApi(isEmpty: true)
        }

    }

    //MARK:- Proceed for Payment
    func proceedToPayment(){
        
        let destViewController = Constants.mainStoryboard.instantiateViewController(withIdentifier: "CheckoutMainVC") as! CheckoutMainVC
        destViewController.orderArray = cartArray
        self.navigationController?.pushViewController(destViewController, animated: true)
    }
    
    //MARK:- Discount Api
    func ApplyDiscountPressed(sender : Any){

        let butto = sender as! UIButton
        
        var quoteID = ""
        if UserDefaults.standard.object(forKey: Constants.QuoteID) == nil{
            quoteID = ""
        }
        else{
            quoteID = "\(UserDefaults.standard.object(forKey: Constants.QuoteID)!)"
        }
        
        if self.discountText.text == ""{
        }
        else{
            
            if Reachability.isConnectedToNetwork() {
                Constants.appDelegate.startIndicator()

                self.discountText.resignFirstResponder()
                
                let urlStr = Constants.BASEURL + "couponPost"
                                

                var paramStr = "coupon_code=\(self.discountText.text!)&quote_id=\(quoteID)"
                
                if butto.currentTitle == "REMOVE"{
                   paramStr = "coupon_code=\(self.discountText.text!)&quote_id=\(quoteID)&remove=1"
                }
                
                var paramDict = ["coupon_code" : self.discountText.text!,
                                 "quote_id" : quoteID,
                                 "ecp_codes" : "\([])",
                                 "ecp_prices" : "\([])",
                                 "ecpCouponcode" : ""]
                if butto.currentTitle == "REMOVE"{
                    paramDict = ["coupon_code" : self.discountText.text!,
                                 "quote_id" : quoteID,
                                 "ecp_codes" : "[]",
                                 "ecp_prices" : "[]",
                                 "ecpCouponcode" : "",
                                 "remove" : "1"]
                }

                
                Server.postRequestWithURL(urlStr, paramString: paramStr, completionHandler: { (response) in

                    print(response)
                    if (response[Constants.STATE] as AnyObject).int32Value  == 1 {
                        
                        let disc = response["totals"] as! Dictionary<String,Any>
                        
                        self.subtotalAmount = Int.init( disc["subtotal"] as! NSNumber)
                        self.grandTotal = Int.init( disc["grandtotal"] as! NSNumber)
                        self.discountAmount = Int.init( disc["discount"] as! NSNumber)

                        UserDefaults.standard.set(self.discountAmount, forKey: Constants.DISCOUNTApplied)

                        if self.discountAmount < 0{
                            UserDefaults.standard.set(self.discountText.text!, forKey: Constants.DISCOUNTCoupon)
                        }
                        else{
                            UserDefaults.standard.set("", forKey: Constants.DISCOUNTCoupon)
                        }
                        
                        let quoteID = response["quote_id"] as! String

                        UserDefaults.standard.set(quoteID, forKey: Constants.QuoteID)

                        let when = DispatchTime.now() + 1 // change 2 to desired number of seconds
                        DispatchQueue.main.asyncAfter(deadline: when) {
                            Constants.appDelegate.stopIndicator()
                            self.cartCollection.reloadData()
                        }
                    }
                    else {
                        Constants.appDelegate.stopIndicator()

                        let quoteID = response["quote_id"] as! String
                        UserDefaults.standard.set(quoteID, forKey: Constants.QuoteID)

                        let mess = response["message"] as! String
                        CustomAlertController.showAlert(on: self, with: Simple, andAttributes: ["title":"", "message":mess])
                    }
                })
//                Server.PostDataInDictionary(urlString: urlStr, dict_data: paramDict, completionHandler: { (response) in
//                    print(response)
//                    Constants.appDelegate.stopIndicator()
//                })
                
            }
            else {
                Constants.appDelegate.stopIndicator()
                
                CustomAlertController.showAlert(on: self, with: Simple, andAttributes:["title": "", "message":"Please check your internet connection."])
            }
        }
    }

    
    
    func forceRemoveDiscountAPI(){
        
        if Reachability.isConnectedToNetwork() {
            Constants.appDelegate.startIndicator()
            
            self.discountText.resignFirstResponder()
            
            let urlStr = Constants.BASEURL + "couponPost"
            
            var quoteID = ""
            if UserDefaults.standard.object(forKey: Constants.QuoteID) == nil{
                quoteID = ""
            }
            else{
                quoteID = "\(UserDefaults.standard.object(forKey: Constants.QuoteID)!)"
            }

            let paramStr = "coupon_code=\(self.discountText.text!)&quote_id=\(quoteID)&ecp_codes=\([])&ecp_prices=\([])&ecpCouponcode=&remove=1"
            
            print(" Force Empty")
            print("parameter str - \(paramStr)")
            

            Server.postRequestWithURL(urlStr, paramString: paramStr, completionHandler: { (response) in
                print("RESPONSE Force Empty -")
                print(response)

                if (response[Constants.STATE] as AnyObject).int32Value  == 1 {
                    
                    let disc = response["totals"] as! Dictionary<String,Any>
                    
                    self.subtotalAmount = Int.init( disc["subtotal"] as! NSNumber)
                    self.grandTotal = Int.init( disc["grandtotal"] as! NSNumber)
                    self.discountAmount = Int.init( disc["discount"] as! NSNumber)
                    
                    UserDefaults.standard.set(self.discountAmount, forKey: Constants.DISCOUNTApplied)
                    
                    if self.discountAmount < 0{
                        UserDefaults.standard.set(self.discountText.text!, forKey: Constants.DISCOUNTCoupon)
                    }
                    else{
                        UserDefaults.standard.set("", forKey: Constants.DISCOUNTCoupon)
                    }
                    
                    let quoteID = response["quote_id"] as! String
                    
                    UserDefaults.standard.set(quoteID, forKey: Constants.QuoteID)
                    
                    let when = DispatchTime.now() + 1 // change 2 to desired number of seconds
                    DispatchQueue.main.asyncAfter(deadline: when) {
                        Constants.appDelegate.stopIndicator()
                        self.cartCollection.reloadData()
                    }
                }
                else {
                    Constants.appDelegate.stopIndicator()
                    
                    let quoteID = response["quote_id"] as! String
                    UserDefaults.standard.set(quoteID, forKey: Constants.QuoteID)
                    
                    let mess = response["message"] as! String
                    CustomAlertController.showAlert(on: self, with: Simple, andAttributes: ["title":"", "message":mess])
                }
            })
        }
        else {
            Constants.appDelegate.stopIndicator()
            
            CustomAlertController.showAlert(on: self, with: Simple, andAttributes:["title": "", "message":"Please check your internet connection."])
        }
    }
    
    //MARK:- Textfield Delegate
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let charSet : CharacterSet = CharacterSet.init(charactersIn: "54321")
        let charSet2 : CharacterSet = CharacterSet.init(charactersIn: "09876")

        let newRa = string.rangeOfCharacter(from: charSet)
        let newRa2 = string.rangeOfCharacter(from: charSet2)

        
        if newRa == nil && newRa2 != nil && range.location == 0 {
            print("NUmer out of range")
             return false
        }
        else if (newRa == nil || range.location > 0) && string != "" {
            print("Greater Range")

            return false
        }
        else if newRa != nil && range.location == 0 && string != "" {
            textField.text = string
            textField.resignFirstResponder()
            self.updateCartPressed()
        }
        
        return true
    }
    
    
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField == self.discountText{
            
        }
        else{
            for cell in self.shoppingTable.visibleCells{
                let cells = cell as! ContentCell
                let str = cells.productQuantity.text
                if str == ""{
                    let indexpath = self.shoppingTable.indexPath(for: cells)
                    let index = indexpath?.row
                    
                    let dict = cartArray[index!] as! Dictionary<String,Any>
                    
                    cells.productQuantity.text = dict["p_Quantity"] as? String
                }
            }
        }
    }
    
}
