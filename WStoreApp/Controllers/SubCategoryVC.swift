//
//  SubCategoryVC.swift
//  WStoreApp
//
//  Created by Surbhi on 13/06/17.
//  Copyright © 2017 SurbhiV. All rights reserved.
//

import UIKit
import Firebase

class SubCategoryVC: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout  {

    @IBOutlet weak var catCollection: UICollectionView!
    @IBOutlet weak var cartButton: UIButton!
    @IBOutlet weak var cartCountLable: UILabel!

    var isDirectView = Bool()

    var subCatString = String()
    var subCatArray = Array<Any>()
    var catArray = Array<Any>()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        if self.subCatString == "kitchen" {
            catArray = ["LAUNDRY" as AnyObject,"LIVING-SPACES" as AnyObject]
        }
        else if self.subCatString == "laundry" {
            catArray = ["KITCHEN" as AnyObject,"LIVING-SPACES" as AnyObject]
        }
        else { // living-spaces
            catArray = ["KITCHEN" as AnyObject,"LAUNDRY" as AnyObject]
        }

        let arr = UserDefaults.standard.value(forKey: subCatString.uppercased()) as! Array<Any>
        if arr.count > 0 {
            self.subCatArray = UserDefaults.standard.value(forKey: subCatString.uppercased()) as! Array<Any>
            Constants.appDelegate.stopIndicator()
            DispatchQueue.main.async {
                self.catCollection.reloadData()
            }
        }
        else{
            self.getSubCategoriesforType(subCatString)
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        sideMenuController()?.sideMenu?.allowPanGesture = false
        sideMenuController()?.sideMenu?.allowLeftSwipe = false
        sideMenuController()?.sideMenu?.allowRightSwipe = false

        if subCatArray.count > 0 {
            DispatchQueue.main.async {
                self.catCollection.reloadData()

            }
        }
        
        cartCountLable.layer.cornerRadius = 7.5
        cartCountLable.clipsToBounds = true
        
        if UserDefaults.standard.bool(forKey: Constants.isTransactionEnabled) == false{
            self.cartButton.isHidden = true
            if UserDefaults.standard.object(forKey: Constants.CART_Array) == nil{
                cartCountLable.isHidden = true
            }
            else{
                let arr = UserDefaults.standard.object(forKey: Constants.CART_Array) as! Array<Any>
                let count = arr.count
                cartCountLable.isHidden = false
                cartCountLable.text = "\(count)"
            }
        }
        else{
            self.cartButton.isHidden = false
            if UserDefaults.standard.object(forKey: Constants.CART_Array) == nil{
                cartCountLable.isHidden = true
            }
            else{
                let arr = UserDefaults.standard.object(forKey: Constants.CART_Array) as! Array<Any>
                let count = arr.count
                cartCountLable.isHidden = false
                cartCountLable.text = "\(count)"
            }
        }

    }
    
    override var prefersStatusBarHidden : Bool {
        return true
    }
    // MARK:- FETCH API
    func getSubCategoriesforType(_ typeStr : String){
        
        if Reachability.isConnectedToNetwork() {
            Constants.appDelegate.startIndicator()
            
            let urlStr = Constants.BASEURL + "categories"
            let param = "type=\(typeStr)"
            
            Server.postRequestWithURL(urlStr, paramString: param, completionHandler: { (response) in
                
                Constants.appDelegate.stopIndicator()
                if response[Constants.STATE]?.int32Value  == 1 {
                    
                    self.subCatArray = response["data"] as! Array<AnyObject>
                    
                    UserDefaults.standard.set(self.subCatArray, forKey: self.subCatString.uppercased())
                    
                    DispatchQueue.main.async {
                        self.catCollection.reloadData()
                    }
                }
                else {
                    
                    let dict = UserDefaults.standard.value(forKey: self.subCatString.uppercased()) as! Array<AnyObject>
                    if dict.count > 0 {
                        self.subCatArray = UserDefaults.standard.value(forKey: self.subCatString.uppercased()) as! Array<AnyObject>
                        Constants.appDelegate.stopIndicator()
                        DispatchQueue.main.async {
                            self.catCollection.reloadData()
                        }
                    }
                    else{
                        Constants.appDelegate.stopIndicator()
                        var mess = String()
                        if response.count == 0{
                            mess = "Some Error Occurred"
                        }
                        else{
                            mess = response["message"] as! String
                        }
                        CustomAlertController.showAlert(on: self, with: Simple, andAttributes: ["title":"", "message":mess])
                    }
                }
            })
            
        }
        else {
            
            self.subCatArray = UserDefaults.standard.array(forKey: self.subCatString.uppercased())! as Array<AnyObject>
            if self.subCatArray.count > 0
            {
                DispatchQueue.main.async {
                    self.catCollection.reloadData()
                }
            }
            else{
                CustomAlertController.showAlert(on: self, with: Simple, andAttributes:["title": "", "message":"Please check your internet connection."])
            }
        }
    }

    
    //MARK:- BUTTON ACTIONS
    
    @IBAction func BackButtonPressed(_ sender: AnyObject) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func SearchButtonPressed(_ sender: AnyObject) {
        
    }

    // MARK:- COLLECTION VIEW FLOWLAYOUT
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize
    {
        if DeviceType.IS_IPAD{
            if indexPath.section == 0 {
                    return CGSize(width: (ScreenSize.SCREEN_MIN_LENGTH - 80)/3, height: (ScreenSize.SCREEN_MIN_LENGTH - 80)/3)
            }
            else{
                    return CGSize(width: (ScreenSize.SCREEN_MAX_LENGTH - 70)/2, height: 250)
            }
            
        }
        return CGSize(width: (collectionView.bounds.size.width - 25)/2, height: (collectionView.bounds.size.width - 25)/2)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        if DeviceType.IS_IPAD{
            return 10
        }
        return 5
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        if DeviceType.IS_IPAD{
            return 10
        }
        return 5
        
    }
    
    // MARK:- COLLECTION VIEW
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        if section == 0 {
            return CGSize.init(width: 1014, height: 130)
        }
        return CGSize.init(width: 1014, height: 0)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForFooterInSection section: Int) -> CGSize {
        if section == 1 {
            return CGSize.init(width: 1014, height: 120)
        }
        return CGSize.init(width: 1014, height: 0)
    }
    
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        
        if kind == UICollectionElementKindSectionHeader
        {
            
            let hview = collectionView.dequeueReusableSupplementaryView(ofKind: UICollectionElementKindSectionHeader, withReuseIdentifier: "CollectionHeader", for: indexPath) as! CollectionHeader
            
            if self.subCatString == "kitchen" {
                hview.headingLabel.text = "KITCHEN"
            }
            else if self.subCatString == "laundry" {
                hview.headingLabel.text = "LAUNDRY"
            }
            else {
                hview.headingLabel.text = "LIVING-SPACES"
            }
            
            return hview
        }
        else{
            let hview = collectionView.dequeueReusableSupplementaryView(ofKind: UICollectionElementKindSectionFooter, withReuseIdentifier: "FooterCollectionCell", for: indexPath) as! FooterCollectionCell
            return hview

        }
    }
    

    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 2
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if section == 0 {
            return subCatArray.count
        }
        else{
            return catArray.count
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if indexPath.section == 0 {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "SubCategoryCell", for: indexPath)
            let dict = self.subCatArray[indexPath.item] as! Dictionary<String,AnyObject>
            
            let img = cell.viewWithTag(201) as! UIImageView
            let lab = cell.viewWithTag(202) as! UILabel
            let bg = cell.viewWithTag(205) as! UIImageView
            bg.backgroundColor = UIColor.white
            
            let imgSTR = dict["image"] as! String
            let imageUrl = URL.init(string: imgSTR)

            let documentsDirectoryURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
            if !FileManager.default.fileExists(atPath: documentsDirectoryURL.appendingPathComponent((imageUrl!.lastPathComponent)).path) {
//                Helper.downloadImageLinkAndCreateAsset(imgSTR)
                if Reachability.isConnectedToNetwork() == true {
                    Helper.downloadImageLinkAndCreateAsset(imgSTR)
                    img.sd_setImage(with: URL.init(string: imgSTR))
                }
                else{
                    img.image = UIImage.init(named: "noImage.png")
                }

            }
            else{
                let abc = documentsDirectoryURL.appendingPathComponent(imageUrl!.lastPathComponent).path
                let imageURL = URL(fileURLWithPath: abc)
                img.image = UIImage(contentsOfFile: imageURL.path)
            }
            
//            img.sd_setImage(with: URL.init(string: imgSTR))
            
            lab.text = dict["name"] as? String
            return cell

        }
        else {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "OtherOptionsCell", for: indexPath)
            let img = cell.viewWithTag(203) as! UIImageView
            if catArray[indexPath.row] as! String == "LAUNDRY" {
                img.image = UIImage.init(named: "laundryBG_Rect")
            }
            else if catArray[indexPath.row] as! String == "LIVING-SPACES" {
                img.image = UIImage.init(named: "livingBG_RECT")
            }
            else{
                img.image = UIImage.init(named: "kitcheBG_RECT")
            }
            let lab = cell.viewWithTag(204) as! UILabel
            lab.text = catArray[indexPath.row] as? String
            
            if DeviceType.IS_IPAD{
//                if UIDevice.current.orientation.isPortrait {
//                    lab.font = UIFont.init(name: FONTS.Helvetica_Bold, size: 35)
//                }
//                else{
                    lab.font = UIFont.init(name: FONTS.Helvetica_Bold, size: 30)
//                }
                
            }
            return cell
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if indexPath.section == 0 {

            let dict = self.subCatArray[indexPath.item] as! Dictionary<String,AnyObject>
            let isNextCategory = dict["category"] as! NSString

            if isNextCategory == "1" {
                let destViewController = Constants.mainStoryboard.instantiateViewController(withIdentifier: "SubSubCategoryVC") as! SubSubCategoryVC
                
                destViewController.topCatType = self.subCatString
                destViewController.topSubCatType = dict["name"] as! String
                let categID = dict["id"] as! Int
                
                destViewController.catID = "\(categID)"
                destViewController.isDirectView = false
                self.navigationController?.pushViewController(destViewController, animated: true)
            }
            else{
                let destViewController = Constants.mainStoryboard.instantiateViewController(withIdentifier: "ProductListVC") as! ProductListVC
                destViewController.ProductType = dict["name"] as! String
                destViewController.catID = "\(dict["id"]!)"
                destViewController.isDirectView = false
                self.navigationController?.pushViewController(destViewController, animated: true)
            }
        }
        else {
            Constants.appDelegate.startIndicator()
            let str = catArray[indexPath.row] as? String
            self.subCatString = str!.lowercased()
            self.viewDidLoad()
        }
    }
    
    
    //MARK:- Orientation
    
    override var supportedInterfaceOrientations : UIInterfaceOrientationMask {
        return UIInterfaceOrientationMask.all
    }
    
    override func didRotate(from fromInterfaceOrientation: UIInterfaceOrientation) {
        self.catCollection.reloadData()
    }
    

}
