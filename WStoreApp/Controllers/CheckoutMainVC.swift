//
//  CheckoutMainVC.swift
//  WStoreApp
//
//  Created by Surbhi on 29/08/17.
//  Copyright © 2017 SurbhiV. All rights reserved.
//

import UIKit

class CheckoutMainVC: UIViewController, UIScrollViewDelegate {

    var headerArray = ["Address","Payment","Order Review"]
    var indicatorLabel = UILabel()
    var ControllerArray = Array<Any>()
    var addressView = BillingInformationVC()
    var orderView = OrderVC()
    var paymentView = PaymentOptionVC()
    var orderArray = Array<Any>()
    

    @IBOutlet weak var detailScrollContentwidth: NSLayoutConstraint!
    @IBOutlet weak var outerScrollConstantHeight: NSLayoutConstraint!

    @IBOutlet weak var mainScroll: UIScrollView!
    @IBOutlet weak var mainScrollContent: UIView!
    @IBOutlet weak var headerScroll: UIScrollView!
    @IBOutlet weak var detailScroll: UIScrollView!
    @IBOutlet weak var contentView: UIView!

    override func viewDidLoad() {
        super.viewDidLoad()

        self.setHeaderScroll()
        self.setUpScrollView()
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override var prefersStatusBarHidden : Bool {
        return true
    }
    
    @IBAction func BackButtonPressed(_ sender: AnyObject) {
        self.navigationController?.popViewController(animated: true)
    }
        
    
    //MARK:- Scroll View SetUp
    func setHeaderScroll()
    {
        
        var scrollContentCount = 0;
        var frame : CGRect = CGRect.zero
        var labelx : CGFloat = 0.0
        let count = CGFloat(headerArray.count)
        
        let width  : CGFloat = ScreenSize.SCREEN_WIDTH / count
        let firstLabelWidth : CGFloat = ScreenSize.SCREEN_WIDTH / count
        
        indicatorLabel = UILabel.init(frame: CGRect.init(x: labelx, y: 45, width: firstLabelWidth, height: 4))
        indicatorLabel.backgroundColor = Colors.appThemeColorText
        indicatorLabel.text = "";
        headerScroll.addSubview(indicatorLabel)
        
        for arrayIndex in 0 ..< headerArray.count
        {
            frame.origin.x = labelx
            // set scorllview properties
            frame.origin.y = 0;
            frame.size.width=width;
            frame.size.height=45;
            
            let accountLabelButton = UIButton.init(type: UIButtonType.custom)
            accountLabelButton.backgroundColor = Colors.Lighttextcolor
//            accountLabelButton.addTarget(self, action:#selector(HeaderlabelSelected), for: UIControlEvents.touchUpInside)
            
            accountLabelButton.frame = frame;
            accountLabelButton.tag = arrayIndex;
            
            accountLabelButton.titleLabel?.numberOfLines = 0
            let heading = headerArray[arrayIndex]
            
            headerScroll.addSubview(accountLabelButton)
            
            if arrayIndex == 0
            {
                accountLabelButton.setAttributedTitle(NSAttributedString(string: heading.uppercased(),  attributes: [NSForegroundColorAttributeName : Colors.appThemeColorText,NSFontAttributeName : UIFont.init(name: "Helvetica-Bold", size: 20.0)!]), for: UIControlState.normal)
                
                accountLabelButton.alpha=1.0;
            }
            else
            {
                accountLabelButton.setAttributedTitle(NSAttributedString(string: heading.uppercased(),  attributes: [NSForegroundColorAttributeName : UIColor.black,NSFontAttributeName : UIFont.init(name: "Helvetica-Bold", size: 20.0)!]), for: UIControlState.normal)
                
                accountLabelButton.alpha=0.6;
            }
            
            accountLabelButton.titleLabel?.textAlignment = NSTextAlignment.center
            
            
            labelx=labelx+width;
            scrollContentCount = scrollContentCount + 1;
        }
        
        headerScroll.contentSize = CGSize.init(width: frame.width/count, height: 50)
        automaticallyAdjustsScrollViewInsets = false;
    }
    
    func HeaderlabelSelected(senderTag: Int)
    {
        let count = CGFloat(headerArray.count)
        let buttonArray = NSMutableArray()
        let xAxis : CGFloat = ScreenSize.SCREEN_WIDTH * CGFloat(senderTag)
        
        for view in headerScroll.subviews
        {
            if view.isKind(of: UIButton.classForCoder())
            {
                let labelButton = view as! UIButton
                buttonArray.add(labelButton)
                let heading = headerArray[labelButton.tag]
                
                if labelButton.tag == senderTag {
                    labelButton.setAttributedTitle(NSAttributedString(string: heading.uppercased(),  attributes: [NSForegroundColorAttributeName : Colors.appThemeColorText,NSFontAttributeName : UIFont.init(name: "Helvetica-Bold", size: 20.0)!]), for: UIControlState.normal)
                    
                    labelButton.alpha = 1.0
                }
                else
                {
                    labelButton.setAttributedTitle(NSAttributedString(string: heading.uppercased(),  attributes: [NSForegroundColorAttributeName : UIColor.black,NSFontAttributeName : UIFont.init(name: "Helvetica-Bold", size: 20.0)!]), for: UIControlState.normal)
                    
                    labelButton.alpha = 0.6
                }
            }
        }
        
        let labelButton = buttonArray[senderTag]
        var frame : CGRect = (labelButton as AnyObject).frame
        frame.origin.y = 46
        frame.size.height = 4
        
        UIView.animate(withDuration: 0.2, animations: {
            self.detailScroll.setContentOffset(CGPoint.init(x: xAxis, y: 0), animated: true)
            
            if senderTag == 0
            {
                self.indicatorLabel.frame =  CGRect.init(x: 0, y: 45 , width: ScreenSize.SCREEN_WIDTH/count, height: 4)
            }
            else
            {
                self.indicatorLabel.frame =  CGRect.init(x: (ScreenSize.SCREEN_WIDTH/count) * CGFloat(senderTag), y: 45, width: ScreenSize.SCREEN_WIDTH/count, height: 4)
            }
            self.headerScroll.scrollRectToVisible(self.indicatorLabel.frame, animated: false)
            
            
            
        }, completion: nil)
    }
    
    // MARK: -  Main Scrollview
    func setUpScrollView() {//
        
        for arrayIndex in headerArray {
            
            if (arrayIndex == "Address") {
                addressView = Constants.mainStoryboard.instantiateViewController(withIdentifier: "BillingInformationVC") as! BillingInformationVC
                ControllerArray.insert(addressView, at: 0)
            }
            
            if (arrayIndex == "Payment")  {
                paymentView = Constants.mainStoryboard.instantiateViewController(withIdentifier: "PaymentOptionVC") as! PaymentOptionVC
                ControllerArray.insert(paymentView, at: 1)
            }
            
            if (arrayIndex == "Order Review")  {
                orderView = Constants.mainStoryboard.instantiateViewController(withIdentifier: "OrderVC") as! OrderVC
                orderView.orderArr = orderArray
                ControllerArray.insert(orderView, at: 2)
            }
        }
        setViewControllers(viewControllers: ControllerArray as NSArray, animated: false)
    }
    
    //  Add and remove view controllers
    
    func setViewControllers(viewControllers : NSArray, animated : Bool) {
        if self.childViewControllers.count > 0 {
            for vC in self.childViewControllers {
                vC.willMove(toParentViewController: nil)
                vC.removeFromParentViewController()
            }
        }
        
        for vC in viewControllers {
            
            self.addChildViewController(vC as! UIViewController)
            (vC as! UIViewController).didMove(toParentViewController: self)
        }
        
        //TODO animations
        if ((detailScroll) != nil) {
            reloadPages()
        }
    }
    
    func reloadPages() {
        var scrollContentCount = 0;
        
        for arrayIndex in 0 ..< headerArray.count {
            // set scorllview properties
            var frame : CGRect = CGRect.init(x: 0, y: 0, width: 0, height: 549)
            frame.origin.x = ScreenSize.SCREEN_WIDTH * CGFloat(arrayIndex)
            frame.origin.y = 0;
            frame.size = CGSize.init(width: 1024, height: 549)//ScreenSize.SCREEN.size
            
            detailScroll.isPagingEnabled = true
            let contentWidth : CGFloat = ScreenSize.SCREEN_WIDTH * CGFloat(scrollContentCount+1)
            detailScrollContentwidth.constant = contentWidth;
            
            let vC = self.childViewControllers[arrayIndex]
            self.addView(view: vC.view, contentView: contentView, frame: frame)
            
            scrollContentCount = scrollContentCount + 1;
        }
        
        detailScroll.contentSize = CGSize.init(width: detailScrollContentwidth.constant, height: detailScroll.frame.size.height)
        detailScroll.delegate = self
    }
    
    // adding views to scrollview
    func addView(view : UIView, contentView : UIView, frame : CGRect)  {
        view.frame = frame
        view.translatesAutoresizingMaskIntoConstraints = false
        contentView.addSubview(view)
        
        contentView.addConstraint(NSLayoutConstraint.init(item: view, attribute: NSLayoutAttribute.leading, relatedBy: NSLayoutRelation.equal, toItem: contentView, attribute:  NSLayoutAttribute.leading, multiplier: 1.0, constant: frame.origin.x))
        contentView.addConstraint(NSLayoutConstraint.init(item: view, attribute: NSLayoutAttribute.width, relatedBy: NSLayoutRelation.equal, toItem: nil, attribute:  NSLayoutAttribute.notAnAttribute, multiplier: 1.0, constant: frame.size.width))
        contentView.addConstraint(NSLayoutConstraint.init(item: view, attribute: NSLayoutAttribute.top, relatedBy: NSLayoutRelation.equal, toItem: contentView, attribute:  NSLayoutAttribute.top, multiplier: 1.0, constant:0))
        
        contentView.addConstraint(NSLayoutConstraint.init(item: view, attribute: NSLayoutAttribute.height, relatedBy: NSLayoutRelation.equal, toItem: nil, attribute:  NSLayoutAttribute.notAnAttribute, multiplier: 1.0, constant: frame.size.height))
    }



}
