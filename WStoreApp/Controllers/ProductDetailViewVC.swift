//
//  ProductDetailViewVC.swift
//  WStoreApp
//
//  Created by Surbhi on 22/06/17.
//  Copyright © 2017 SurbhiV. All rights reserved.
//

import UIKit
import Firebase

class ProductDetailViewVC: UIViewController, UIWebViewDelegate, UICollectionViewDelegate, UICollectionViewDataSource, UITableViewDelegate, UITableViewDataSource, UITextFieldDelegate {

    @IBOutlet weak var mainScroll: UIScrollView!
    @IBOutlet weak var mainScrollContentView: UIView!
    @IBOutlet weak var mainScrollContentHeight: NSLayoutConstraint!

    @IBOutlet weak var productImageCollection: UICollectionView!
    @IBOutlet weak var productNameLabel: UILabel!
    @IBOutlet weak var colorAvalableLabel: UILabel!

    @IBOutlet weak var productDescriptionTable: UITableView!
    @IBOutlet weak var productDescriptionLabel: UITextView!

    
    @IBOutlet weak var productMRPLabel: UILabel!
    @IBOutlet weak var productSellingPriceLabel: UILabel!

    @IBOutlet weak var headerScroll: UIScrollView!
    @IBOutlet weak var detailScroll: UIScrollView!
    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var detailScrollContentwidth: NSLayoutConstraint!

    @IBOutlet weak var colorCollection: UICollectionView!

    @IBOutlet weak var prev: UIButton!
    @IBOutlet weak var next_Button: UIButton!
    @IBOutlet weak var pinText: UITextField!
    @IBOutlet weak var productAvailabilityLabel: UILabel!
    @IBOutlet weak var buyNowButton: UIButton!
    @IBOutlet weak var pinView: UIView!
    @IBOutlet weak var cartButton: UIButton!
    @IBOutlet weak var cartCountLable: UILabel!

    @IBOutlet weak var popOverView: UIView!
    @IBOutlet weak var popOverCollection: UICollectionView!
    @IBOutlet weak var popOverImagePageControl: UIPageControl!
    @IBOutlet weak var prev_popOver: UIButton!
    @IBOutlet weak var next_popOver: UIButton!

    
    
    var headerArray = ["Features","Specifications"]
    var indicatorLabel = UILabel()
    var ControllerArray = Array<Any>()
    var colorDict = Dictionary<String,Any>()
    var colorArray = Array<Any>()
    var selectedColor = String()
    var descriptionArray = Array<Any>()
    var featureArray = Array<Any>()
    var specArray = Array<Any>()

    var selectedImageArray = Array<Any>()
    var associatedImagesDict = Dictionary<String,Any>()
    var associatedImagesArray = Array<Any>()
    var expandedSections = NSMutableIndexSet()
    var SelectedColorDict = Dictionary<String,Any>()
    var colorProductsDict = Dictionary<String,Any>()

    var featureView = featureVC()
    var specialView = SpecialisationVC()
    var selectedColorKey = String()
    var canBuy = false
    var pinSelected = String()
    var textFieldPinSTR = String()
    
    @IBOutlet weak var webVC: UIWebView!

    var productID = String()
    var responseData = Dictionary<String,Any>()
    
    //MARK:- Start
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        Constants.appDelegate.startIndicator()
        prev.addTarget(self, action: #selector(PreviousImagePresed), for: .touchUpInside)
        next_Button.addTarget(self, action: #selector(NextImagePresed), for: .touchUpInside)
        prev_popOver.addTarget(self, action: #selector(PreviousImagePresed), for: .touchUpInside)
        next_popOver.addTarget(self, action: #selector(NextImagePresed), for: .touchUpInside)

        let prodID = "PROD\(self.productID)"
        let dict = UserDefaults.standard.value(forKey: prodID) as! Dictionary<String,Any>
        if dict.count > 0 {
            self.responseData = UserDefaults.standard.value(forKey: prodID) as! Dictionary<String,Any>
            self.setDataForView(responseData: self.responseData)
        }
        else{
            self.getProductDetailsforType()
        }
        
        expandedSections = NSMutableIndexSet.init()
        
        self.pinView.layer.borderWidth = 1.0
        self.pinView.layer.borderColor = Colors.detailYellowTextColor.cgColor
        self.buyNowButton.isUserInteractionEnabled = false

        // Do any additional setup after loading the view.
    }

    override func viewWillAppear(_ animated: Bool) {
        sideMenuController()?.sideMenu?.allowPanGesture = false
        sideMenuController()?.sideMenu?.allowLeftSwipe = false
        sideMenuController()?.sideMenu?.allowRightSwipe = false

        cartCountLable.layer.cornerRadius = 7.5
        cartCountLable.clipsToBounds = true
        
        
        if UserDefaults.standard.bool(forKey: Constants.isTransactionEnabled) == false{
            self.cartButton.isHidden = true
            self.pinView.isHidden = true

            if UserDefaults.standard.object(forKey: Constants.CART_Array) == nil{
                cartCountLable.isHidden = true
            }
            else{
                let arr = UserDefaults.standard.object(forKey: Constants.CART_Array) as! Array<Any>
                let count = arr.count
                cartCountLable.isHidden = false
                cartCountLable.text = "\(count)"
            }
        }
        else{
            self.cartButton.isHidden = false
            self.pinView.isHidden = false

            if UserDefaults.standard.object(forKey: Constants.CART_Array) == nil{
                cartCountLable.isHidden = true
            }
            else{
                let arr = UserDefaults.standard.object(forKey: Constants.CART_Array) as! Array<Any>
                let count = arr.count
                cartCountLable.isHidden = false
                cartCountLable.text = "\(count)"
            }
        }
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override var prefersStatusBarHidden : Bool {
        return true
    }
    
    @IBAction func BackButtonPressed(_ sender: AnyObject) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
   
    //MARK:- Buy Now
    func checkPinAvailabilityAPI(){
        if Reachability.isConnectedToNetwork() {
            
            
            if self.textFieldPinSTR == "" || self.pinText.text == nil{
                Constants.appDelegate.stopIndicator()
                CustomAlertController.showAlert(on: self, with: Simple, andAttributes:["title": "Enter valid pincode.", "message":""])
            }
            else{
                self.pinText.resignFirstResponder()
                self.pinText.text =  self.textFieldPinSTR
                
                let urlStr =  Constants.BASEURL + "checkAvailability"
                let param = "product_id=\(self.productID)&pincode=\(String(describing: self.textFieldPinSTR))&color=\(self.selectedColorKey)"
                
                Server.postRequestWithURL(urlStr, paramString: param, completionHandler: { (response) in
                    
                    if response[Constants.STATE]?.int32Value  == 1 {
                        
                        DispatchQueue.main.async {
                            self.pinSelected = "\(String(describing: self.textFieldPinSTR))"
                            UserDefaults.standard.set(self.pinSelected, forKey: Constants.selectedPin)
                            self.buyNowButton.isUserInteractionEnabled = true

                            self.productAvailabilityLabel.textColor = Colors.greenTextColor
                            self.productAvailabilityLabel.text = "✓ Stock Available for this Product and can be delivered in 10-12 days."
                            
                        }
                    }
                    else {
                        DispatchQueue.main.async {
                            let mess = response["message"] as! String
                            self.buyNowButton.isUserInteractionEnabled = false
                            self.productAvailabilityLabel.textColor = UIColor.red
                            self.productAvailabilityLabel.text = "✖ \(mess)"
                        }
                    }
                })
            }
        }
        else {
            Constants.appDelegate.stopIndicator()
            
            CustomAlertController.showAlert(on: self, with: Simple, andAttributes:["title": "", "message":"Please check your internet connection."])
        }
    }
    
    @IBAction func BuyNowPressed(_ sender: Any) {
        
        if UserDefaults.standard.array(forKey: Constants.CART_Array) == nil{
            
            var sellpricestr = String()//(describing: self.responseData["final_price"]!)
            if self.responseData["price"] == nil{
                print("Empty")
            }
            else{
                sellpricestr = String(describing: self.responseData["price"]!)
            }
            
            
            let dict = self.selectedImageArray[0] as! Dictionary<String,Any>
            let imgSTR = dict["image_big"] as! String
            var skuDesc = self.responseData["product_sku"] as! String
            var prodID = self.productID

            if self.responseData["product_type"] as! String == "configurable" && SelectedColorDict.count > 0{
                skuDesc = SelectedColorDict["sku"] as! String
                prodID = self.SelectedColorDict["id"] as! String
            }
            
            let cartDict = ["p_ID":prodID,
                            "p_Name":self.productNameLabel.text,
                            "p_SKU":skuDesc,
                            "P_Price":sellpricestr,
                            "p_Quantity":"1",
                            "P_Total":sellpricestr,
                            "p_Image":imgSTR,
                            "pincode":self.pinText.text!]
            
            var array = Array<Any>()
            array.append(cartDict)
            
            UserDefaults.standard.set(array, forKey: Constants.CART_Array)
            
        }
        else{
            var cartArray = UserDefaults.standard.array(forKey: Constants.CART_Array)!
            var skuDesc = self.responseData["product_sku"] as! String
            var prodID = self.productID
            
            if self.responseData["product_type"] as! String == "configurable" && SelectedColorDict.count > 0{
                skuDesc = SelectedColorDict["sku"] as! String
                prodID = self.SelectedColorDict["id"] as! String

            }

            var containsProd = false
            for diction  in cartArray{
                let dict = diction as! Dictionary<String,Any>
                let prodIDStr = dict["p_SKU"] as! String
                if prodIDStr == skuDesc{
                    containsProd = true
                }
            }
            
            if containsProd == false{
                var sellpricestr = String()//(describing: self.responseData["final_price"]!)
                if self.responseData["final_price"] == nil{
                    print("Empty")
                }
                else{
                    sellpricestr = String(describing: self.responseData["price"]!)
                }
                
                
                let dict = self.selectedImageArray[0] as! Dictionary<String,Any>
                let imgSTR = dict["image_big"] as! String
                
                
                let cartDict = ["p_ID":prodID,
                                "p_Name":self.productNameLabel.text,
                                "p_SKU":skuDesc,
                                "P_Price":sellpricestr,
                                "p_Quantity":"1",
                                "P_Total":sellpricestr,
                                "p_Image":imgSTR,
                                "pincode":self.pinText.text!]
                
                cartArray.append(cartDict)
                UserDefaults.standard.set(cartArray, forKey: Constants.CART_Array)
            }
        }
        
        DispatchQueue.main.async {
            let destViewController = Constants.mainStoryboard.instantiateViewController(withIdentifier: "CartVC") as! CartVC
            self.navigationController?.pushViewController(destViewController, animated: true)
            
        }
    }
    
    //MARK:- setDATA
    func setDataForView(responseData : Dictionary<String, Any>){
        
        self.featureArray = self.responseData["features"] as! Array<Any>
        self.specArray = self.responseData["specifications"] as! Array<Any>
        if specArray.count > 0 && self.featureArray.count > 0{
            headerArray = ["Features","Specifications"]
            self.setHeaderScroll()
        }
        else if specArray.count > 0 && self.featureArray.count == 0{
            headerArray = ["Specifications"]
            self.setHeaderScroll()
        }
        else if self.featureArray.count > 0 && self.specArray.count == 0{
            headerArray = ["Features"]
            self.setHeaderScroll()
        }
        else{
            headerArray = []
        }


        if self.responseData["color_products"] == nil{
            print("Empty")
        }
        else{
            colorProductsDict = self.responseData["color_products"] as! Dictionary<String,Any>
        }
        

        
        if self.responseData["color_options"] == nil{
            print("Empty")
            self.colorAvalableLabel.isHidden = true
        }
        else{
            self.colorAvalableLabel.isHidden = false
            self.colorDict = (self.responseData["color_options"] as? Dictionary<String, Any>)!
            
            if self.colorDict.count > 0
            {
                for dc in self.colorDict{
                    let key = dc.key
                    let val = dc.value as! Dictionary<String,Any>
                    
                    let valImg = val["thumb"] as! String
                    let valName = val["label"] as! String
                    
                    let newDc = ["key" : key,
                                 "label" : valName,
                                 "thumb" : valImg]
                    
                    self.colorArray.append(newDc)
                }
            }
        }
        
        
        if self.responseData["associated_images"] == nil{
            print("Empty")
        }
        else{
            self.associatedImagesDict = self.responseData["associated_images"] as! Dictionary<String,Any>
        }
        
        
        if self.colorArray.count > 0{
            let dict = self.colorArray[0] as! Dictionary<String,Any>
            let key = dict["key"] as! String
            selectedColorKey = key
            
            for dc in self.associatedImagesDict{
                let dc_key = dc.key
                if dc_key == key {
                    self.selectedImageArray.removeAll()
                    self.selectedImageArray = dc.value as! Array<Any>
                    SelectedColorDict = colorProductsDict[key] as! Dictionary<String,Any>

                }
            }
        }
        else{
            if self.responseData["images"] == nil{
                print("Empty")
            }
            else{
                self.selectedImageArray = self.responseData["images"] as! Array<Any>
            }
        }
        
        DispatchQueue.main.async {
            
            if self.responseData["product_name"] == nil{
                print("Empty")
            }
            else{
                self.productNameLabel.text = self.responseData["product_name"] as? String
            }
            
            var pricestr = String()//(describing: self.responseData["price"]!)
            if self.responseData["price"] == nil{
                print("Empty")
            }
            else{
                pricestr = String(describing: self.responseData["price"]!)
            }
            
            var sellpricestr = String()//(describing: self.responseData["final_price"]!)
            if self.responseData["final_price"] == nil{
                print("Empty")
            }
            else{
                sellpricestr = String(describing: self.responseData["final_price"]!)
            }
            
            
            let attr = [NSForegroundColorAttributeName: UIColor.black, NSFontAttributeName: UIFont(name: FONTS.Helvetica_Medium, size: 18)!] as [String : Any]//,NSStrikethroughStyleAttributeName : 3
            let oldP = NSMutableAttributedString.init(string: "MRP: ₹\(pricestr)", attributes: attr)
            self.productMRPLabel.attributedText = oldP
            
            self.productSellingPriceLabel.text = "SELLING PRICE: ₹\(sellpricestr)"
            
            self.SetUpDesription()

            if self.headerArray.count > 0{
                self.mainScrollContentHeight.constant = 1336
                self.setUpScrollView()
            }
            else{
                self.mainScrollContentHeight.constant = 714
            }
            
            if self.colorArray.count > 0{
                self.colorCollection.delegate = self
                self.colorCollection.dataSource = self
                self.colorCollection.reloadData()
            }
            
            if self.selectedImageArray.count > 0{
                self.productImageCollection.delegate = self
                self.productImageCollection.dataSource = self
                self.productImageCollection.reloadData()
            }
        }
        
        Constants.appDelegate.stopIndicator()
    }
    
    

    // MARK:- FETCH API
    func getProductDetailsforType(){
        
        if Reachability.isConnectedToNetwork() {
            Constants.appDelegate.startIndicator()
            
            let urlStr = Constants.BASEURL + "productview"
            let param = "product_id=\(self.productID)"
            
            Server.postRequestWithURL(urlStr, paramString: param, completionHandler: { (response) in
                
                if response[Constants.STATE]?.int32Value  == 1 {
                    
                    self.responseData = response["product"] as! Dictionary<String,Any>
                    
                    let prodID = "PROD\(self.productID)"
                    UserDefaults.standard.set(self.responseData, forKey: prodID)

                    self.setDataForView(responseData: self.responseData)
                    Constants.appDelegate.stopIndicator()
                }
                else {
                    
                    let prodID = "PROD\(self.productID)"
                    let dict = UserDefaults.standard.value(forKey: prodID) as! Dictionary<String,Any>
                    if dict.count > 0 {
                        Constants.appDelegate.stopIndicator()
                        self.responseData = UserDefaults.standard.value(forKey: prodID) as! Dictionary<String,Any>
                        self.setDataForView(responseData: self.responseData)
                    }
                    else{
                        Constants.appDelegate.stopIndicator()
                        var mess = String()
                        if response.count == 0{
                            mess = "Some Error Occurred"
                        }
                        else{
                            mess = response["message"] as! String
                        }
                        
                        CustomAlertController.showAlert(on: self, with: Simple, andAttributes: ["title":"", "message":mess])
                    }
                }
            })
        }
        else {
            CustomAlertController.showAlert(on: self, with: Simple, andAttributes:["title": "", "message":"Please check your internet connection."])
        }
    }

    
    func SetUpDesription(){
        descriptionArray = responseData["description"] as! Array<Any>//short_description//description
        let descriptionStr  = responseData["short_description"] as! String//short_description//description

        if descriptionArray.count > 0 {
            self.productDescriptionTable.isHidden = false
            self.productDescriptionLabel.isHidden = true
            self.view.bringSubview(toFront: self.productDescriptionTable)
            self.productDescriptionTable.reloadData()
        }
        else
        {
            self.productDescriptionTable.isHidden = true
            self.productDescriptionLabel.isHidden = false
            do {
                let str = try NSAttributedString(data: descriptionStr.data(using: String.Encoding.unicode, allowLossyConversion: true)!, options: [NSDocumentTypeDocumentAttribute : NSHTMLTextDocumentType], documentAttributes: nil)
                self.productDescriptionLabel.attributedText = str

            } catch {
                print(error)
            }
            
//            self.productDescriptionLabel.attributedText = NSAttributedString.init(string: descriptionStr)
            self.view.bringSubview(toFront: self.productDescriptionLabel)
        }
    }
    
    //MARK:- Scroll
    
    
    func setHeaderScroll()
    {
        
        var scrollContentCount = 0;
        var frame : CGRect = CGRect.zero
        var labelx : CGFloat = 0.0
        let count = CGFloat(headerArray.count)
        
        let width  : CGFloat = ScreenSize.SCREEN_WIDTH / count
        let firstLabelWidth : CGFloat = ScreenSize.SCREEN_WIDTH / count

        indicatorLabel = UILabel.init(frame: CGRect.init(x: labelx, y: 45, width: firstLabelWidth, height: 4))
        indicatorLabel.backgroundColor = Colors.appThemeColorText
        indicatorLabel.text = "";
        headerScroll.addSubview(indicatorLabel)
        
        for arrayIndex in 0 ..< headerArray.count
        {
            frame.origin.x = labelx
            // set scorllview properties
            frame.origin.y = 0;
            frame.size.width=width;
            frame.size.height=45;
            
            let accountLabelButton = UIButton.init(type: UIButtonType.custom)
            accountLabelButton.backgroundColor = Colors.Lighttextcolor
            accountLabelButton.addTarget(self, action:#selector(HeaderlabelSelected), for: UIControlEvents.touchUpInside)
            
            accountLabelButton.frame = frame;
            accountLabelButton.tag = arrayIndex;
            
            accountLabelButton.titleLabel?.numberOfLines = 0
            let heading = headerArray[arrayIndex]
            
            headerScroll.addSubview(accountLabelButton)
            
            if arrayIndex == 0
            {
                accountLabelButton.setAttributedTitle(NSAttributedString(string: heading.uppercased(),  attributes: [NSForegroundColorAttributeName : Colors.appThemeColorText,NSFontAttributeName : UIFont.init(name: "Helvetica-Bold", size: 20.0)!]), for: UIControlState.normal)

                accountLabelButton.alpha=1.0;
            }
            else
            {
                accountLabelButton.setAttributedTitle(NSAttributedString(string: heading.uppercased(),  attributes: [NSForegroundColorAttributeName : UIColor.black,NSFontAttributeName : UIFont.init(name: "Helvetica-Bold", size: 20.0)!]), for: UIControlState.normal)

                accountLabelButton.alpha=0.6;
            }
            
            accountLabelButton.titleLabel?.textAlignment = NSTextAlignment.center

            
            labelx=labelx+width;
            scrollContentCount = scrollContentCount + 1;
        }
        
        headerScroll.contentSize = CGSize.init(width: frame.width/count, height: 50)
        automaticallyAdjustsScrollViewInsets = false;
    }
    
    func HeaderlabelSelected(sender: UIButton)
    {
        let count = CGFloat(headerArray.count)
        let buttonArray = NSMutableArray()
        let xAxis : CGFloat = ScreenSize.SCREEN_WIDTH * CGFloat(sender.tag)

        for view in headerScroll.subviews
        {
            if view.isKind(of: UIButton.classForCoder())
            {
                let labelButton = view as! UIButton
                buttonArray.add(labelButton)
                let heading = headerArray[labelButton.tag]

                if labelButton.tag == sender.tag {
                    labelButton.setAttributedTitle(NSAttributedString(string: heading.uppercased(),  attributes: [NSForegroundColorAttributeName : Colors.appThemeColorText,NSFontAttributeName : UIFont.init(name: "Helvetica-Bold", size: 20.0)!]), for: UIControlState.normal)

                    labelButton.alpha = 1.0
                }
                else
                {
                    labelButton.setAttributedTitle(NSAttributedString(string: heading.uppercased(),  attributes: [NSForegroundColorAttributeName : UIColor.black,NSFontAttributeName : UIFont.init(name: "Helvetica-Bold", size: 20.0)!]), for: UIControlState.normal)

                    labelButton.alpha = 0.6
                }
            }
        }
        
        let labelButton = buttonArray[sender.tag]
        var frame : CGRect = (labelButton as AnyObject).frame
        frame.origin.y = 46
        frame.size.height = 4
        
        UIView.animate(withDuration: 0.2, animations: {
            self.detailScroll.setContentOffset(CGPoint.init(x: xAxis, y: 0), animated: true)

            if sender.tag == 0
            {
                self.indicatorLabel.frame =  CGRect.init(x: 0, y: 45 , width: ScreenSize.SCREEN_WIDTH/count, height: 4)
            }
            else
            {
                self.indicatorLabel.frame =  CGRect.init(x: (ScreenSize.SCREEN_WIDTH/count) * CGFloat(sender.tag), y: 45, width: ScreenSize.SCREEN_WIDTH/count, height: 4)
            }
            self.headerScroll.scrollRectToVisible(self.indicatorLabel.frame, animated: false)
            


        }, completion: nil)
    }
    
    // MARK: -  Main Scrollview
    func setUpScrollView() {//
        
        for arrayIndex in headerArray {
            
            if (arrayIndex == "Features") {
                featureView = Constants.mainStoryboard.instantiateViewController(withIdentifier: "featureVC") as! featureVC
                featureView.featureArray = self.featureArray
                ControllerArray.insert(featureView, at: 0)
            }
            
            if (arrayIndex == "Specifications")  {
                specialView = Constants.mainStoryboard.instantiateViewController(withIdentifier: "SpecialisationVC") as! SpecialisationVC
                specialView.specArray = self.specArray
                ControllerArray.insert(specialView, at: 1)
            }
        }
        setViewControllers(viewControllers: ControllerArray as NSArray, animated: false)
    }
    
    //  Add and remove view controllers
    
    func setViewControllers(viewControllers : NSArray, animated : Bool) {
        if self.childViewControllers.count > 0 {
            for vC in self.childViewControllers {
                vC.willMove(toParentViewController: nil)
                vC.removeFromParentViewController()
            }
        }
        
        for vC in viewControllers {
            
            self.addChildViewController(vC as! UIViewController)
            (vC as! UIViewController).didMove(toParentViewController: self)
        }
        
        //TODO animations
        if ((detailScroll) != nil) {
            reloadPages()
        }
    }
    
    func reloadPages() {
        var scrollContentCount = 0;
        
        for arrayIndex in 0 ..< headerArray.count {
            // set scorllview properties
            var frame : CGRect = CGRect.init(x: 0, y: 0, width: 0, height: 616)
            frame.origin.x = ScreenSize.SCREEN_WIDTH * CGFloat(arrayIndex)
            frame.origin.y = 0;
            frame.size = CGSize.init(width: 1024, height: 616)
            
            detailScroll.isPagingEnabled = true
            let contentWidth : CGFloat = ScreenSize.SCREEN_WIDTH * CGFloat(scrollContentCount+1)
            detailScrollContentwidth.constant = contentWidth;
            
            let vC = self.childViewControllers[arrayIndex]
            self.addView(view: vC.view, contentView: contentView, frame: frame)
            
            scrollContentCount = scrollContentCount + 1;
        }
        
        detailScroll.contentSize = CGSize.init(width: detailScrollContentwidth.constant, height: detailScroll.frame.size.height)
        detailScroll.delegate = self
    }
    
    // adding views to scrollview
    func addView(view : UIView, contentView : UIView, frame : CGRect)  {
        view.frame = frame
        view.translatesAutoresizingMaskIntoConstraints = false
        contentView.addSubview(view)
        
        contentView.addConstraint(NSLayoutConstraint.init(item: view, attribute: NSLayoutAttribute.leading, relatedBy: NSLayoutRelation.equal, toItem: contentView, attribute:  NSLayoutAttribute.leading, multiplier: 1.0, constant: frame.origin.x))
        contentView.addConstraint(NSLayoutConstraint.init(item: view, attribute: NSLayoutAttribute.width, relatedBy: NSLayoutRelation.equal, toItem: nil, attribute:  NSLayoutAttribute.notAnAttribute, multiplier: 1.0, constant: frame.size.width))
        contentView.addConstraint(NSLayoutConstraint.init(item: view, attribute: NSLayoutAttribute.top, relatedBy: NSLayoutRelation.equal, toItem: contentView, attribute:  NSLayoutAttribute.top, multiplier: 1.0, constant:0))
        
        contentView.addConstraint(NSLayoutConstraint.init(item: view, attribute: NSLayoutAttribute.height, relatedBy: NSLayoutRelation.equal, toItem: nil, attribute:  NSLayoutAttribute.notAnAttribute, multiplier: 1.0, constant: frame.size.height))
    }
    
    //MARK:- TABLEVIEW
    func numberOfSections(in tableView: UITableView) -> Int {
        return descriptionArray.count
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if expandedSections.contains(section) {
            return 2
        }
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let dict = self.descriptionArray[indexPath.section] as! Dictionary<String,Any>

        if indexPath.row == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "titleCell", for: indexPath)
            let lab = cell.contentView.viewWithTag(802) as! UILabel

            var str = dict["title"] as? String
            
            str = str?.replacingOccurrences(of: "Â®", with: "®")
            str = str?.replacingOccurrences(of: "â¢", with: "™")
            str = str?.replacingOccurrences(of: "Â", with: "")
            str = str?.replacingOccurrences(of: "â°C", with: "°C")
            str = str?.replacingOccurrences(of: "â", with: "'")

            lab.text = str
            
            tableView.estimatedRowHeight = 30
            tableView.rowHeight = UITableViewAutomaticDimension
            
            return cell
        }
        else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "descriptioncell", for: indexPath)
            let lab = cell.contentView.viewWithTag(803) as! UILabel
            
            var str = dict["description"] as? String

            str = str?.replacingOccurrences(of: "Â®", with: "®")
            str = str?.replacingOccurrences(of: "â¢", with: "™")
            str = str?.replacingOccurrences(of: "Â", with: "")
            str = str?.replacingOccurrences(of: "â°C", with: "°C")
            str = str?.replacingOccurrences(of: "â", with: "'")

            lab.text = str

            tableView.estimatedRowHeight = 45
            tableView.rowHeight = UITableViewAutomaticDimension

            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)

        if indexPath.row == 0 {
            let cell = tableView.cellForRow(at: NSIndexPath.init(row: 0, section: indexPath.section) as IndexPath)
            let img = cell?.contentView.viewWithTag(801) as! UIImageView

            var isExpanded = Bool()
            if expandedSections.contains(indexPath.section) {
                isExpanded = true
            }
            else{
                isExpanded = false
            }
            
            
            if isExpanded == true{
                expandedSections.remove(NSIndexSet.init(index: indexPath.section) as IndexSet)
//                tableView.beginUpdates()
                let index = IndexPath.init(row: 1, section: indexPath.section)
                tableView.deleteRows(at: [index], with: .none)
                img.transform = CGAffineTransform.init(rotationAngle: 0.0)
//                tableView.endUpdates()
            }
            else{
                var deleteIndex = IndexPath()
                let count = expandedSections.count

                if count > 0{
                    let selectedIndex = expandedSections.firstIndex
                    deleteIndex = IndexPath.init(row: 1, section: selectedIndex)
                    expandedSections.remove(NSIndexSet.init(index: selectedIndex) as IndexSet)
                    let cell_old = tableView.cellForRow(at: NSIndexPath.init(row: 0, section: selectedIndex) as IndexPath)
                    let img_old = cell_old?.contentView.viewWithTag(801) as! UIImageView
                    
//                    tableView.beginUpdates()
                        tableView.deleteRows(at: [deleteIndex], with: .none)
                        img_old.transform = CGAffineTransform.init(rotationAngle: 0.0)
//                    tableView.endUpdates()
                }
                
                let addindex = IndexPath.init(row: 1, section: indexPath.section)
                expandedSections.add(NSIndexSet.init(index: indexPath.section) as IndexSet)

//                tableView.beginUpdates()
                    tableView.insertRows(at: [addindex], with: .none)
                    img.transform = CGAffineTransform.init(rotationAngle: CGFloat(Double.pi/2))
//                tableView.endUpdates()

            }
            
            DispatchQueue.main.async {
                tableView.reloadData()
            }
            
        }
    }
    
    
    
    
    //MARK:- TEXTFIELD DELEGATE
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == self.pinText{
            
            let charSet : CharacterSet = CharacterSet.init(charactersIn: "0987654321")
            let newRa = string.rangeOfCharacter(from: charSet)
            if (newRa == nil || range.location > 5) && string != "" {
                
                self.textFieldPinSTR = "\(self.pinText.text!)\(string)"
                self.buyNowButton.isUserInteractionEnabled = false
                self.productAvailabilityLabel.textColor = UIColor.red
                self.productAvailabilityLabel.text = ""

                return false
            }
            else if range.location == 5 && string != ""{
                self.textFieldPinSTR = "\(self.pinText.text!)\(string)"
                self.checkPinAvailabilityAPI()
            }
            else if range.location == 0 {
                self.textFieldPinSTR = ""
                self.buyNowButton.isUserInteractionEnabled = false
                self.productAvailabilityLabel.textColor = UIColor.red
                self.productAvailabilityLabel.text = ""
            }
            else{
                
                self.textFieldPinSTR = "\(self.pinText.text!)\(string)"

                self.buyNowButton.isUserInteractionEnabled = false
                self.productAvailabilityLabel.textColor = UIColor.red
                self.productAvailabilityLabel.text = ""
            }
        }
            
            return true
    }
    
    
    
    
    // MARK:- COLLECTION VIEW


    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == self.colorCollection {
            return colorArray.count
        }
        else if collectionView == self.productImageCollection{
            return self.selectedImageArray.count
        }
        else if collectionView == self.popOverCollection {
            return self.selectedImageArray.count
        }
        return 5
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        if collectionView == self.popOverCollection{
            self.popOverImagePageControl.currentPage = Int(indexPath.item)
            if self.selectedImageArray.count > 1 && indexPath.item == 0{
                prev_popOver.isHidden = true
                next_popOver.isHidden = false
            }
            else if self.selectedImageArray.count > 1 && indexPath.item == self.selectedImageArray.count - 1{
                prev_popOver.isHidden = false
                next_popOver.isHidden = true
            }
            else if self.selectedImageArray.count > 1{
                prev_popOver.isHidden = false
                next_popOver.isHidden = false
            }
            else{
                prev_popOver.isHidden = true
                next_popOver.isHidden = true
            }

        }
        else if collectionView == self.productImageCollection{
            if self.selectedImageArray.count > 1 && indexPath.item == 0{
                prev.isHidden = true
                next_Button.isHidden = false
            }
            else if self.selectedImageArray.count > 1 && indexPath.item == self.selectedImageArray.count - 1{
                prev.isHidden = false
                next_Button.isHidden = true
            }
            else if self.selectedImageArray.count > 1{
                prev.isHidden = false
                next_Button.isHidden = false
            }
            else{
                prev.isHidden = true
                next_Button.isHidden = true
            }
        }
    }
    
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if collectionView == self.colorCollection {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell_Color", for: indexPath)
            let dict = self.colorArray[indexPath.item] as! Dictionary<String,Any>
        
//            let imgURL = dict["thumb"] as! String
            let img = cell.viewWithTag(901) as! UIImageView

            let imgSTR = dict["thumb"] as! String
            let imageUrl = URL.init(string: imgSTR)
            
            let documentsDirectoryURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
            if !FileManager.default.fileExists(atPath: documentsDirectoryURL.appendingPathComponent((imageUrl!.lastPathComponent)).path) {
                if Reachability.isConnectedToNetwork() == true {
                    Helper.downloadImageLinkAndCreateAsset(imgSTR)
                    img.sd_setImage(with: URL.init(string: imgSTR))
                }
                else{
                    img.image = UIImage.init(named: "noImage.png")
                }
            }
            else{
                let abc = documentsDirectoryURL.appendingPathComponent(imageUrl!.lastPathComponent).path
                let imageURL = URL(fileURLWithPath: abc)
                img.image = UIImage(contentsOfFile: imageURL.path)
            }

            
        
            return cell
        }
        else if collectionView == self.popOverCollection{
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "popOverImageCell", for: indexPath)
            let img = cell.viewWithTag(601) as! UIImageView
            let dict = self.selectedImageArray[indexPath.item] as! Dictionary<String,Any>
            let imgSTR = dict["image_big"] as! String
            let imageUrl = URL.init(string: imgSTR)

            let documentsDirectoryURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
            if !FileManager.default.fileExists(atPath: documentsDirectoryURL.appendingPathComponent((imageUrl!.lastPathComponent)).path) {
                
                if Reachability.isConnectedToNetwork() == true {
                    Helper.downloadImageLinkAndCreateAsset(imgSTR)
                    img.sd_setImage(with: URL.init(string: imgSTR))
                }
                else{
                    img.image = UIImage.init(named: "noImage.png")
                }
            }
            else{
                let abc = documentsDirectoryURL.appendingPathComponent(imageUrl!.lastPathComponent).path
                let imageURL = URL(fileURLWithPath: abc)
                img.image = UIImage(contentsOfFile: imageURL.path)
            }

            return cell
        }
        else{
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell_image", for: indexPath)
            let dict = self.selectedImageArray[indexPath.item] as! Dictionary<String,Any>
            let img = cell.viewWithTag(911) as! UIImageView

            let imgSTR = dict["image_big"] as! String
            let imageUrl = URL.init(string: imgSTR)
            
            let documentsDirectoryURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
            if !FileManager.default.fileExists(atPath: documentsDirectoryURL.appendingPathComponent((imageUrl!.lastPathComponent)).path) {

                if Reachability.isConnectedToNetwork() == true {
                    Helper.downloadImageLinkAndCreateAsset(imgSTR)
                    img.sd_setImage(with: URL.init(string: imgSTR))
                }
                else{
                    img.image = UIImage.init(named: "noImage.png")
                }
            }
            else{
                let abc = documentsDirectoryURL.appendingPathComponent(imageUrl!.lastPathComponent).path
                let imageURL = URL(fileURLWithPath: abc)
                img.image = UIImage(contentsOfFile: imageURL.path)
            }
            
            
            return cell
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if collectionView == self.colorCollection {
            self.pinText.text = ""
            self.productAvailabilityLabel.text = ""
            self.buyNowButton.isUserInteractionEnabled = false
            
            let dict = self.colorArray[indexPath.item] as! Dictionary<String,Any>
            let key = dict["key"] as! String
            selectedColorKey = key
            
            if SelectedColorDict.count > 0{
                SelectedColorDict.removeAll()
            }
            
            SelectedColorDict = colorProductsDict[key] as! Dictionary<String,Any>
            
            DispatchQueue.main.async {
                var pricestr = String()//(describing: self.responseData["price"]!)
                if self.SelectedColorDict["price"] == nil{
                    print("Empty")
                }
                else{
                    pricestr = String(describing: self.responseData["price"]!)
                }
                
                var sellpricestr = String()//(describing: self.responseData["final_price"]!)
                if self.SelectedColorDict["final_price"] == nil{
                    print("Empty")
                }
                else{
                    sellpricestr = String(describing: self.SelectedColorDict["final_price"]!)
                }
                
                let attr = [NSForegroundColorAttributeName: UIColor.black, NSFontAttributeName: UIFont(name: FONTS.Helvetica_Medium, size: 18)!] as [String : Any]//,NSStrikethroughStyleAttributeName : 3
                let oldP = NSMutableAttributedString.init(string: "MRP: ₹\(pricestr)", attributes: attr)
                self.productMRPLabel.attributedText = oldP
                
                self.productSellingPriceLabel.text = "SELLING PRICE: ₹\(sellpricestr)"
            }

            
            
            for dc in self.associatedImagesDict{
                let dc_key = dc.key
                if dc_key == key {
                    self.selectedImageArray.removeAll()
                    self.selectedImageArray = dc.value as! Array<Any>
                }
            }
            
            self.reloadData()
//            self.perform(#selector(reloadData), with: nil, afterDelay: 0.05)
        }
        else if collectionView == self.productImageCollection{
            DispatchQueue.main.async {
                self.popOverCollection.reloadData()
                self.view.bringSubview(toFront: self.popOverView)
                self.popOverCollection.scrollToItem(at: indexPath, at: UICollectionViewScrollPosition.centeredHorizontally, animated: false)
                self.popOverImagePageControl.numberOfPages = self.selectedImageArray.count
                self.popOverImagePageControl.currentPage = Int(indexPath.item)
                self.popOverView.isHidden = false
            }
        }
        else {
            DispatchQueue.main.async {
                self.productImageCollection.scrollToItem(at: indexPath, at: UICollectionViewScrollPosition.centeredHorizontally, animated: false)
                self.popOverView.isHidden = true
                self.view.sendSubview(toBack: self.popOverView)
            }
        }
    }
    
    func reloadData(){
        DispatchQueue.main.async {
            if self.selectedImageArray.count > 0{
                self.productImageCollection.delegate = self
                self.productImageCollection.dataSource = self
                self.productImageCollection.reloadData()
            }
        }

    }
    
    
    func PreviousImagePresed(sender: Any){
        
        if (sender as! UIButton).tag == 912{
            print("Recognise me")
            let ar = self.productImageCollection.indexPathsForVisibleItems
            if selectedImageArray.count > 0
            {
                let opt1 = ar.first
                let opt2 = ar.last
                
                var currentIndexPathReset = opt2
                
                if (opt1?.item)! > (opt2?.item)!{
                    currentIndexPathReset = opt1
                }
                
                if((currentIndexPathReset?.item)! > 0)
                {
                    if currentIndexPathReset != nil
                    {
                        let newIndex = currentIndexPathReset!.item - 1
                        self.productImageCollection.scrollToItem(at: IndexPath.init(item: newIndex, section: 0), at: UICollectionViewScrollPosition.centeredHorizontally, animated: true)
                    }
                }
            }
        }
        else if (sender as! UIButton).tag == 602{
            let ar = self.popOverCollection.indexPathsForVisibleItems
            if selectedImageArray.count > 0
            {
                let opt1 = ar.first
                let opt2 = ar.last
                
                var currentIndexPathReset = opt2
                
                if (opt1?.item)! > (opt2?.item)!{
                    currentIndexPathReset = opt1
                }
                
                if((currentIndexPathReset?.item)! > 0)
                {
                    if currentIndexPathReset != nil
                    {
                        let newIndex = currentIndexPathReset!.item - 1
                        self.popOverCollection.scrollToItem(at: IndexPath.init(item: newIndex, section: 0), at: UICollectionViewScrollPosition.centeredHorizontally, animated: true)
                    }
                }
            }
        }
    }

    func NextImagePresed(sender: Any){
        
        if (sender as! UIButton).tag == 913{
            let ar = self.productImageCollection.indexPathsForVisibleItems
            if selectedImageArray.count > 0
            {
                let MaxSections = selectedImageArray.count
                var currentIndexPathReset = ar.last
                
                if((currentIndexPathReset?.item)! < MaxSections - 1)
                {
                    if currentIndexPathReset != nil
                    {
                        let newIndex = currentIndexPathReset!.item + 1
                        
                        self.productImageCollection.scrollToItem(at: IndexPath.init(item: newIndex, section: 0), at: UICollectionViewScrollPosition.centeredHorizontally, animated: true)
                    }
                }
            }
        }

        else if (sender as! UIButton).tag == 603{
            let ar = self.popOverCollection.indexPathsForVisibleItems
            if selectedImageArray.count > 0
            {
                let MaxSections = selectedImageArray.count
                var currentIndexPathReset = ar.last
                
                if((currentIndexPathReset?.item)! < MaxSections - 1)
                {
                    if currentIndexPathReset != nil
                    {
                        let newIndex = currentIndexPathReset!.item + 1
                        
                        self.popOverCollection.scrollToItem(at: IndexPath.init(item: newIndex, section: 0), at: UICollectionViewScrollPosition.centeredHorizontally, animated: true)
                    }
                }
            }
        }
    }
    
    
    @IBAction func crossButtonPressed(_ sender: Any) {
        DispatchQueue.main.async {
            let ar = self.popOverCollection.indexPathsForVisibleItems
            let opt1 = ar.first
            let opt2 = ar.last
            var currentIndexPathReset = opt2
            
            if (opt1?.item)! > (opt2?.item)!{
                currentIndexPathReset = opt1
            }
            
            self.productImageCollection.scrollToItem(at: currentIndexPathReset!, at: UICollectionViewScrollPosition.centeredHorizontally, animated: false)
            self.popOverView.isHidden = true
            self.view.sendSubview(toBack: self.popOverView)
        }

    }
    
}
