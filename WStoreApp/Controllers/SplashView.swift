 //
//  SplashView.swift
//  MyRecipeApp
//
//  Created by Surbhi on 26/10/16.
//  Copyright © 2016 Neuronimbus. All rights reserved.
//

import UIKit
import Photos
 
class SplashView: UIViewController {

    var superCatArray = ["kitchen","laundry","living-space"]
    var categoryIDArray = Array<NSInteger>()
    var productIDArray = Array<NSInteger>()
    var imagArray = Array<Any>()
    var videoArray = Array<Any>()

    //MARK:- START
    override func viewDidLoad() {
        super.viewDidLoad()
        
        NotificationCenter.default.addObserver(self, selector: #selector(VideoDownloadComplete), name: NSNotification.Name(rawValue: "videoDownloadingComplete"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(ImageDownloadComplete), name: NSNotification.Name(rawValue: "ImageDownloadingComplete"), object: nil)

        if UserDefaults.standard.object(forKey: "TIMESTAMP") == nil
        {
            self.getImagesToDisplay()
        }
        else
        {
            
            GoToLogin()
        }
    }
    
    func viewDidAppear( animated: Bool) {

    }
    
    func getImagesToDisplay(){
        
        if Reachability.isConnectedToNetwork() {
            Constants.appDelegate.startIndicator()
            
            let urlStr = "http://neuronimbusinteractive.com/screensaver/get_image.php"
            Server.getRequestWithURL(urlString: urlStr, completionHandler: { (response) in
                Constants.appDelegate.stopIndicator()
                
                if response[Constants.STATE]?.int32Value  == 1 {
                    
                    var imgArray = response["data_ios"] as! Array<AnyObject>
                    
                    for img in imgArray{
                        let dc = img as! Dictionary<String,Any>
                        let imgStr = dc["image"] as! String
                        self.imagArray.append(imgStr)
                    }
                    
                    DispatchQueue.main.async {
                        
                        if imgArray.count > 1{
                            
                            let newImg = imgArray[imgArray.count - 1] as! Dictionary<String,Any>
                            let newmm = newImg["image"] as! String
                            let videoStr = "http://neuronimbusinteractive.com/screensaver/videos/Whirlpool3videoloop4.mp4"
                            let newvid = ["video" : videoStr,
                                          "image" : newmm]
                            
                            let count = imgArray.count
                            imgArray.insert(newvid as AnyObject, at: count)
                            self.videoArray.insert(videoStr, at: 0)
                            
                            UserDefaults.standard.set(imgArray, forKey: Constants.SCREENSAVER)

                            self.getSubCategoriesforType("kitchen")
                        }
                    }
                }
                else {
                    CustomAlertController.showAlert(on: self, with: Simple, andAttributes:["title": "", "message":"Please check your internet connection."])
                    self.GoToLogin()
                }
            })
        }
        else {
            CustomAlertController.showAlert(on: self, with: Simple, andAttributes:["title": "", "message":"Please check your internet connection."])
            GoToLogin()
        }
    }

    
    //MARK:- GET SUB CATEGORIES
    func getSubCategoriesforType(_ typeStr : String){
        // Kitchen // Laundry // Living Spaces
        
        if Reachability.isConnectedToNetwork() {
            Constants.appDelegate.startIndicator()
            
            let urlStr = Constants.BASEURL + "categories"
            let param = "type=\(typeStr)"
            
            Server.postRequestWithURL(urlStr, paramString: param, completionHandler: { (response) in
                
                if response[Constants.STATE]?.int32Value  == 1 {
                    
                    let subCatArray = response["data"] as! Array<AnyObject>
                    
                    UserDefaults.standard.set(subCatArray, forKey: typeStr.uppercased())
                    
                    for cat in subCatArray{
                        let dict = cat as! Dictionary<String,Any>
                        let catdID = dict["id"] as! NSInteger
                        let imageStr = dict["image"] as! String
                        self.imagArray.append(imageStr)
                        
                        if self.categoryIDArray.contains(catdID) == false{
                            self.categoryIDArray.append(catdID)
                        }
                    }
                    
                    if typeStr == "kitchen" {
                        Constants.appDelegate.stopIndicator()
                        self.getSubCategoriesforType("laundry")
                    }
                    else if typeStr == "laundry" {
                        Constants.appDelegate.stopIndicator()
                        self.getSubCategoriesforType("living-spaces")
                    }
                    else{
                        self.categoryIDArray.append(17) // for accessories addinf cat id manually
                        
                        UserDefaults.standard.set(self.categoryIDArray, forKey: Constants.catID_array)
                        Constants.appDelegate.stopIndicator()
                        
                        if self.videoArray.count>0{
                            Constants.appDelegate.startIndicator()
                            self.downloadVideos()
                        }
                        else{
                            if self.imagArray.count > 0 {
                                self.downloadImages()
                            }
                        }
//                        self.GoToLogin()
                    }
                }
                else {
                    self.getSubCategoriesforType(typeStr)
                }
            })
        }
        else {
            CustomAlertController.showAlert(on: self, with: Simple, andAttributes:["title": "", "message":"Please check your internet connection."])
        }
    }
    
    //MARK:- VIDEO DOWNLOAD
    func downloadVideos(){
        let documentsDirectoryURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
        
        for obj in 0 ..< self.videoArray.count{
            
            let imgname = self.videoArray[obj] as! String
            
            let imageUrl = URL.init(string: imgname)
            if !FileManager.default.fileExists(atPath: documentsDirectoryURL.appendingPathComponent((imageUrl!.lastPathComponent)).path) {
                Helper.downloadVideoLinkAndCreateAsset(imgname, objectNum: obj)
            }
            else{
                if obj == self.videoArray.count-1 {
                    self.downloadImages()
                }
            }
        }
    }
    
    func VideoDownloadComplete(noti : Notification){
        let obj =  noti.object as! NSInteger
        
        if obj == self.videoArray.count-1 {
            self.downloadImages()
        }
    }


    //MARK:- DOWNLOAD IMAGES
    func downloadImages(){
        let documentsDirectoryURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!

        for obj in 0 ..< self.imagArray.count{
            
            let imgname = self.imagArray[obj] as! String
            
            let imageUrl = URL.init(string: imgname)
            if !FileManager.default.fileExists(atPath: documentsDirectoryURL.appendingPathComponent((imageUrl!.lastPathComponent)).path) {
                Helper.downloadImageLinkAndCreateAsset(imgname, notificationName: "ImageDownloadingComplete", objectNUm: obj, isLast : true)
            }
            else{
                if obj == self.imagArray.count-1 {
                    let when = DispatchTime.now() + 15
                    DispatchQueue.main.asyncAfter(deadline: when) {
                        Constants.appDelegate.stopIndicator()
                        self.GoToLogin()
                    }
                }
            }
        }
    }
    
    func ImageDownloadComplete(noti : Notification){
        
        let obj =  noti.object as! NSInteger

        if obj == self.imagArray.count-1 {
            
            let when = DispatchTime.now() + 15
            DispatchQueue.main.asyncAfter(deadline: when) {
                Constants.appDelegate.stopIndicator()
                self.GoToLogin()
            }
        }
    }

    //MARK:- LOGIN
    
    func GoToLogin() {
        
        SVProgressHUD.dismiss()
        
        if UserDefaults.standard.string(forKey: Constants.UserID) == nil || UserDefaults.standard.string(forKey: Constants.UserID) == ""  {
            
                DispatchQueue.main.async(execute: {
                    
                    Constants.appDelegate.justLogedIn = false
                    let login = Constants.mainStoryboard.instantiateViewController(withIdentifier: "LoginView")
                    UIApplication.shared.keyWindow?.rootViewController = login
                })
        }
        else {
            Constants.appDelegate.justLogedIn = false

            if !self.isKeyPresentInUserDefaults(key: Constants.isTransactionEnabled){
                DispatchQueue.main.async(execute: {
                    
                    let forgot = Constants.mainStoryboard.instantiateViewController(withIdentifier: "DecisionVC") as! DecisionVC
                    forgot.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
                    self.present(forgot, animated: true, completion: nil)
                })

            }
            else{
                DispatchQueue.main.async(execute: {
                    
                    let viewController = Constants.mainStoryboard.instantiateViewController(withIdentifier: "SideMenuNavigation")
                    UIApplication.shared.keyWindow?.rootViewController = viewController
                })
            }
        }
    }
    
    
    func isKeyPresentInUserDefaults(key: String) -> Bool {
        return UserDefaults.standard.object(forKey: key) != nil
    }
    
    //MARK:- STATUS BAR HIDE UNHIDE
    override var prefersStatusBarHidden : Bool {
        return true
    }
    
    //MARK:- DOWNLOAD IMAGES AND SAVE IN PHGALARY
//    func downImages(){
//        
//        if (Helper.findAlbum(albumName: "WStoreImages") != nil){
//            Helper.saveImage(<#T##Helper#>)
//        }
//
//        
//        
////        [[PHPhotoLibrary sharedPhotoLibrary] performChanges:^{
////            [PHAssetChangeRequest creationRequestForAssetFromImage:[info valueForKey:UIImagePickerControllerOriginalImage]];
////            } completionHandler:^(BOOL success, NSError *error) {
////            if (success) {
////            NSLog(@"Success");
////            }
////            else {
////            NSLog(@"write error : %@",error);
////            }
////            }];
//    }
    
    
   
        
}
