//
//  PaymentCheckOutVC.swift
//  WStoreApp
//
//  Created by Surbhi on 31/08/17.
//  Copyright © 2017 SurbhiV. All rights reserved.
//

import UIKit
//#import <PayUmoney_SDK/PayUmoney_SDK.h>
import PayUmoney_SDK

class PaymentCheckOutVC: UIViewController, UIWebViewDelegate {

    var params : PUMRequestParams = PUMRequestParams.shared()
    var utils : Utils = Utils()
    var productsArray = Array<Any>()
    var checkoutArray = Dictionary<String,Any>()

    var productString = String()
    var checkoutString = String()
    
    @IBOutlet weak var paymentweb: UIWebView!
    @IBOutlet weak var backButton: UIButton!

    override func viewDidLoad() {
        super.viewDidLoad()
        self.orderAPiCalled()
        // Do any additional setup after loading the view.
        
        
        self.backButton.setTitle("CANCEL", for: .normal)
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override var prefersStatusBarHidden : Bool {
        return true
    }

    
    @IBAction func backButtonPressed(_ sender: Any) {
        
        if self.backButton.currentTitle == "HOME"{
            self.navigationController?.popToRootViewController(animated: false)
        }
        else{
            
            if UserDefaults.standard.value(forKey: Constants.QuoteID) == nil{
                let Alert = UIAlertController.init(title: "Cancel Order", message: "Cancelling the order will remove all items from the cart.", preferredStyle: UIAlertControllerStyle.alert)
                Alert.addAction(UIAlertAction.init(title: "YES", style: UIAlertActionStyle.default, handler: { (action:UIAlertAction) in
                    self.navigationController?.popToRootViewController(animated: false)
                }))
                
                Alert.addAction(UIAlertAction.init(title: "NO", style: UIAlertActionStyle.default, handler: { (action:UIAlertAction) in
                    //                self.navigationController?.popToRootViewController(animated: false)
                }))
                self.present(Alert, animated: true, completion: nil)
            }
            else
            {
                self.navigationController?.popToRootViewController(animated: false)
            }
//            else{
//                let Alert = UIAlertController.init(title: "Some Error Occurred", message: "Please make sure the personal information is correct.", preferredStyle: UIAlertControllerStyle.alert)
//                Alert.addAction(UIAlertAction.init(title: "Ok", style: UIAlertActionStyle.default, handler: { (action:UIAlertAction) in
//
//                    let controllers = self.navigationController?.childViewControllers
//                    
//                    for vc in controllers! {
//                        if vc.isKind(of: CartVC.classForCoder()){
//                            self.navigationController?.popToViewController(vc, animated: false)
//                        }
//                    }
//
//                }))
//
//                self.present(Alert, animated: true, completion: nil)
//            }
        }
    }
    
    
    func orderAPiCalled(){
        
        let jsonData_add = try! JSONSerialization.data(withJSONObject: checkoutArray, options: JSONSerialization.WritingOptions.prettyPrinted)
        checkoutString = NSString(data: jsonData_add, encoding: String.Encoding.utf8.rawValue)! as String
        checkoutString = checkoutString.replacingOccurrences(of: "\" : \"", with: "\":\"")
        
        
        
        let cartArray = UserDefaults.standard.array(forKey: Constants.CART_Array)!
        var arrayPar = Array<Any>()
        for index in 0 ... cartArray.count - 1 {
            let dict = cartArray[index] as! Dictionary<String,Any>
            
            let quant = (dict["p_Quantity"] as! String)
            let sku = String(describing: dict["p_SKU"] as! String)
            let productID = String(describing: dict["p_ID"] as! String)
            
            let newDict = ["sku":sku,
                           "qty":quant,
                           "product_id":productID]
            
            arrayPar.append(newDict)
        }
        
        let brandShopID = UserDefaults.standard.object(forKey: Constants.UserID) as! String
        
        if Reachability.isConnectedToNetwork() {
            Constants.appDelegate.startIndicator()
            var quoteID = ""
            if UserDefaults.standard.object(forKey: Constants.QuoteID) == nil{
                quoteID = ""
            }
            else{
                quoteID = "\(UserDefaults.standard.object(forKey: Constants.QuoteID)!)"
            }
            
            let urlStr = Constants.BASEURL + "saveorder"
            let paramStr = "checkout_data=\(checkoutString)&brandshop_id=\(brandShopID)&quote_id=\(quoteID)"
            
            Server.postRequestWithURL(urlStr, paramString: paramStr, completionHandler: { (response) in
                Constants.appDelegate.stopIndicator()
                
                URLCache.shared.removeAllCachedResponses()
                URLCache.shared.diskCapacity = 0
                URLCache.shared.memoryCapacity = 0
                
                if (response[Constants.STATE] as AnyObject).int32Value  == 1 {
                    
                    let htmlStr = response["html"] as! String

                    UserDefaults.standard.removeObject(forKey: Constants.QuoteID)
                    UserDefaults.standard.removeObject(forKey: Constants.DISCOUNTApplied)
                    UserDefaults.standard.removeObject(forKey: Constants.CART_Array)

                    DispatchQueue.main.async {
                        self.paymentweb.loadHTMLString(htmlStr, baseURL: nil)
                    }
                }
                else {
                    
                    let mess = response["message"] as! String
                    let quoteID = response["quote_id"] as! String
                    UserDefaults.standard.set(quoteID, forKey: Constants.QuoteID)
                    let Alert = UIAlertController.init(title: "", message: mess, preferredStyle: UIAlertControllerStyle.alert)
                    Alert.addAction(UIAlertAction.init(title: "Ok", style: UIAlertActionStyle.default, handler: { (action:UIAlertAction) in
                        
                        let controllers = self.navigationController?.childViewControllers
                        
                        for vc in controllers! {
                            if vc.isKind(of: CartVC.classForCoder()){
                                self.navigationController?.popToViewController(vc, animated: false)
                            }
                        }
                    }))
                    
                    self.present(Alert, animated: true, completion: nil)
                }
            })
        }
        else {

            
            
            Constants.appDelegate.stopIndicator()
            
            CustomAlertController.showAlert(on: self, with: Simple, andAttributes:["title": "", "message":"Please check your internet connection."])
        }
    }

    
    
    
    func webView(_ webView: UIWebView, shouldStartLoadWith request: URLRequest, navigationType: UIWebViewNavigationType) -> Bool {

        if request.description.contains("failure"){
            let Alert = UIAlertController.init(title: "Payment Failed", message: "", preferredStyle: UIAlertControllerStyle.alert)
            Alert.addAction(UIAlertAction.init(title: "Dismiss", style: UIAlertActionStyle.default, handler: { (action:UIAlertAction) in
                UserDefaults.standard.removeObject(forKey: Constants.QuoteID)
                UserDefaults.standard.removeObject(forKey: Constants.CART_Array)
                self.navigationController?.popToRootViewController(animated: false)
            }))
            
            self.present(Alert, animated: true, completion: nil)

        }
        if request.description.contains("/cancel"){
            UserDefaults.standard.removeObject(forKey: Constants.QuoteID)
            UserDefaults.standard.removeObject(forKey: Constants.DISCOUNTApplied)
            UserDefaults.standard.removeObject(forKey: Constants.CART_Array)
        }
        if request.description.contains("/thanks"){
            self.backButton.setTitle("HOME", for: .normal)
            UserDefaults.standard.removeObject(forKey: Constants.QuoteID)
            UserDefaults.standard.removeObject(forKey: Constants.DISCOUNTApplied)
            UserDefaults.standard.removeObject(forKey: Constants.CART_Array)

        }
        if request.description.contains("/success") {
            
            if Constants.BASEURL == "https://www.whirlpoolindiawstore.com/brandshop/api/" {
//                self.paymentweb.loadRequest(URLRequest.init(url: URL.init(string: "https://www.whirlpoolindiawstore.com/brandshop/index/thanks/")!))
            }
            else{
                self.paymentweb.loadRequest(URLRequest.init(url: URL.init(string: "http://52.66.173.141/brandshop/index/thanks/")!))
            }
        }

        
        return true
    }
    
    
    func webViewDidStartLoad(_ webView: UIWebView) {

        if (webView.request?.description.contains("/thanks"))!{
            self.backButton.setTitle("HOME", for: .normal)
        }
        if (webView.request?.description.contains("/success"))!{
            self.backButton.setTitle("HOME", for: .normal)
        }

    }
    
    func webViewDidFinishLoad(_ webView: UIWebView) {

        if (webView.request?.description.contains("/thanks"))!{
            self.backButton.setTitle("HOME", for: .normal)
            UserDefaults.standard.removeObject(forKey: Constants.QuoteID)
            UserDefaults.standard.removeObject(forKey: Constants.DISCOUNTApplied)
            UserDefaults.standard.removeObject(forKey: Constants.CART_Array)

        }
        if (webView.request?.description.contains("/success"))!{
            self.backButton.setTitle("HOME", for: .normal)
        }

    }
}
