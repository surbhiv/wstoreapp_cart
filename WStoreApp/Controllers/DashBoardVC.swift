//
//  DashBoardVC.swift
//  TestSwiftApp
//
//  Created by Surbhi on 21/10/16.
//  Copyright © 2016 Neuronimbus. All rights reserved.
//

import UIKit
import SafariServices

class DashBoardVC: UIViewController, ENSideMenuDelegate, SFSafariViewControllerDelegate, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    
    @IBOutlet weak var HomeCollection: UICollectionView!
    @IBOutlet weak var cartButton: UIButton!
    @IBOutlet weak var cartCountLable: UILabel!

    var catIDArray = Array<NSInteger>()
    var prodDArray = Array<NSInteger>()
    var isViaSideMenu = false
    
    var categImgArray = Array<String>()
    var colorListArray = Array<String>()
    
    var prodImageArray = Array<String>()
    
    var colorDetailArray = Array<String>()
    var featureArray = Array<String>()
    var prodCommonImageArray = Array<String>()
    var prodDetailImageArray = Array<String>()
    
    var isloadingProductDetail : Bool = false
    
    
    //MARK:- START
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        Constants.appDelegate.justLogedIn = false
        Constants.appDelegate.addNotificationforScreensaver()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        cartCountLable.layer.cornerRadius = 7.5
        cartCountLable.clipsToBounds = true
        
        
        if UserDefaults.standard.bool(forKey: Constants.isTransactionEnabled) == false{
            self.cartButton.isHidden = true
            if UserDefaults.standard.object(forKey: Constants.CART_Array) == nil{
                cartCountLable.isHidden = true
            }
            else{
                let arr = UserDefaults.standard.object(forKey: Constants.CART_Array) as! Array<Any>
                let count = arr.count
                cartCountLable.isHidden = false
                cartCountLable.text = "\(count)"
            }
        }
        else{
            self.cartButton.isHidden = false
            if UserDefaults.standard.object(forKey: Constants.CART_Array) == nil{
                cartCountLable.isHidden = true
            }
            else{
                let arr = UserDefaults.standard.object(forKey: Constants.CART_Array) as! Array<Any>
                let count = arr.count
                cartCountLable.isHidden = false
                cartCountLable.text = "\(count)"
            }
        }

        self.sideMenuController()?.sideMenu?.delegate = self
        self.navigationController?.isNavigationBarHidden = true
        
        sideMenuController()?.sideMenu?.allowPanGesture = true
        sideMenuController()?.sideMenu?.allowLeftSwipe = true
        sideMenuController()?.sideMenu?.allowRightSwipe = true
        
        
        if UserDefaults.standard.object(forKey: "TIMESTAMP") == nil
        {
            catIDArray = UserDefaults.standard.value(forKey: Constants.catID_array) as! Array<NSInteger>
            
            if self.isViaSideMenu == false{
                if catIDArray.count > 0{
                    self.getSub_SubCategoriesforType(catArray: catIDArray, objectLocation: 0)
                }
            }
            else{
                self.isViaSideMenu = false
            }
        }
        else{
            
            if self.isViaSideMenu == true
            {
                self.isViaSideMenu = false
            }
        }
    
    }
    
    @IBAction func toggleSideMenu(_ sender: AnyObject) {

        toggleSideMenuView()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let backItem = UIBarButtonItem()
        backItem.title = ""
        backItem.tintColor = UIColor.lightGray
        navigationItem.backBarButtonItem = backItem
    }
    override var prefersStatusBarHidden : Bool {
        return true
    }
    
    @IBAction func KitchenButtonPressed(){//(sender: AnyObject) {
        
        let destViewController = Constants.mainStoryboard.instantiateViewController(withIdentifier: "SubCategoryVC") as! SubCategoryVC
        destViewController.subCatString = "kitchen"
        destViewController.catArray = NSArray.init(objects: "laundry","living-spaces") as Array<AnyObject>
        
        self.navigationController?.pushViewController(destViewController, animated: true)
    }
    
    @IBAction func LaundryButtonPressed(){//(sender: AnyObject) {
        
        let destViewController = Constants.mainStoryboard.instantiateViewController(withIdentifier: "SubCategoryVC") as! SubCategoryVC
        destViewController.subCatString = "laundry"
        destViewController.catArray = NSArray.init(objects: "kitchen","living-spaces") as Array<AnyObject>
        
        self.navigationController?.pushViewController(destViewController, animated: true)
        
    }
    
    @IBAction func LivingSpacesButtonPressed(){//(sender: AnyObject) {
        
        let destViewController = Constants.mainStoryboard.instantiateViewController(withIdentifier: "SubCategoryVC") as! SubCategoryVC
        destViewController.subCatString = "living-spaces"
        destViewController.catArray = NSArray.init(objects: "kitchen","laundry") as Array<AnyObject>
        
        self.navigationController?.pushViewController(destViewController, animated: true)
    }
    
    @IBAction func SearchButtonPressed(_ sender: AnyObject) {
        
        // Do Searching Here
    }
    
    
    // MARK:- COLLECTION VIEW FLOWLAYOUT
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize
    {
        if DeviceType.IS_IPAD{
            
            if indexPath.section == 0 {
                return CGSize(width: (ScreenSize.SCREEN_MAX_LENGTH - 40)/3, height: (ScreenSize.SCREEN_MAX_LENGTH - 40)/3 + 50)
            }
            return CGSize(width: ScreenSize.SCREEN_MAX_LENGTH - 10, height: 110)
            
        }
        return CGSize(width: (collectionView.bounds.size.width - 25)/2, height: (collectionView.bounds.size.width - 25)/2)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        if DeviceType.IS_IPAD{
            return 10
        }
        return 5
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        if DeviceType.IS_IPAD{
            return 10
        }
        return 5
        
    }
    
    
    // MARK:- COLLECTION VIEW
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 2
    }
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if section == 0 {
            return 3
        }
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if indexPath.section == 0 {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "HomeCell", for: indexPath)
            
            let bg = cell.contentView.viewWithTag(101) as! UIImageView
            let titl = cell.contentView.viewWithTag(102) as! UILabel
            let forLab = cell.contentView.viewWithTag(103) as! UILabel
            
            if DeviceType.IS_IPAD{
                
                forLab.font = UIFont.init(name: FONTS.Helvetica_Light, size: 20)
                titl.font = UIFont.init(name: FONTS.Helvetica_Bold, size: 50)
                if indexPath.row == 0 {
                    bg.image = UIImage.init(named: "kitchenBG_SQ")
                    bg.backgroundColor = UIColor.red
                    titl.text = "KITCHEN"
                }
                else if indexPath.row == 1 {
                    bg.image = UIImage.init(named: "laundryBG_SQ")
                    bg.backgroundColor = UIColor.yellow
                    titl.text = "LAUNDRY"
                }
                else {
                    bg.image = UIImage.init(named: "livingBG_SQ")
                    bg.backgroundColor = UIColor.blue
                    titl.text = "LIVING SPACES"
                }
            }
            
            return cell//FooterCell
        }
        else{
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "FooterCell", for: indexPath)
            return cell//FooterCell
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if indexPath.item == 0 {
            KitchenButtonPressed()
        }
        else if indexPath.item == 1{
            LaundryButtonPressed()
        }
        else{
            LivingSpacesButtonPressed()
        }
    }
    
    
    // MARK:- FETCH API SUB CAT AND PROD LIST
    func getSub_SubCategoriesforType(catArray : Array<NSInteger>, objectLocation : Int){
        
        if Reachability.isConnectedToNetwork() {
            
            Constants.appDelegate.startIndicator()
            
            let catdID = "\(String(describing: catArray[objectLocation]))"
            
            let urlStr = Constants.BASEURL + "productlisting"
            let param = "brandshop_id=\(String(describing: UserDefaults.standard.object(forKey: Constants.UserID)!))&category_id=\(catdID)"
            
            Server.postRequestWithURL(urlStr, paramString: param, completionHandler: { (response) in
                
                
                if response[Constants.STATE]?.int32Value  == 1 {
                    
                    
                    let subCatArray = response["data"] as! Array<AnyObject>
                    if response["type"] as! String == "category"{
                        UserDefaults.standard.setValue(response, forKeyPath: catdID)
                        
                        for dc in subCatArray{
                            let dict = dc as! Dictionary<String,Any>
                            
                            let imageString = dict["image"] as! String
                            if self.categImgArray.contains(imageString) == false {
                                self.categImgArray.append(imageString)
                            }
                            
                            let catdID = dict["id"] as! String
                            let catInt = NSInteger.init(catdID)
                            
                            if self.catIDArray.contains(catInt!) == false{
                                self.catIDArray.append(catInt!)
                            }
                        }
                        // save sub categories
                    }
                    else{
                        // save product list table
                        UserDefaults.standard.setValue(response, forKeyPath: catdID)
                        
                        for dc in subCatArray{
                            let dict = dc as! Dictionary<String,Any>
                            
                            let imageString = dict["image"] as! String
                            if self.prodImageArray.contains(imageString) == false {
                                self.prodImageArray.append(imageString)
                            }
                            
                            var colorArr = Array<Any>()
                            if dict["color_options"] == nil{
                            }
                            else{
                                
                                let colordict = (dict["color_options"] as? Dictionary<String, Any>)!
                                
                                if colordict.count > 0
                                {
                                    for dc in colordict{
                                        let key = dc.key
                                        let val = dc.value as! Dictionary<String,Any>
                                        
                                        let valImg = val["thumb"] as! String
                                        let valName = val["label"] as! String
                                        
                                        let newDc = ["key" : key,
                                                     "label" : valName,
                                                     "thumb" : valImg]
                                        
                                        if self.colorListArray.contains(valImg) == false {
                                            self.colorListArray.append(valImg)
                                        }
                                        
                                        colorArr.append(newDc)
                                    }
                                }
                            }
                            
                            let catdID = dict["id"] as! String
                            let catInt = NSInteger.init(catdID)
                            
                            if self.prodDArray.contains(catInt!) == false{
                                self.prodDArray.append(catInt!)
                            }
                        }
                    }
                    
                    if objectLocation < self.catIDArray.count - 1 {
                        self.getSub_SubCategoriesforType(catArray: self.catIDArray, objectLocation: objectLocation + 1)
                    }
                    else{
                        // api for product detail
                        self.downloadCategoryImage(object : 0)
//                        self.callDetailAPI()
                    }
                }
                else {
                    let mess = response["message"] as! String
                    CustomAlertController.showAlert(on: self, with: Simple, andAttributes: ["title":"", "message":mess])
                }
            })
            
        }
        else {
            Constants.appDelegate.stopIndicator()
            
            CustomAlertController.showAlert(on: self, with: Simple, andAttributes:["title": "", "message":"Please check your internet connection."])
        }
    }
    
    //MARK:- CATEGORY DOWNLOAD IMAGES
    func downloadCategoryImage(object : NSInteger){
        
        let count = self.categImgArray.count
        print("CATEGORY Image Count - \(count)")

        UserDefaults.standard.set(count, forKey: Constants.CATEGORY_IMAGE_TOTAL)
        UserDefaults.standard.set(0, forKey: Constants.CATEGORY_IMAGE_CURRENT)
        
        let documentsDirectoryURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
        
        let imgname = self.categImgArray[object]
        
        let imageUrl = URL.init(string: imgname)
        if !FileManager.default.fileExists(atPath: documentsDirectoryURL.appendingPathComponent((imageUrl!.lastPathComponent)).path) {
            
            Helper.downloadImageAsset(imgname, objectNUm: object, completionHandler: { (objectNum) in
                let receivedNum = objectNum
                print("Downloaded Image - \(receivedNum)")
                if receivedNum < count - 1{
                    self.downloadCategoryImage(object: receivedNum + 1)
                }
                else{
                    print("Going to Download Color Images")
                    let when = DispatchTime.now() + 10 // change 90 to desired number of seconds
                    DispatchQueue.main.asyncAfter(deadline: when) {
                        // Your code with delay
                        self.downloadColorListImage(object : 0)
                    }
                }
            })
        }
        else
        {
            print("Incrementing cz Downloaded")
            if object == count - 1{
                let when = DispatchTime.now() + 10 // change 90 to desired number of seconds
                DispatchQueue.main.asyncAfter(deadline: when) {
                    // Your code with delay
                    self.downloadColorListImage(object : 0)
                }
            }
            else{
                self.downloadCategoryImage(object : object + 1)
            }
        }
    }
    
    //MARK:- COLOR LIST DOWNLOAD IMAGES
    func downloadColorListImage(object : NSInteger){
        
        let count = self.colorListArray.count
        print("Color List Count = \(count)")
        
        let documentsDirectoryURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
        
        let imgname = self.colorListArray[object]
        
        let imageUrl = URL.init(string: imgname)
        if !FileManager.default.fileExists(atPath: documentsDirectoryURL.appendingPathComponent((imageUrl!.lastPathComponent)).path) {
            
            Helper.downloadImageAsset(imgname, objectNUm: object, completionHandler: { (objectNum) in
                let receivedNum = objectNum
                print("Downloaded Image - \(receivedNum)")
                if receivedNum < count - 1{
                    self.downloadColorListImage(object: receivedNum + 1)
                }
                else{
                    print("Going to Download Color Images")
                    let when = DispatchTime.now() + 10 // change 90 to desired number of seconds
                    DispatchQueue.main.asyncAfter(deadline: when) {
                        // Your code with delay
                        print("Product List Image Count - \(self.prodImageArray.count)")

                        self.downloadProductListImage(object : 0)
                    }
                }
            })
        }
        else
        {
            print("Incrementing cz Downloaded")
            if object == count - 1{
                let when = DispatchTime.now() + 10 // change 90 to desired number of seconds
                DispatchQueue.main.asyncAfter(deadline: when) {
                    // Your code with delay
                    print("Product List Image Count - \(self.prodImageArray.count)")

                    self.downloadProductListImage(object : 0)
                }
            }
            else{
                self.downloadColorListImage(object : object + 1)
            }
        }
    }
    
    
    //MARK:- PRODUCT LIST DOWNLOAD IMAGES
    func downloadProductListImage(object : NSInteger){
        
        
        let count = self.prodImageArray.count
        print("Product Image Count - \(count)")
        
        let documentsDirectoryURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
        
        let imgname = self.prodImageArray[object]
        let imageUrl = URL.init(string: imgname)
        if !FileManager.default.fileExists(atPath: documentsDirectoryURL.appendingPathComponent((imageUrl!.lastPathComponent)).path) {
            
            Helper.downloadImageAsset(imgname, objectNUm: object, completionHandler: { (objectNum) in
                let receivedNum = objectNum
                print("Downloaded Image - \(receivedNum)")
                if receivedNum < count - 1{
                    self.downloadProductListImage(object: receivedNum + 1)
                }
                else if object == count - 1{
                    print("Going to Download Detail API")
                    let when = DispatchTime.now() + 10 // change 90 to desired number of seconds
                    DispatchQueue.main.asyncAfter(deadline: when) {
                        // Your code with delay
                        Constants.appDelegate.stopIndicator()
                        self.callDetailAPI()
                    }
                }
            })
        }
        else{
            if object == count - 1{
                print("Going to Download Detail API")
                let when = DispatchTime.now() + 10 // change 90 to desired number of seconds
                DispatchQueue.main.asyncAfter(deadline: when) {
                    // Your code with delay
                    Constants.appDelegate.stopIndicator()
                    self.callDetailAPI()
                }
            }
            else{
                self.downloadProductListImage(object : object + 1)
            }
        }
        
    }
    
    func callDetailAPI(){
        if self.prodDArray.count > 0{
            let when = DispatchTime.now() + 1
            DispatchQueue.main.asyncAfter(deadline: when) {
                self.getProductDetailsforType(productArray: self.prodDArray, objectLocation: 0)
            }
        }
    }
    
    // MARK:- FETCH API PRODUCT DETAILS
    func getProductDetailsforType(productArray : Array<NSInteger>, objectLocation : Int){
        
        if Reachability.isConnectedToNetwork() {
            Constants.appDelegate.startIndicator()
            
            let productID = "\(String(describing: productArray[objectLocation]))"
            
            let urlStr = Constants.BASEURL + "productview"
            let param = "product_id=\(productID)"
            
            Server.postRequestWithURL(urlStr, paramString: param, completionHandler: { (response) in
                
                if response[Constants.STATE]?.int32Value  == 1 {
                    
                    let responseData = response["product"] as! Dictionary<String,Any>
                    
                    let prodID = "PROD\(productID)"
                    
                    //Colors images
                    var colorArr = Array<Any>()
                    if responseData["color_options"] == nil{
                    }
                    else{
                        
                        let colordict = (responseData["color_options"] as? Dictionary<String, Any>)!
                        
                        if colordict.count > 0
                        {
                            for dc in colordict{
                                let key = dc.key
                                let val = dc.value as! Dictionary<String,Any>
                                
                                let valImg = val["thumb"] as! String
                                let valName = val["label"] as! String
                                
                                let newDc = ["key" : key,
                                             "label" : valName,
                                             "thumb" : valImg]
                                
                                if self.colorDetailArray.contains(valImg) == false {
                                    self.colorDetailArray.append(valImg)
                                }
                                
                                colorArr.append(newDc)
                            }
                        }
                    }
                    
                    //FEATURE
                    let featureArray = responseData["features"] as! Array<Any>
                    if featureArray.count > 0{
                        for  dc in featureArray{
                            let dict = dc as! Dictionary<String,Any>
                            let imageStr = dict["img"] as? String
                            
                            if dict["type"] as! String == "image"{
                                if self.featureArray.contains(imageStr!) == false {
                                    self.featureArray.append(imageStr!)
                                }
                            }
                        }
                    }
                    
                    //Associated images
                    if responseData["associated_images"] == nil{
                    }
                    else{
                        
                        let associateddict = (responseData["associated_images"] as? Dictionary<String, Any>)!
                        
                        if associateddict.count > 0
                        {
                            for dc in associateddict{
                                let val = dc.value as! Array<Any>
                                
                                for obj in val {
                                    let objc = obj as! Dictionary<String,Any>
                                    let valBIGImg = objc["image_big"] as! String
                                    
                                    if self.prodDetailImageArray.contains(valBIGImg) == false {
                                        self.prodDetailImageArray.append(valBIGImg)
                                    }
                                }
                                
                            }
                        }
                    }
                    
                    
                    UserDefaults.standard.set(responseData, forKey: prodID)
                    
                    if objectLocation < self.prodDArray.count - 1{
                        self.getProductDetailsforType(productArray: self.prodDArray, objectLocation: objectLocation + 1)
                    }
                    else{
                        self.downloadProductColorImage(object: 0)
//                        self.storeTime()
                    }
                }
                else {
                    if objectLocation < self.prodDArray.count - 1{
                        self.getProductDetailsforType(productArray: self.prodDArray, objectLocation: objectLocation + 1)
                    }
                    else{
                        Constants.appDelegate.stopIndicator()
                        var mess = String()
                        if response.count == 0{
                            mess = "Some Error Occurred"
                        }
                        else{
                            mess = response["message"] as! String
                        }
                        
                        CustomAlertController.showAlert(on: self, with: Simple, andAttributes: ["title":"", "message":mess])
                    }
                }
            })
        }
        else {
            Constants.appDelegate.stopIndicator()
            
            CustomAlertController.showAlert(on: self, with: Simple, andAttributes:["title": "", "message":"Please check your internet connection."])
        }
    }

    
    //MARK:- DOWNLOAD COLOR DETAIL IMAGES
    func downloadProductColorImage(object : NSInteger){
        
        print("Product Color Detail Image Count - \(self.colorDetailArray.count)")
        let tot = self.colorDetailArray.count
        UserDefaults.standard.set(tot, forKey: Constants.COLOR_DETAIL_TOTAL)
        
        let documentsDirectoryURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
        
        let imgname = self.colorDetailArray[object]
        
        let imageUrl = URL.init(string: imgname)
        if !FileManager.default.fileExists(atPath: documentsDirectoryURL.appendingPathComponent((imageUrl!.lastPathComponent)).path) {
            
            Helper.downloadImageAsset(imgname, objectNUm: object, completionHandler: { (objectNum) in
                let receivedNum = objectNum
                print("Downloaded Image - \(receivedNum)")
                if receivedNum < tot - 1{
                    self.downloadProductColorImage(object: receivedNum + 1)
                }
                else if object == tot - 1{
                    print("Going to Download Feature Images")
                    let when = DispatchTime.now() + 10 // change 90 to desired number of seconds
                    DispatchQueue.main.asyncAfter(deadline: when) {
                        // Your code with delay
                        self.downloadFeatureImage(object: 0)
                    }
                }
            })
        }
        else{
            if object == tot - 1{
                print("Going to Download Feature Images")
                let when = DispatchTime.now() + 10 // change 90 to desired number of seconds
                DispatchQueue.main.asyncAfter(deadline: when) {
                    // Your code with delay
                    self.downloadFeatureImage(object: 0)
                }
            }
            else{
                print("Incrementing cz Downloaded")

                self.downloadProductColorImage(object: object + 1)
            }
        }
    }
    
    //MARK:- DOWNLOAD FEATURE IMAGES
    func downloadFeatureImage(object : NSInteger){
        let tot = self.featureArray.count
        print("FEATURE IMAGE DOWNLOAD \(tot)")

        let documentsDirectoryURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
        
        let imgname = self.featureArray[object]
        let imageUrl = URL.init(string: imgname)
        if !FileManager.default.fileExists(atPath: documentsDirectoryURL.appendingPathComponent((imageUrl!.lastPathComponent)).path) {
            
            Helper.downloadImageAsset(imgname, objectNUm: object, completionHandler: { (objectNum) in
                let receivedNum = objectNum
                print("Downloaded Image - \(receivedNum)")
                if receivedNum < tot - 1{
                    self.downloadFeatureImage(object: receivedNum + 1)
                }
                else if object == tot - 1{
                    print("Going to Download Detail Images")
                    let when = DispatchTime.now() + 10 // change 90 to desired number of seconds
                    DispatchQueue.main.asyncAfter(deadline: when) {
                        // Your code with delay
                        self.downloadProductDetailImage(object: 0)
                    }
                }
            })
        }
        else{
            if object == tot - 1{
                print("Going to Download Detail Images")
                let when = DispatchTime.now() + 10 // change 90 to desired number of seconds
                DispatchQueue.main.asyncAfter(deadline: when) {
                    // Your code with delay
                    self.downloadProductDetailImage(object: 0)
                }
            }
            else{
                self.downloadFeatureImage(object: object + 1)
            }
        }
    }
    
    //MARK:- DOWNLOAD Product Detail Images
    func downloadProductDetailImage(object : NSInteger){
        print("PRODUCT DETAIL IMAGE DOWNLOAD \(self.prodDetailImageArray.count)")
        let tot = self.prodDetailImageArray.count
        
        let documentsDirectoryURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
        
        let imgname = self.prodDetailImageArray[object]
        let imageUrl = URL.init(string: imgname)
        if !FileManager.default.fileExists(atPath: documentsDirectoryURL.appendingPathComponent((imageUrl!.lastPathComponent)).path) {
            Helper.downloadImageAsset(imgname, objectNUm: object, completionHandler: { (objectNum) in
                let receivedNum = objectNum
                print("Downloaded Image - \(receivedNum)")
                if receivedNum < tot - 1{
                    self.downloadProductDetailImage(object: receivedNum + 1)
                }
                else if object == tot - 1{
                    print("Going to Download Detail Images")
                    self.storeTime()
                }
            })
        }
        else{
            print("Incrementing cz Downloaded - \(object)")
            if object == tot - 1{
                print("Going to Save timeStamp")
                self.storeTime()
            }
            else{
                self.downloadProductDetailImage(object: object + 1)
            }
        }
    }
    
    //MARK:- STOre TIMESTAMP
    func storeTime(){
        
//        if isloadingProductDetail == false{
//            self.isloadingProductDetail = true
            
            let datetime = Date()
            print(datetime)
            UserDefaults.standard.set(datetime, forKey: "TIMESTAMP")
            let when = DispatchTime.now() + 5 // change 2 to desired number of seconds
            DispatchQueue.main.asyncAfter(deadline: when) {
                // Your code with delay
                Constants.appDelegate.stopIndicator()
            }
//        }
    }
    
    
}
