//
//  LoginView.swift
//  MyRecipeApp
//
//  Created by Surbhi on 26/10/16.
//  Copyright © 2016 Neuronimbus. All rights reserved.
//

import UIKit


class LoginView: UIViewController, UITextFieldDelegate {

    // MARK: IBOutlets Defined
    @IBOutlet weak var logo: UIImageView!
    @IBOutlet weak var emailLabel: UILabel!
    @IBOutlet weak var emailText: UITextField!
    @IBOutlet weak var emailLine: UILabel!
    @IBOutlet weak var lineHeight: NSLayoutConstraint!
    @IBOutlet weak var passLabel: UILabel!
    @IBOutlet weak var passLine: UILabel!
    @IBOutlet weak var passText: UITextField!
    @IBOutlet weak var passLineHeight: NSLayoutConstraint!
    

    var fontsze : CGFloat = (DeviceType.IS_IPAD ? 16.0 : 13.0)
    
    fileprivate var indexToredirect : Int?
    fileprivate var sideMenuObj = MenuTableView()
    
    // MARK: Start
    override func viewDidLoad() {
        super.viewDidLoad()
        
//        self.emailText.text = "sales@aarco.in"
//        self.passText.text = "bShop123#@!"
        
        if UIApplication.shared.keyWindow?.rootViewController?.isKind(of: SideMenuNavigation.self) == true {
            sideMenuObj = sideMenuController()!.sideMenu!.menuViewController as! MenuTableView
            if sideMenuObj.lastSelectedMenuItem == 4
            {
                indexToredirect = (sideMenuController()?.sideMenu?.menuViewController as! MenuTableView).lastSelectedMenuItem
            }
        }
        
        DispatchQueue.main.async {
            // Do stuff to UI
            self.emailText.attributedPlaceholder = NSAttributedString(string: " Email Id*",  attributes: [NSForegroundColorAttributeName : Colors.Apptextcolor, NSFontAttributeName : UIFont(name: FONTS.Helvetica_Light, size: self.fontsze)!])
            
            self.passText.attributedPlaceholder = NSAttributedString(string: " Password*",  attributes: [NSForegroundColorAttributeName : Colors.Apptextcolor, NSFontAttributeName : UIFont(name: FONTS.Helvetica_Light, size: self.fontsze)!])

        }

    }
    
    
    override var prefersStatusBarHidden : Bool {
        return true
    }

    
    
    // MARK: TEXTFIELD DELEGATE
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        if textField == emailText {

            
            UIView.animate(withDuration: 1.0, animations: { 
                self.emailLabel.isHidden = false
                self.emailText.attributedPlaceholder = NSAttributedString(string: "")
                Helper.activatetextField(self.emailLine,height: self.lineHeight)

                },
                completion: { (true) in
                    DispatchQueue.main.async(execute: {
                        self.emailLabel.isHidden = false
                        self.view.bringSubview(toFront: self.emailLabel)
                        self.emailText.attributedPlaceholder = NSAttributedString(string: "")
                        Helper.activatetextField(self.emailLine,height: self.lineHeight)
                        
                        if self.passText.text == nil || self.passText.text == "" {
                            self.passLabel.isHidden = true
                        }
                        else {
                            self.passLabel.isHidden = false
                        }
                    })
            })
            
            
        }
        if textField == passText {
            if emailText.text == nil || emailText.text == "" {
             emailLabel.isHidden = true
            }
            else {
                emailLabel.isHidden = false
            }
            UIView.animate(withDuration: 1.0, delay: 0, options: UIViewAnimationOptions.transitionFlipFromBottom, animations: {
                self.passLabel.isHidden = false
                self.passText.attributedPlaceholder = NSAttributedString(string: "")
                Helper.activatetextField(self.passLine,height: self.passLineHeight)
                }, completion: nil)
        }
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField == emailText {
            if textField.text == nil || textField.text == ""
            {
                UIView.animate(withDuration: 1.0, animations: {
                    self.emailLabel.isHidden = true
                    Helper.deActivatetextField(self.emailLine,height: self.lineHeight)
                    self.emailText.attributedPlaceholder = NSAttributedString(string: " Email Id*",  attributes: [NSForegroundColorAttributeName : Colors.Apptextcolor, NSFontAttributeName : UIFont(name: FONTS.Helvetica_Light, size: self.fontsze)!])
                    }, completion: nil)
            }
        }
        if textField == passText {
            if textField.text == nil || textField.text == ""
            {
                UIView.animate(withDuration: 1.0, animations: {
                    self.passLabel.isHidden = true
                    Helper.deActivatetextField(self.passLine,height: self.passLineHeight)
                    self.passText.attributedPlaceholder = NSAttributedString(string: " Password",  attributes: [NSForegroundColorAttributeName : Colors.Apptextcolor, NSFontAttributeName : UIFont(name: FONTS.Helvetica_Light, size: self.fontsze)!])
                    }, completion: nil)
            }
        }

    }
    
    
    // MARK: BUTTON ACTIONS
    @IBAction func SignInPressed(_ sender: AnyObject) {
        
        emailText.resignFirstResponder()
        passText.resignFirstResponder()

        
        if emailText.text == "" {
            
            CustomAlertController.showAlert(on: self, with: Simple, andAttributes:["title": "", "message": "Email Id cannot be empty."] )
        }
        else if passText.text == "" {
            CustomAlertController.showAlert(on: self, with: Simple, andAttributes:["title": "", "message": "Password cannot be empty."] )
        }
        else if !Helper.isValidEmail(emailText.text!) {
            CustomAlertController.showAlert(on: self, with: Simple, andAttributes:["title": "", "message": "Enter a valid email id."] )
        }
        else {
            if Reachability.isConnectedToNetwork() {
                Constants.appDelegate.startIndicator()
                
                Server.LoginToApp(emailText.text!, authType: "", authId: "", pass: passText.text!, completionHandler: { (response) in
                    
                    Constants.appDelegate.stopIndicator()
                    
                    if response[Constants.STATE]?.int32Value  == 1 {
                        
                        let dict = response["userdata"]
                        UserDefaults.standard.set(dict!["brandshop_id"] as Any, forKey:Constants.UserID)
                        UserDefaults.standard.set(dict!, forKey:Constants.PROFILE)

                        DispatchQueue.main.async(execute: {
//                            let viewController = Constants.mainStoryboard.instantiateViewController(withIdentifier: "SideMenuNavigation")
//                            UIApplication.shared.keyWindow?.rootViewController = viewController;
                            
                            
                            Constants.appDelegate.justLogedIn = true

                            
                            let forgot = Constants.mainStoryboard.instantiateViewController(withIdentifier: "DecisionVC") as! DecisionVC
                            forgot.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
                            self.present(forgot, animated: true, completion: nil)

                        })
                    }
                    else {
                        let mess = response["message"] as! String
                        CustomAlertController.showAlert(on: self, with: Simple, andAttributes: ["title":"", "message":mess])
                    }
                })
            }
            else {
                CustomAlertController.showAlert(on: self, with: Simple, andAttributes:["title": "", "message":"Please check your internet connection."])
            }
        }
    }
    
    
    override var supportedInterfaceOrientations : UIInterfaceOrientationMask {
        return UIInterfaceOrientationMask.landscape
    }

    
}
