//
//  OrderVC.swift
//  WStoreApp
//
//  Created by Surbhi on 28/08/17.
//  Copyright © 2017 SurbhiV. All rights reserved.
//

import UIKit

class OrderVC: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var orderCollection: UICollectionView!
    var orderArr = Array<Any>()
    var address = Dictionary<String,Any>()
    var shipDict = Dictionary<String,Any>()
    var billAddressSTR = String()
    var shipAddressSTR = String()

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    // MARK:- CollectionView
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 10
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 10
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        return CGSize.init(width: 0, height: 0)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForFooterInSection section: Int) -> CGSize {
        return CGSize.init(width: 1024, height: 160)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if indexPath.item == 0 {
            let heigh = (95 * orderArr.count) + 60
            
            return CGSize.init(width: 1024, height: heigh)
        }
        else if indexPath.item == 1{
            return CGSize.init(width: 1024, height: 325)

        }
        else if indexPath.item == 2{
            return CGSize.init(width: 1024, height: 400)
            
        }
        
        return CGSize.init(width: 1024, height: 500)
    }
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        
        if kind == UICollectionElementKindSectionHeader
        {
            let hview = collectionView.dequeueReusableSupplementaryView(ofKind: UICollectionElementKindSectionHeader, withReuseIdentifier: "CollectionHeader", for: indexPath) as! CollectionHeader
            return hview
        }
        else{
            let hview = collectionView.dequeueReusableSupplementaryView(ofKind: UICollectionElementKindSectionFooter, withReuseIdentifier: "FooterCollectionCell", for: indexPath) as! FooterCollectionCell
            return hview
            
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 3
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if indexPath.item == 0{
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "OrderContentCell", for: indexPath) as! OrderContentCell
            cell.orderTable.delegate = self
            cell.orderTable.dataSource = self
            cell.orderTable.reloadData()
            return cell
        }
        else if indexPath.item == 1{
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "OrderPaymentCell", for: indexPath) as! OrderPaymentCell
            cell.billingAddressLabel.text = self.billAddressSTR
            cell.shippingAddressLabel.text = self.shipAddressSTR
            
            cell.editCartButton.addTarget(self, action: #selector(editCartOpionPressed), for: .touchUpInside)
            cell.changeBillingAddressButton.addTarget(self, action: #selector(ChangeAddressOpionPressed(sender:)), for: .touchUpInside)
            cell.changeShippingAddressButton.addTarget(self, action: #selector(ChangeAddressOpionPressed(sender:)), for: .touchUpInside)
            cell.changePaymentButton.addTarget(self, action: #selector(ChangePaymentOpionPressed(sender:)), for: .touchUpInside)

            return cell
        }
        else{
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "OrderCheckoutCell", for: indexPath) as! OrderCheckoutCell
            let totalAmount = self.CalculateTotals()
            cell.subTotalLabel.text = "Rs. \(totalAmount)"
            cell.grandTotalLabel.text = "Rs. \(totalAmount)"

            let discount = UserDefaults.standard.integer(forKey: Constants.DISCOUNTApplied)

            if discount < 0{
                cell.shippingChargesLabel.text = "Rs. \(discount)"
                cell.grandTotalLabel.text = "Rs. \(totalAmount + discount)"
            }
            else {
                cell.shippingChargesLabel.text = "Rs. 0"
            }
            
            
            cell.placeOrderButton.addTarget(self, action: #selector(PlaceOrderOpionPressed), for: .touchUpInside)
            return cell
        }
    }
    
    
    //MARK:- TABLEVIEW
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return orderArr.count + 1
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == 0{
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "headerViewOrderCell", for: indexPath)
            return cell
        }
        else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "OrderDetailcell", for: indexPath) as! OrderDetailcell
            
            if orderArr.count > 0{
                let dict = orderArr[indexPath.row - 1] as! Dictionary<String, Any>
                cell.nameLabel.text = (dict["p_Name"] as! String)
                cell.priceLabel.text = "Rs. \(String(describing: dict["P_Price"] as! String))"
                cell.subTotalLabel.text = "Rs. \(String(describing: dict["P_Price"] as! String))"
                cell.quantityText.text = (dict["p_Quantity"] as! String)
            
            }
            
            tableView.estimatedRowHeight = 100;
            tableView.rowHeight = UITableViewAutomaticDimension
            return cell
        }
    }

    
    //MARK:- Calculate Total
    func CalculateTotals()-> Int {
        var fnalTotal : Int = 0
        
        for index in 0 ... self.orderArr.count - 1 {
            var dict = self.orderArr[index] as! Dictionary<String,Any>
            let quantStr = dict["p_Quantity"] as! String
            let quant = Int.init(quantStr)
            let priceStr = dict["P_Price"] as! String
            let price = Int.init(priceStr)
            let total = price! * quant!
            fnalTotal = fnalTotal + total
        }
        
        return fnalTotal
    }
    

    
    //MARK:- Cell Actions
    func editCartOpionPressed (sender : Any){
        self.parent?.navigationController?.popViewController(animated: true)
    }
    
    func ChangePaymentOpionPressed (sender : Any){
        let nextStepVC = self.parent as! CheckoutMainVC
        nextStepVC.HeaderlabelSelected(senderTag : 1 )
    }
    
    func ChangeAddressOpionPressed (sender : Any){
        let nextStepVC = self.parent as! CheckoutMainVC
        nextStepVC.HeaderlabelSelected(senderTag : 0 )
    }
    
    func PlaceOrderOpionPressed (sender : Any){
//        PaymentCheckOutVC
        let nextStepVC = self.parent as! CheckoutMainVC
        let destViewController = Constants.mainStoryboard.instantiateViewController(withIdentifier: "PaymentCheckOutVC") as! PaymentCheckOutVC
        destViewController.checkoutArray = address
        destViewController.productsArray = orderArr
        nextStepVC.navigationController?.pushViewController(destViewController, animated: true)

    }

}
