//
//  AppDelegate.swift
//  WStoreApp
//
//  Created by Surbhi on 12/06/17.
//  Copyright © 2017 SurbhiV. All rights reserved.
//

import UIKit
import CoreData
import Firebase

//@UIApplicationMain
//@UIApplicationMain(argc, argv, MyUIApplication, AppDelegate)



class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    var deviceTokenString = String()
    var activityView1  = UIView()
    var back = UIImageView()
    var bgView = UIView()
    var imageForR = UIImageView ()
    var load = UILabel()
    var isScreenSaverOn : Bool = false
    var isLoadingOn : Bool = false
    var downloaddata : Bool = false
    var justLogedIn : Bool = false
//    @property (weak, nonatomic) UIViewController *paymentOptionVC;
    var paymentOptionVC = UIViewController()

    
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        
        if self.window == nil {
            print("nil window")
            self.window = UIWindow.init(frame: UIScreen.main.bounds)
        }
        
        
        
        let root =  Constants.mainStoryboard.instantiateViewController(withIdentifier: "SplashView") as! SplashView
        self.window?.rootViewController = root

        UIApplication.shared.applicationIconBadgeNumber = 0
        
        self.window?.makeKeyAndVisible()
        self.createIndicator()
        FirebaseApp.configure()
        
        return true
    }
    
    
    func addNotificationforScreensaver(){
        NotificationCenter.default.addObserver(self, selector: #selector(applicationDidTimeout), name: NSNotification.Name(rawValue: kApplicationDidTimeoutNotification), object: nil)
    }
    
   
    func applicationDidTimeout(_ notif : Notification)  {
        print("time exceeded!! -- SCREENSAVER ON")
        
        if self.isScreenSaverOn == false && isLoadingOn == false && justLogedIn == false {
            let destViewController = Constants.mainStoryboard.instantiateViewController(withIdentifier: "ScreenSaverVC") as! ScreenSaverVC
            window?.rootViewController?.childViewControllers.last?.present(destViewController, animated: true, completion: nil)
        }
        else{
            print("ScreenSaver ALready Running");
        }
        
    }
    
    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
        
        NotificationCenter.default.post(name: Notification.Name(rawValue: kApplicationDidTimeoutNotification), object: nil, userInfo: nil)
        if self.isLoadingOn {
            DispatchQueue.main.async {
                self.imageForR.layer.removeAllAnimations()
                self.window?.willRemoveSubview(self.activityView1)
                self.activityView1.removeFromSuperview()
            }
        }
        
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
        if self.isLoadingOn {
            if DeviceType.IS_IPAD{
//                if UIDevice.current.orientation.isPortrait {
//                    activityView1.frame = CGRect(x: 0, y: 0, width: SCREEN_MIN_LENGTH, height: SCREEN_MAX_LENGTH)
//                    back.frame = CGRect(x: 0, y: 0, width: SCREEN_MIN_LENGTH, height: SCREEN_MAX_LENGTH)
//                    bgView.frame = CGRect(x: (SCREEN_MIN_LENGTH - 120)/2,y: (SCREEN_MAX_LENGTH - 150)/2, width: 120,height: 150)
//                }
//                else{
                    activityView1.frame = CGRect(x: 0, y: 0, width: SCREEN_MAX_LENGTH, height: SCREEN_MIN_LENGTH)
                    back.frame = CGRect(x: 0, y: 0, width: SCREEN_MAX_LENGTH, height: SCREEN_MIN_LENGTH)
                    bgView.frame = CGRect(x: (SCREEN_MAX_LENGTH - 120)/2,y: (SCREEN_MIN_LENGTH - 150)/2, width: 120,height: 150)
//                }
            }
            else{
                activityView1.frame = CGRect(x: 0, y: 0, width: ScreenSize.SCREEN_WIDTH, height: ScreenSize.SCREEN_HEIGHT)
                back.frame = CGRect(x: 0, y: 0, width: ScreenSize.SCREEN_WIDTH, height: ScreenSize.SCREEN_HEIGHT)
                bgView.frame = CGRect(x: (ScreenSize.SCREEN_WIDTH - 120)/2,y: (ScreenSize.SCREEN_HEIGHT - 150)/2, width: 120,height: 150)
            }
            
            DispatchQueue.main.async {
                self.window!.addSubview(self.activityView1)
                self.rotate360Degrees()
            }

        }
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
        self.saveContext()
    }


    
    //MARK:- UNIQUE Identifier
    static func getUniqueDeviceIdentifierAsString() -> String {
        
        let appName = Bundle.main.infoDictionary![kCFBundleNameKey as String] as! String
        
        if let appUUID = SSKeychain.password(forService: appName, account: "com.WhirlpoolCulineria") {
            return appUUID
        }
        else {
            
            let appUUID = UIDevice.current.identifierForVendor?.uuidString
            SSKeychain.setPassword(appUUID, forService: appName, account: "com.WhirlpoolCulineria")
            return appUUID!
        }
    }
    
    
    //MARK:- DEVICE Token
    static func getDeviceToken() -> String {
        var str = UserDefaults.standard.string(forKey: "deviceToken")
        if str == nil || (str?.isEmpty)! {
            str = "6aa2790400e270717622b325f412951bd8b3183c9b4a099c497681b8ea2dd8db"
        }
        
        return str!
    }
    

    //MARK:- INDICATOR VIEW
    func createIndicator()
    {
        if DeviceType.IS_IPAD{
            
            activityView1 = UIView.init(frame: CGRect(x: 0, y: 0, width: SCREEN_MAX_LENGTH, height: SCREEN_MIN_LENGTH))
            activityView1.backgroundColor = UIColor.clear
            
            back = UIImageView.init(frame: CGRect(x: 0, y: 0, width: SCREEN_MAX_LENGTH, height: SCREEN_MIN_LENGTH))
            back.backgroundColor = UIColor.black
            back.alpha = 0.5
            activityView1.addSubview(back)
            
            bgView = UIView.init(frame: CGRect(x: (SCREEN_MAX_LENGTH - 120)/2,y: (SCREEN_MIN_LENGTH - 150)/2, width: 120,height: 150))
            bgView.backgroundColor = Colors.appNavigationColor
            bgView.layer.cornerRadius = 18.0
            bgView.layer.shadowColor = Colors.Lighttextcolor.cgColor
            bgView.clipsToBounds = true
            
            let bgImage = UIImageView.init(frame: CGRect(x: 36,y: 36, width: 48,height: 48))
            bgImage.backgroundColor = UIColor.clear
            bgImage.image = UIImage.init(named: "loader_icon.png")
            
            
            imageForR = UIImageView.init(frame: CGRect(x: 25,y: 25, width: 70,height: 70))
            imageForR.image = UIImage.init(named: "mouse.png")
            imageForR.backgroundColor = UIColor.clear
            
            
            load = UILabel.init(frame: CGRect(x: 0, y: 120, width: 120, height: 25))
            load.text = "LOADING..."
            load.textColor = UIColor.white
            load.font = UIFont.boldSystemFont(ofSize: 14.0)
            load.textAlignment = NSTextAlignment.center
            
            bgView.addSubview(imageForR)
            bgView.addSubview(bgImage)
            bgView.addSubview(load)
            
            activityView1.addSubview(bgView)
            print(self.window!)
            self.window?.addSubview(activityView1)
            self.window?.sendSubview(toBack: activityView1)
        }
        else{
            activityView1 = UIView.init(frame: CGRect(x: 0, y: 0, width: ScreenSize.SCREEN_WIDTH, height: ScreenSize.SCREEN_HEIGHT))
            activityView1.backgroundColor = UIColor.clear
            
            back = UIImageView.init(frame: CGRect(x: 0, y: 0, width: ScreenSize.SCREEN_WIDTH, height: ScreenSize.SCREEN_HEIGHT))
            back.backgroundColor = UIColor.black
            back.alpha = 0.5
            activityView1.addSubview(back)
            
            bgView = UIView.init(frame: CGRect(x: (ScreenSize.SCREEN_WIDTH - 120)/2,y: (ScreenSize.SCREEN_HEIGHT - 150)/2, width: 120,height: 150))
            bgView.backgroundColor = Colors.appNavigationColor
            bgView.layer.cornerRadius = 18.0
            bgView.layer.shadowColor = Colors.Lighttextcolor.cgColor
            bgView.clipsToBounds = true
            
            let bgImage = UIImageView.init(frame: CGRect(x: 36,y: 36, width: 48,height: 48))
            bgImage.backgroundColor = UIColor.clear
            bgImage.image = UIImage.init(named: "loader_icon.png")
            
            
            imageForR = UIImageView.init(frame: CGRect(x: 25,y: 25, width: 70,height: 70))
            imageForR.image = UIImage.init(named: "mouse.png")
            imageForR.backgroundColor = UIColor.clear
            
            
            load = UILabel.init(frame: CGRect(x: 0, y: 120, width: 120, height: 25))
            load.text = "LOADING..."
            load.textColor = UIColor.white
            load.font = UIFont.boldSystemFont(ofSize: 14.0)
            load.textAlignment = NSTextAlignment.center
            
            bgView.addSubview(imageForR)
            bgView.addSubview(bgImage)
            bgView.addSubview(load)
            
            activityView1.addSubview(bgView)
            print(self.window!)
            self.window?.addSubview(activityView1)
            self.window?.sendSubview(toBack: activityView1)
 
        }
    }
    
    func startIndicator()
    {
        if isLoadingOn == false {
            isLoadingOn = true
            if DeviceType.IS_IPAD{
//                if UIDevice.current.orientation.isPortrait {
//                    activityView1.frame = CGRect(x: 0, y: 0, width: SCREEN_MIN_LENGTH, height: SCREEN_MAX_LENGTH)
//                    back.frame = CGRect(x: 0, y: 0, width: SCREEN_MIN_LENGTH, height: SCREEN_MAX_LENGTH)
//                    bgView.frame = CGRect(x: (SCREEN_MIN_LENGTH - 120)/2,y: (SCREEN_MAX_LENGTH - 150)/2, width: 120,height: 150)
//                }
//                else{
                    activityView1.frame = CGRect(x: 0, y: 0, width: SCREEN_MAX_LENGTH, height: SCREEN_MIN_LENGTH)
                    back.frame = CGRect(x: 0, y: 0, width: SCREEN_MAX_LENGTH, height: SCREEN_MIN_LENGTH)
                    bgView.frame = CGRect(x: (SCREEN_MAX_LENGTH - 120)/2,y: (SCREEN_MIN_LENGTH - 150)/2, width: 120,height: 150)
//                }
            }
            else{
                activityView1.frame = CGRect(x: 0, y: 0, width: ScreenSize.SCREEN_WIDTH, height: ScreenSize.SCREEN_HEIGHT)
                back.frame = CGRect(x: 0, y: 0, width: ScreenSize.SCREEN_WIDTH, height: ScreenSize.SCREEN_HEIGHT)
                bgView.frame = CGRect(x: (ScreenSize.SCREEN_WIDTH - 120)/2,y: (ScreenSize.SCREEN_HEIGHT - 150)/2, width: 120,height: 150)
            }
            
            DispatchQueue.main.async {
                self.window!.addSubview(self.activityView1)
                self.rotate360Degrees()
            }
        }
    }
    
    func stopIndicator()
    {
        isLoadingOn = false
        DispatchQueue.main.async {
            self.imageForR.layer.removeAllAnimations()
            self.window?.willRemoveSubview(self.activityView1)
            self.activityView1.removeFromSuperview()
        }
    }
    
    func rotate360Degrees(_ duration: CFTimeInterval = 3.5, completionDelegate: AnyObject? = nil)
    {
        let rotateAnimation = CABasicAnimation(keyPath: "transform.rotation")
        rotateAnimation.fromValue = 0.0
        rotateAnimation.toValue = CGFloat(Double.pi * -2.0)
        rotateAnimation.duration = duration
        rotateAnimation.repeatCount = 100000
        
        if let delegate: AnyObject = completionDelegate {
            rotateAnimation.delegate = delegate as? CAAnimationDelegate
        }
        imageForR.layer.add(rotateAnimation, forKey: nil)
    }
    
    //MARK:- Orientation
    
    // MARK: - Core Data stack
    
    lazy var persistentContainer: NSPersistentContainer = {
        /*
         The persistent container for the application. This implementation
         creates and returns a container, having loaded the store for the
         application to it. This property is optional since there are legitimate
         error conditions that could cause the creation of the store to fail.
         */
        let container = NSPersistentContainer(name: "WStore")
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                
                /*
                 Typical reasons for an error here include:
                 * The parent directory does not exist, cannot be created, or disallows writing.
                 * The persistent store is not accessible, due to permissions or data protection when the device is locked.
                 * The device is out of space.
                 * The store could not be migrated to the current model version.
                 Check the error message to determine what the actual problem was.
                 */
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
        return container
    }()
    
    // MARK: - Core Data Saving support
    
    func saveContext () {
        let context = persistentContainer.viewContext
        if context.hasChanges {
            do {
                try context.save()
            } catch {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                let nserror = error as NSError
                fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
            }
        }
    }  


}



