//
//  main.swift
//  WStoreApp
//
//  Created by Surbhi on 16/06/17.
//  Copyright © 2017 SurbhiV. All rights reserved.
//

import UIKit

//UIApplicationMain(Progress.argc, Progress.unsafeArgv, NSStringFromClass(MyUIApplication) , NSStringFromClass(AppDelegate) )
UIApplicationMain(
    CommandLine.argc,
    UnsafeMutableRawPointer(CommandLine.unsafeArgv)
        .bindMemory(
            to: UnsafeMutablePointer<Int8>.self,
            capacity: Int(CommandLine.argc)),
    NSStringFromClass(MyUIApplication.self),
    NSStringFromClass(AppDelegate.self)
)

//UIApplicationMain(argc,unsafeArgv,NSStringFromClass(MyUIApplication),NSStringFromClass(AppDelegate))

class main: UIApplication {

 
}
